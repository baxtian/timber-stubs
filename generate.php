<?php

require 'vendor/autoload.php';

use StubsGenerator\StubsGenerator;
use StubsGenerator\Finder;
use Symfony\Component\Filesystem\Filesystem;
use PhpParser\PrettyPrinter\Standard;

$header = '
/**
 * Generated stub declarations for Timber.
 * https://upstatement.com/
 */';
$filename = 'timber-stubs.php';
$include  = [
	'wp-content/plugins/timber-library/lib',
	'wp-content/plugins/timber-library/vendor/twig/twig/src',
];
$filesystem = new Filesystem();

// Generador del stub
$finder = Finder::create()->in($include)
	->exclude('tests')
	// ->exclude('timber-starter-theme')
	// ->exclude('wp-includes/spl-autoload-compat.php')
	->sortByName()
	;
$generator = new StubsGenerator(StubsGenerator::DEFAULT, ['nullify_globals' => true]);
$result    = $generator->generate($finder);

// Archivar el stub
$printer       = new Standard();
$prettyPrinted = substr_replace($result->prettyPrint($printer), "<?php{$header}", 0, 5);
$filesystem->dumpFile($filename, $prettyPrinted);

<?php
/**
 * Generated stub declarations for Timber.
 * https://upstatement.com/
 */

namespace Timber {
    class Admin
    {
        public static function init()
        {
        }
        /**
         * @param array   $links
         * @param string  $file
         * @return array
         */
        public static function meta_links($links, $file)
        {
        }
        /**
         *	@codeCoverageIgnore
         */
        protected static function disable_update()
        {
        }
        /**
         *	@codeCoverageIgnore
         */
        protected static function update_message_milestone()
        {
        }
        /**
         *	@codeCoverageIgnore
         */
        protected static function update_message_major()
        {
        }
        /**
         *	@codeCoverageIgnore
         */
        protected static function update_message_minor()
        {
        }
        public static function get_upgrade_magnitude($current_version, $new_version)
        {
        }
        /**
         *  Displays an update message for plugin list screens.
         *  Shows only the version updates from the current until the newest version
         *
         *	@codeCoverageIgnore
         *
         *  @type	function
         *  @date	4/22/16
         *
         *  @param	{array}		$plugin_data
         *  @param	{object}	$r
         */
        public static function in_plugin_update_message($plugin_data, $r)
        {
        }
    }
    abstract class Core
    {
        public $id;
        public $ID;
        public $object_type;
        /**
         *
         * @return boolean
         */
        public function __isset($field)
        {
        }
        /**
         * This is helpful for twig to return properties and methods see: https://github.com/fabpot/Twig/issues/2
         * @return mixed
         */
        public function __call($field, $args)
        {
        }
        /**
         * This is helpful for twig to return properties and methods see: https://github.com/fabpot/Twig/issues/2
         *
         * @return mixed
         */
        public function __get($field)
        {
        }
        /**
         * Takes an array or object and adds the properties to the parent object
         * @example
         * ```php
         * $data = array('airplane' => '757-200', 'flight' => '5316');
         * $post = new Timber\Post()
         * $post->import(data);
         * echo $post->airplane; //757-200
         * ```
         * @param array|object $info an object or array you want to grab data from to attach to the Timber object
         */
        public function import($info, $force = false)
        {
        }
        /**
         * @ignore
         * @param string  $key
         * @param mixed   $value
         */
        public function update($key, $value)
        {
        }
        /**
         * Can you edit this post/term/user? Well good for you. You're no better than me.
         * @example
         * ```twig
         * {% if post.can_edit %}
         * <a href="{{ post.edit_link }}">Edit</a>
         * {% endif %}
         * ```
         * ```html
         * <a href="http://example.org/wp-admin/edit.php?p=242">Edit</a>
         * ```
         * @return bool
         */
        public function can_edit()
        {
        }
        /**
         *
         *
         * @return array
         */
        public function get_method_values()
        {
        }
        /**
         * @param string $field_name
         * @return mixed
         */
        public function get_field($field_name)
        {
        }
    }
    /**
     * The TimberArchives class is used to generate a menu based on the date archives of your posts. The [Nieman Foundation News site](http://nieman.harvard.edu/news/) has an example of how the output can be used in a real site ([screenshot](https://cloud.githubusercontent.com/assets/1298086/9610076/3cdca596-50a5-11e5-82fd-acb74c09c482.png)).
     *
     * @example
     * ```php
     * $context['archives'] = new TimberArchives( $args );
     * ```
     * ```twig
     * <ul>
     * {% for item in archives.items %}
     *     <li><a href="{{item.link}}">{{item.name}}</a></li>
     *     {% for child in item.children %}
     *         <li class="child"><a href="{{child.link}}">{{child.name}}</a></li>
     *     {% endfor %}
     * {% endfor %}
     * </ul>
     * ```
     * ```html
     * <ul>
     *     <li>2015</li>
     *     <li class="child">May</li>
     *     <li class="child">April</li>
     *     <li class="child">March</li>
     *     <li class="child">February</li>
     *     <li class="child">January</li>
     *     <li>2014</li>
     *     <li class="child">December</li>
     *     <li class="child">November</li>
     *     <li class="child">October</li>
     * </ul>
     * ```
     */
    class Archives extends \Timber\Core
    {
        public $base = '';
        /**
         * @api
         * @var array the items of the archives to iterate through and markup for your page
         */
        public $items;
        /**
         * @api
         * @param $args array of arguments {
         *     @type bool show_year => false
         *     @type string
         *     @type string type => 'monthly-nested'
         *     @type int limit => -1
         *     @type bool show_post_count => false
         *     @type string order => 'DESC'
         *     @type string post_type => 'post'
         *     @type bool show_year => false
         *     @type bool nested => false
         * }
         * @param string $base any additional paths that need to be prepended to the URLs that are generated, for example: "tags"
         */
        public function __construct($args = null, $base = '')
        {
        }
        /**
         * @internal
         * @param array|string $args
         * @param string $base
         */
        public function init($args = null, $base = '')
        {
        }
        /**
         * @internal
         * @param string $url
         * @param string $text
         * @return mixed
         */
        protected function get_archives_link($url, $text, $post_count = 0)
        {
        }
        /**
         * @internal
         * @param array $args
         * @param string $last_changed
         * @param string $join
         * @param string $where
         * @param string $order
         * @param string $limit
         * @return array
         */
        protected function get_items_yearly($args, $last_changed, $join, $where, $order, $limit)
        {
        }
        /**
         * @internal
         * @param array|string $args
         * @param string $last_changed
         * @param string $join
         * @param string $where
         * @param string $order
         * @param string $limit
         * @param bool $nested
         * @return array
         */
        protected function get_items_monthly($args, $last_changed, $join, $where, $order, $limit = '', $nested = true)
        {
        }
        /**
         * @api
         * @param array|string $args
         * @return array|string
         */
        public function get_items($args = null)
        {
        }
    }
}
namespace Timber\Cache {
    class Cleaner
    {
        protected static function delete_transients_single_site()
        {
        }
        protected static function delete_transients_multisite()
        {
        }
        public static function delete_transients()
        {
        }
    }
    class KeyGenerator implements \Twig\CacheExtension\CacheStrategy\KeyGeneratorInterface
    {
        /**
         * @param mixed $value
         * @return string
         */
        public function generateKey($value)
        {
        }
    }
    interface TimberKeyGeneratorInterface
    {
        public function _get_cache_key();
    }
    class WPObjectCacheAdapter implements \Twig\CacheExtension\CacheProviderInterface
    {
        public function __construct(\Timber\Loader $timberloader, $cache_group = 'timber')
        {
        }
        public function fetch($key)
        {
        }
        public function save($key, $value, $expire = 0)
        {
        }
    }
}
namespace Timber {
    interface CoreInterface
    {
        public function __call($field, $args);
        public function __get($field);
        /**
         * @return boolean
         */
        public function __isset($field);
        public function meta($key);
    }
    /**
     * The Timber\Comment class is used to view the output of comments. 99% of the time this will be in the context of the comments on a post. However you can also fetch a comment directly using its comment ID.
     * @example
     * ```php
     * $comment = new Timber\Comment($comment_id);
     * $context['comment_of_the_day'] = $comment;
     * Timber::render('index.twig', $context);
     * ```
     *
     * ```twig
     * <p class="comment">{{comment_of_the_day.content}}</p>
     * <p class="comment-attribution">- {{comment.author.name}}</p>
     * ```
     *
     * ```html
     * <p class="comment">But, O Sarah! If the dead can come back to this earth and flit unseen around those they loved, I shall always be near you; in the garish day and in the darkest night -- amidst your happiest scenes and gloomiest hours - always, always; and if there be a soft breeze upon your cheek, it shall be my breath; or the cool air fans your throbbing temple, it shall be my spirit passing by.</p>
     * <p class="comment-attribution">- Sullivan Ballou</p>
     * ```
     */
    class Comment extends \Timber\Core implements \Timber\CoreInterface
    {
        public $PostClass = 'Post';
        public $object_type = 'comment';
        public static $representation = 'comment';
        public $ID;
        public $id;
        public $comment_author_email;
        public $comment_content;
        public $comment_date;
        public $comment_ID;
        public $user_id;
        public $post_id;
        public $comment_author;
        public $_depth = 0;
        protected $children = array();
        /**
         * @param int $cid
         */
        public function __construct($cid)
        {
        }
        public function __toString()
        {
        }
        /**
         * @internal
         * @param integer $cid
         */
        public function init($cid)
        {
        }
        /**
         * @api
         * @example
         * ```twig
         * <h3>Comments by...</h3>
         * <ol>
         * {% for comment in post.comments %}
         *     <li>{{comment.author.name}}, who has the following roles: {{comment.author.roles|join(', ')}}</li>
         * {% endfor %}
         * </ol>
         * ```
         * ```html
         * <h3>Comments by...</h3>
         * <ol>
         *  <li>Jared Novack, who is a contributor</li>
         *  <li>Katie Ricci, who is a subscriber</li>
         *  <li>Rebecca Pearl, who is a author</li>
         * </ol>
         * ```
         * @return User
         */
        public function author()
        {
        }
        /**
         * Fetches the Gravatar
         * @api
         * @example
         * ```twig
         * <img src="{{comment.avatar(36,template_uri~"/img/dude.jpg")}}" alt="Image of {{comment.author.name}}" />
         * ```
         * ```html
         * <img src="http://gravatar.com/i/sfsfsdfasdfsfa.jpg" alt="Image of Katherine Rich" />
         * ```
         * @param int $size
         * @param string $default
         * @return bool|mixed|string
         */
        public function avatar($size = 92, $default = '')
        {
        }
        /**
         * @api
         * @return string
         */
        public function content()
        {
        }
        /**
         * @api
         * @return array Comments
         */
        public function children()
        {
        }
        /**
         * @param Comment $child_comment;
         */
        public function add_child(\Timber\Comment $child_comment)
        {
        }
        /**
         * @param int $depth
         */
        public function update_depth($depth = 0)
        {
        }
        public function depth()
        {
        }
        /**
         * @api
         * @example
         * ```twig
         * {% if comment.approved %}
         * 	Your comment is good
         * {% else %}
         * 	Do you kiss your mother with that mouth?
         * {% endif %}
         * ```
         * @return boolean
         */
        public function approved()
        {
        }
        /**
         * @api
         * @example
         * ```twig
         * {% for comment in post.comments %}
         * <article class="comment">
         *   <p class="date">Posted on {{ comment.date }}:</p>
         *   <p class="comment">{{ comment.content }}</p>
         * </article>
         * {% endfor %}
         * ```
         * ```html
         * <article class="comment">
         *   <p class="date">Posted on September 28, 2015:</p>
         *   <p class="comment">Happy Birthday!</p>
         * </article>
         * ```
         * @return string
         */
        public function date($date_format = '')
        {
        }
        /**
         * @api
         * @example
         * ```twig
         * {% for comment in post.comments %}
         * <article class="comment">
         *   <p class="date">Posted on {{ comment.date }} at {{comment.time}}:</p>
         *   <p class="comment">{{ comment.content }}</p>
         * </article>
         * {% endfor %}
         * ```
         * ```html
         * <article class="comment">
         *   <p class="date">Posted on September 28, 2015 at 12:45 am:</p>
         *   <p class="comment">Happy Birthday!</p>
         * </article>
         * ```
         * @return string
         */
        public function time($time_format = '')
        {
        }
        /**
         * @param string $field_name
         * @return mixed
         */
        public function meta($field_name)
        {
        }
        /**
         * @api
         * @return bool
         */
        public function is_child()
        {
        }
        /**
         * @internal
         * @param int $comment_id
         * @return mixed
         */
        protected function get_meta_fields($comment_id = null)
        {
        }
        /**
         *
         * @internal
         * @param string $field_name
         * @return mixed
         */
        protected function get_meta_field($field_name)
        {
        }
        /**
         * Enqueue the WP threaded comments javascript,
         * and fetch the reply link for various comments.
         * @api
         * @return string
         */
        public function reply_link($reply_text = 'Reply')
        {
        }
        /* AVATAR Stuff
        	======================= */
        /**
         * @internal
         * @return string
         */
        protected function avatar_email()
        {
        }
        /**
         * @internal
         * @param string $email_hash
         * @return string
         */
        protected function avatar_host($email_hash)
        {
        }
        /**
         * @internal
         * @todo  what if it's relative?
         * @param string $default
         * @param string $email
         * @param string $size
         * @param string $host
         * @return string
         */
        protected function avatar_default($default, $email, $size, $host)
        {
        }
        /**
         * @internal
         * @param string $default
         * @param string $host
         * @param string $email_hash
         * @param string $size
         * @return mixed
         */
        protected function avatar_out($default, $host, $email_hash, $size)
        {
        }
    }
    /**
     * This object is a special type of array that hold WordPress comments as `Timber\Comment` objects. 
     * You probably won't use this directly. This object is returned when calling `{{ post.comments }}` 
     * in Twig.
     *
     * @example 
     * ```twig
     * {# single.twig #}
     * <div id="post-comments">
     *   <h4>Comments on {{ post.title }}</h4>
     *   <ul>
     *     {% for comment in post.comments %}
     *       {% include 'comment.twig' %}
     *     {% endfor %}
     *   </ul>
     *   <div class="comment-form">
     *     {{ function('comment_form') }}
     *   </div>
     * </div>
     * ```
     *
     * ```twig
     * {# comment.twig #}
     * <li>
     *   <div>{{ comment.content }}</div>
     *   <p class="comment-author">{{ comment.author.name }}</p>
     *   {{ function('comment_form') }}
     *   <!-- nested comments here -->
     *   {% if comment.children %}
     *     <div class="replies"> 
     *	     {% for child_comment in comment.children %}
     *         {% include 'comment.twig' with { comment:child_comment } %}
     *       {% endfor %}
     *     </div> 
     *   {% endif %}    
     * </li>
     * ```
     */
    class CommentThread extends \ArrayObject
    {
        var $CommentClass = 'Timber\\Comment';
        var $post_id;
        var $_orderby = '';
        var $_order = 'ASC';
        /**
         * @param int $post_id
         * @param array|boolean $args an array of arguments
         * 						or false if to skip initialization
         */
        public function __construct($post_id, $args = array())
        {
        }
        /**
         * @internal
         */
        protected function fetch_comments($args = array())
        {
        }
        /**
         * @internal
         */
        protected function merge_args($args)
        {
        }
        /**
         * @internal
         * @experimental
         */
        public function order($order = 'ASC')
        {
        }
        /**
         * @internal
         * @experimental
         */
        public function orderby($orderby = 'wp')
        {
        }
        /**
         * @internal
         */
        public function init($args = array())
        {
        }
        /**
         * @internal
         */
        protected function clear()
        {
        }
        /**
         * @internal
         */
        protected function import_comments($arr)
        {
        }
    }
    /**
     * FunctionWrapper Class.
     *
     * With Timber, we want to prepare all the data before we echo content through a render function. Some functionality in WordPress directly echoes output instead of returning it. This class makes it easier to store the results of an echoing function by using ob_start() and ob_end_clean() behind the scenes.
     *
     * @package Timber
     */
    class FunctionWrapper
    {
        public function __toString()
        {
        }
        /**
         *
         *
         * @param callable $function
         * @param array   $args
         * @param bool    $return_output_buffer
         */
        public function __construct($function, $args = array(), $return_output_buffer = false)
        {
        }
        /**
         * Make function available in Twig.
         *
         * When a function is added more than once, addFunction() will throw a LogicException that states that the function
         * is already registered. By catching this exception, we can prevent a fatal error.
         * @see \Twig\Extension\StagingExtension::addFunction()
         *
         * @deprecated since 1.3.0
         * @todo remove in 1.4.0
         * @param \Twig\Environment $twig
         * @return \Twig\Environment
         */
        public function add_to_twig($twig)
        {
        }
        /**
         *
         *
         * @return string
         */
        public function call()
        {
        }
    }
    /**
     * As the name suggests these are helpers for Timber (and you!) when developing. You can find additional (mainly internally-focused helpers) in TimberURLHelper
     */
    class Helper
    {
        /**
         * A utility for a one-stop shop for Transients
         * @api
         * @example
         * ```php
         * $context = Timber::context();
         * $context['favorites'] = Timber\Helper::transient('user-' .$uid. '-favorites', function() use ($uid) {
         *  	//some expensive query here that's doing something you want to store to a transient
         *  	return $favorites;
         * }, 600);
         * Timber::render('single.twig', $context);
         * ```
         *
         * @param string  	$slug           Unique identifier for transient
         * @param callable 	$callback      Callback that generates the data that's to be cached
         * @param integer  	$transient_time (optional) Expiration of transients in seconds
         * @param integer 	$lock_timeout   (optional) How long (in seconds) to lock the transient to prevent race conditions
         * @param boolean 	$force          (optional) Force callback to be executed when transient is locked
         * @return mixed
         */
        public static function transient($slug, $callback, $transient_time = 0, $lock_timeout = 5, $force = false)
        {
        }
        /**
         * Does the dirty work of locking the transient, running the callback and unlocking
         * @param string 	$slug
         * @param callable 	$callback
         * @param integer  	$transient_time Expiration of transients in seconds
         * @param integer 	$lock_timeout   How long (in seconds) to lock the transient to prevent race conditions
         * @param boolean 	$force          Force callback to be executed when transient is locked
         * @param boolean 	$enable_transients Force callback to be executed when transient is locked
         */
        protected static function handle_transient_locking($slug, $callback, $transient_time, $lock_timeout, $force, $enable_transients)
        {
        }
        /**
         * @internal
         * @param string $slug
         * @param integer $lock_timeout
         */
        public static function _lock_transient($slug, $lock_timeout)
        {
        }
        /**
         * @internal
         * @param string $slug
         */
        public static function _unlock_transient($slug)
        {
        }
        /**
         * @internal
         * @param string $slug
         */
        public static function _is_transient_locked($slug)
        {
        }
        /* These are for measuring page render time */
        /**
         * For measuring time, this will start a timer
         * @api
         * @return float
         */
        public static function start_timer()
        {
        }
        /**
         * For stopping time and getting the data
         * @example
         * ```php
         * $start = TimberHelper::start_timer();
         * // do some stuff that takes awhile
         * echo TimberHelper::stop_timer( $start );
         * ```
         * @param int     $start
         * @return string
         */
        public static function stop_timer($start)
        {
        }
        /* Function Utilities
        	======================== */
        /**
         * Calls a function with an output buffer. This is useful if you have a function that outputs text that you want to capture and use within a twig template.
         * @example
         * ```php
         * function the_form() {
         *     echo '<form action="form.php"><input type="text" /><input type="submit /></form>';
         * }
         *
         * $context = Timber::context();
         * $context['post'] = new Timber\Post();
         * $context['my_form'] = TimberHelper::ob_function('the_form');
         * Timber::render('single-form.twig', $context);
         * ```
         * ```twig
         * <h1>{{ post.title }}</h1>
         * {{ my_form }}
         * ```
         * ```html
         * <h1>Apply to my contest!</h1>
         * <form action="form.php"><input type="text" /><input type="submit /></form>
         * ```
         * @api
         * @param callback $function
         * @param array   $args
         * @return string
         */
        public static function ob_function($function, $args = array(null))
        {
        }
        /**
         * @codeCoverageIgnore
         * @deprecated since 1.3.0
         *
         * @param mixed $function_name        String or array( $class( string|object ), $function_name ).
         * @param array $defaults             Optional.
         * @param bool  $return_output_buffer Optional. Return function output instead of return value. Default false.
         * @return FunctionWrapper|mixed
         */
        public static function function_wrapper($function_name, $defaults = array(), $return_output_buffer = false)
        {
        }
        /**
         *
         *
         * @param mixed $arg that you want to error_log
         * @return void
         */
        public static function error_log($error)
        {
        }
        /**
         * @param string $message that you want to output
         * @return boolean
         */
        public static function warn($message)
        {
        }
        /**
         *
         *
         * @param string  $separator
         * @param string  $seplocation
         * @return string
         */
        public static function get_wp_title($separator = ' ', $seplocation = 'left')
        {
        }
        /* Text Utitilites */
        /* Object Utilities
        	======================== */
        /**
         * @codeCoverageIgnore
         * @deprecated since 1.2.0
         * @see TextHelper::trim_words
         * @param string  $text
         * @param int     $num_words
         * @param string|null|false  $more text to appear in "Read more...". Null to use default, false to hide
         * @param string  $allowed_tags
         * @return string
         */
        public static function trim_words($text, $num_words = 55, $more = null, $allowed_tags = 'p a span b i br blockquote')
        {
        }
        /**
         * @deprecated since 1.2.0
         * @see TextHelper::close_tags
         * @param string  $html
         * @return string
         */
        public static function close_tags($html)
        {
        }
        /**
         *
         *
         * @param array   $array
         * @param string  $prop
         * @return void
         */
        public static function osort(&$array, $prop)
        {
        }
        /**
         *
         *
         * @param array   $arr
         * @return bool
         */
        public static function is_array_assoc($arr)
        {
        }
        /**
         *
         *
         * @param array   $array
         * @return \stdClass
         */
        public static function array_to_object($array)
        {
        }
        /**
         *
         *
         * @param array   $array
         * @param string  $key
         * @param mixed   $value
         * @return bool|int
         */
        public static function get_object_index_by_property($array, $key, $value)
        {
        }
        /**
         *
         *
         * @param array   $array
         * @param string  $key
         * @param mixed   $value
         * @return array|null
         * @throws Exception
         */
        public static function get_object_by_property($array, $key, $value)
        {
        }
        /**
         *
         *
         * @param array   $array
         * @param int     $len
         * @return array
         */
        public static function array_truncate($array, $len)
        {
        }
        /* Bool Utilities
        	======================== */
        /**
         *
         *
         * @param mixed   $value
         * @return bool
         */
        public static function is_true($value)
        {
        }
        /**
         *
         *
         * @param int     $i
         * @return bool
         */
        public static function iseven($i)
        {
        }
        /**
         *
         *
         * @param int     $i
         * @return bool
         */
        public static function isodd($i)
        {
        }
        /**
         * Plucks the values of a certain key from an array of objects
         * @param array $array
         * @param string $key
         */
        public static function pluck($array, $key)
        {
        }
        /**
         * Filters a list of objects, based on a set of key => value arguments.
         * Uses native Twig Filter.
         *
         * @since 1.14.0
         * @deprecated since 1.17 (to be removed in 2.0). Use array_filter or Helper::wp_list_filter instead
         * @todo remove this in 2.x
         * @param array                 $list to filter.
         * @param callback|string|array $arrow function used for filtering,
         *                              string or array for backward compatibility.
         * @param string                $operator to use (AND, NOT, OR). For backward compatibility.
         * @return array
         */
        public static function filter_array($list, $arrow, $operator = 'AND')
        {
        }
        /**
         * Filters a list of objects, based on a set of key => value arguments.
         * Uses WordPress WP_List_Util's filter.
         *
         * @since 1.5.3
         * @ticket #1594
         * @param array        $list to filter.
         * @param string|array $args to search for.
         * @param string       $operator to use (AND, NOT, OR).
         * @return array
         */
        public static function wp_list_filter($list, $args, $operator = 'AND')
        {
        }
        /* Links, Forms, Etc. Utilities
        	======================== */
        /**
         *
         * Gets the comment form for use on a single article page
         * @deprecated 0.21.8 use `{{ function('comment_form') }}` instead
         * @param int $post_id which post_id should the form be tied to?
         * @param array The $args thing is a mess, [fix at some point](http://codex.wordpress.org/Function_Reference/comment_form)
         * @return string
         */
        public static function get_comment_form($post_id = null, $args = array())
        {
        }
        /**
         * @codeCoverageIgnore
         * @deprecated since 1.1.2
         * @param array  $args
         * @return array
         */
        public static function paginate_links($args = array())
        {
        }
        /**
         * @codeCoverageIgnore
         * @return string
         */
        public function get_current_url()
        {
        }
        /**
         * Converts a WP object (WP_Post, WP_Term) into his
         * equivalent Timber class (Timber\Post, Timber\Term).
         *
         * If no match is found the function will return the inital argument.
         *
         * @param mix $obj WP Object
         * @return mix Instance of equivalent Timber object, or the argument if no match is found
         */
        public static function convert_wp_object($obj)
        {
        }
    }
    /**
     * This is the object you use to access or extend WordPress posts. Think of it as Timber's (more accessible) version of WP_Post. This is used throughout Timber to represent posts retrieved from WordPress making them available to Twig templates. See the PHP and Twig examples for an example of what it's like to work with this object in your code.
     * @example
     * ```php
     * // single.php, see connected twig example
     * $context = Timber::context();
     * $context['post'] = new Timber\Post(); // It's a new Timber\Post object, but an existing post from WordPress.
     * Timber::render('single.twig', $context);
     * ?>
     * ```
     * ```twig
     * {# single.twig #}
     * <article>
     *     <h1 class="headline">{{post.title}}</h1>
     *     <div class="body">
     *         {{post.content}}
     *     </div>
     * </article>
     * ```
     *
     * ```html
     * <article>
     *     <h1 class="headline">The Empire Strikes Back</h1>
     *     <div class="body">
     *         It is a dark time for the Rebellion. Although the Death Star has been destroyed, Imperial troops have driven the Rebel forces from their hidden base and pursued them across the galaxy.
     *     </div>
     * </article>
     * ```
     *
     * @package Timber
     */
    class Post extends \Timber\Core implements \Timber\CoreInterface
    {
        /**
         * @var string $ImageClass the name of the class to handle images by default
         */
        public $ImageClass = 'Timber\\Image';
        /**
         * @var string $PostClass the name of the class to handle posts by default
         */
        public $PostClass = 'Timber\\Post';
        /**
         * @var string $TermClass the name of the class to handle terms by default
         */
        public $TermClass = 'Timber\\Term';
        /**
         * @var string $object_type what does this class represent in WordPress terms?
         */
        public $object_type = 'post';
        /**
         * @var array $custom stores custom meta data
         */
        public $custom = array();
        /**
         * @var string $representation what does this class represent in WordPress terms?
         */
        public static $representation = 'post';
        /**
         * @internal
         * @var string $___content stores the processed content internally
         */
        protected $___content;
        /**
         * @var string $_permalink the returned permalink from WP's get_permalink function
         */
        protected $_permalink;
        /**
         * @var array $_next stores the results of the next Timber\Post in a set inside an array (in order to manage by-taxonomy)
         */
        protected $_next = array();
        /**
         * @var array $_prev stores the results of the previous Timber\Post in a set inside an array (in order to manage by-taxonomy)
         */
        protected $_prev = array();
        /**
         * @var string $class stores the CSS classes for the post (ex: "post post-type-book post-123")
         */
        protected $_css_class;
        /**
         * @api
         * @var int $id the numeric WordPress id of a post
         */
        public $id;
        /**
         * @var string 	$ID 			the numeric WordPress id of a post, capitalized to match WP usage
         */
        public $ID;
        /**
         * @var int 	$post_author 	the numeric ID of the a post's author corresponding to the wp_user dtable
         */
        public $post_author;
        /**
         * @var string 	$post_content 	the raw text of a WP post as stored in the database
         */
        public $post_content;
        /**
         * @var string 	$post_date 		the raw date string as stored in the WP database, ex: 2014-07-05 18:01:39
         */
        public $post_date;
        /**
         * @var string 	$post_excerpt 	the raw text of a manual post excerpt as stored in the database
         */
        public $post_excerpt;
        /**
         * @var int 		$post_parent 	the numeric ID of a post's parent post
         */
        public $post_parent;
        /**
         * @api
         * @var string 		$post_status 	the status of a post ("draft", "publish", etc.)
         */
        public $post_status;
        /**
         * @var string 	$post_title 	the raw text of a post's title as stored in the database
         */
        public $post_title;
        /**
         * @api
         * @var string 	$post_type 		the name of the post type, this is the machine name (so "my_custom_post_type" as opposed to "My Custom Post Type")
         */
        public $post_type;
        /**
         * @api
         * @var string 	$slug 		the URL-safe slug, this corresponds to the poorly-named "post_name" in the WP database, ex: "hello-world"
         */
        public $slug;
        /**
         * @var PostType $_type stores the PostType object for the Post
         */
        protected $__type;
        /**
         * If you send the constructor nothing it will try to figure out the current post id based on being inside The_Loop
         * @example
         * ```php
         * $post = new Timber\Post();
         * $other_post = new Timber\Post($random_post_id);
         * ```
         * @param mixed $pid
         */
        public function __construct($pid = null)
        {
        }
        /**
         * This is helpful for twig to return properties and methods see: https://github.com/fabpot/Twig/issues/2
         * This is also here to ensure that {{ post.class }} remains usable
         * @return mixed
         */
        public function __get($field)
        {
        }
        /**
         * This is helpful for twig to return properties and methods see: https://github.com/fabpot/Twig/issues/2
         * This is also here to ensure that {{ post.class }} remains usable
         * @return mixed
         */
        public function __call($field, $args)
        {
        }
        /**
         * Determine whether or not an admin/editor is looking at the post in "preview mode" via the
         * WordPress admin
         * @internal
         * @return bool
         */
        protected static function is_previewing()
        {
        }
        /**
         * tries to figure out what post you want to get if not explictly defined (or if it is, allows it to be passed through)
         * @internal
         * @param mixed a value to test against
         * @return int|null the numberic id we should be using for this post object, null when there's no ID (ex: 404 page)
         */
        protected function determine_id($pid)
        {
        }
        /**
         * Outputs the title of the post if you do something like `<h1>{{post}}</h1>`
         * @return string
         */
        public function __toString()
        {
        }
        protected function get_post_preview_object()
        {
        }
        protected function get_post_preview_id($query)
        {
        }
        /**
         * Initializes a Post
         * @internal
         * @param integer $pid
         */
        protected function init($pid = false)
        {
        }
        /**
         * Get the URL that will edit the current post/object
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @see Timber\Post::edit_link
         * @return bool|string
         */
        public function get_edit_url()
        {
        }
        /**
         * updates the post_meta of the current object with the given value
         * @param string $field
         * @param mixed $value
         */
        public function update($field, $value)
        {
        }
        /**
         * takes a mix of integer (post ID), string (post slug),
         * or object to return a WordPress post object from WP's built-in get_post() function
         * @internal
         * @param integer $pid
         * @return WP_Post on success
         */
        protected function prepare_post_info($pid = 0)
        {
        }
        /**
         * helps you find the post id regardless of whether you send a string or whatever
         * @param integer $pid ;
         * @internal
         * @return integer ID number of a post
         */
        protected function check_post_id($pid)
        {
        }
        /**
         * get_post_id_by_name($post_name)
         * @internal
         * @deprecated since 1.5.0
         * @param string $post_name
         * @return int
         */
        public static function get_post_id_by_name($post_name)
        {
        }
        /**
         * Gets a preview/excerpt of your post.
         *
         * If you have text defined in the excerpt textarea of your post, it will use that. Otherwise it
         * will pull from the post_content. If there's a `<!-- more -->` tag, it will use that to mark
         * where to pull through.
         *
         * This method returns a `Timber\PostPreview` object, which is a **chainable object**. This
         * means that you can change the output of the preview by **adding more methods**. Refer to the
         * [documentation of the `Timber\PostPreview` class](https://timber.github.io/docs/reference/timber-postpreview/)
         * to get an overview of all the available methods.
         *
         * @example
         * ```twig
         * {# Use default preview #}
         * <p>{{ post.preview }}</p>
         *
         * {# Change the post preview text #}
         * <p>{{ post.preview.read_more('Continue Reading') }}</p>
         *
         * {# Additionally restrict the length to 50 words #}
         * <p>{{ post.preview.length(50).read_more('Continue Reading') }}</p>
         * ```
         * @see \Timber\PostPreview
         * @return \Timber\PostPreview
         */
        public function preview()
        {
        }
        /**
         * get a preview of your post, if you have an excerpt it will use that,
         * otherwise it will pull from the post_content.
         * If there's a <!-- more --> tag it will use that to mark where to pull through.
         * @deprecated since 1.3.1, use {{ post.preview }} instead
         * @example
         * ```twig
         * <p>{{post.get_preview(50)}}</p>
         * ```
         * @param int $len The number of words that WP should use to make the tease. (Isn't this better than [this mess](http://wordpress.org/support/topic/changing-the-default-length-of-the_excerpt-1?replies=14)?). If you've set a post_excerpt on a post, we'll use that for the preview text; otherwise the first X words of the post_content
         * @param bool $force What happens if your custom post excerpt is longer then the length requested? By default (`$force = false`) it will use the full `post_excerpt`. However, you can set this to true to *force* your excerpt to be of the desired length
         * @param string $readmore The text you want to use on the 'readmore' link
         * @param bool|string $strip true for default, false for none, string for list of custom attributes
         * @param string $end The text to end the preview with (defaults to ...)
         * @return string of the post preview
         */
        public function get_preview($len = 50, $force = false, $readmore = 'Read More', $strip = true, $end = '&hellip;')
        {
        }
        /**
         * gets the post custom and attaches it to the current object
         * @internal
         * @param integer $pid a post ID number
         */
        public function import_custom($pid)
        {
        }
        /**
         * Used internally to fetch the metadata fields (wp_postmeta table)
         * and attach them to our TimberPost object
         * @internal
         * @param int $pid
         * @return array
         */
        protected function get_post_custom($pid)
        {
        }
        /**
         * @param int $i
         * @return string
         */
        protected static function get_wp_link_page($i)
        {
        }
        /**
         * Used internally by init, etc. to build TimberPost object
         * @internal
         * @param  int $pid
         * @return null|object|WP_Post
         */
        protected function get_info($pid)
        {
        }
        /**
         *
         * Gets the comment form for use on a single article page
         * @param array This $args array thing is a mess, [fix at some point](http://codex.wordpress.org/Function_Reference/comment_form)
         * @return string of HTML for the form
         */
        public function comment_form($args = array())
        {
        }
        /**
         * Gets the terms associated with the post.
         *
         * @api
         * @todo Remove deprecated parameters in 2.x
         * @example
         * ```twig
         * <section id="job-feed">
         * {% for post in job %}
         *     <div class="job">
         *         <h2>{{ post.title }}</h2>
         *         <p>{{ post.terms( {query:{taxonomy:'category', orderby:'name', order: 'ASC'}} )|join(', ') }}</p>
         *     </div>
         * {% endfor %}
         * </section>
         * ```
         * ```html
         * <section id="job-feed">
         *     <div class="job">
         *         <h2>Cheese Maker</h2>
         *         <p>Cheese, Food, Fromage</p>
         *     </div>
         *     <div class="job">
         *         <h2>Mime</h2>
         *         <p>Performance, Silence</p>
         *     </div>
         * </section>
         * ```
         * ```php
         * // Get all terms of a taxonomy.
         * $terms = $post->terms( 'category' );
         *
         * // Get terms of multiple taxonomies.
         * $terms = $post->terms( array( 'books', 'movies' ) );
         *
         * // Use custom arguments for taxonomy query and options.
         * $terms = $post->terms( array(
         *     'query' => [
         *         'taxonomy' => 'custom_tax',
         *         'orderby'  => 'count',
         *     ],
         *     'merge'      => false,
         *     'term_class' => 'My_Term_Class'
         * ) );
         * ```
         *
         * @param string|array $args {
         *     Optional. Name of the taxonomy or array of arguments.
         *
         *     @type array $query       Any array of term query parameters for getting the terms. See
         *                              `WP_Term_Query::__construct()` for supported arguments. Use the
         *                              `taxonomy` argument to choose which taxonomies to get. Defaults
         *                              to querying all registered taxonomies for the post type. You can
         *                              use custom or built-in WordPress taxonomies (category, tag).
         *                              Timber plays nice and figures out that `tag`, `tags` or
         *                              `post_tag` are all the same (also for `categories` or
         *                              `category`). For custom taxonomies you need to define the
         *                              proper name.
         *     @type bool $merge        Whether the resulting array should be one big one (`true`) or
         *                              whether it should be an array of sub-arrays for each taxonomy
         *                              (`false`). Default `true`.
         *     @type string $term_class The Timber term class to use for the term objects.
         * }
         * @param bool   $merge      Deprecated. Optional. See `$merge` argument in `$args` parameter.
         * @param string $term_class Deprecated. Optional. See `$term_class` argument in `$args`
         *                           parameter.
         * @return array An array of taxonomies.
         */
        public function terms($args = array(), $merge = true, $term_class = '')
        {
        }
        /**
         * @param string|int $term_name_or_id
         * @param string $taxonomy
         * @return bool
         */
        public function has_term($term_name_or_id, $taxonomy = 'all')
        {
        }
        /**
         * @return string
         */
        public function get_paged_content()
        {
        }
        /**
         * Returns the post_type object with labels and other info
         *
         * @deprecated since 1.0.4
         * @example
         *
         * ```twig
         * This post is from <span>{{ post.get_post_type.labels.plural }}</span>
         * ```
         *
         * ```html
         * This post is from <span>Recipes</span>
         * ```
         * @return PostType
         */
        public function get_post_type()
        {
        }
        /**
         * @return int the number of comments on a post
         */
        public function get_comment_count()
        {
        }
        /**
         * @param string $field_name
         * @return boolean
         */
        public function has_field($field_name)
        {
        }
        /**
         * Gets the field object data from Advanced Custom Fields.
         * This includes metadata on the field like whether it's conditional or not.
         *
         * @since 1.6.0
         * @param string $field_name of the field you want to lookup.
         * @return mixed
         */
        public function field_object($field_name)
        {
        }
        /**
         * @param string $field_name
         * @return mixed
         */
        public function get_field($field_name)
        {
        }
        /**
         * @param string $field_name
         */
        public function import_field($field_name)
        {
        }
        /**
         * Get the CSS classes for a post without cache. For usage you should use `{{post.class}}`
         * @internal
         * @param string $class additional classes you want to add
         * @example
         * ```twig
         * <article class="{{ post.post_class }}">
         *    {# Some stuff here #}
         * </article>
         * ```
         *
         * ```html
         * <article class="post-2612 post type-post status-publish format-standard has-post-thumbnail hentry category-data tag-charleston-church-shooting tag-dylann-roof tag-gun-violence tag-hate-crimes tag-national-incident-based-reporting-system">
         *    {# Some stuff here #}
         * </article>
         * ```
         * @return string a space-seperated list of classes
         */
        public function post_class($class = '')
        {
        }
        /**
         * Get the CSS classes for a post, but with caching css post classes. For usage you should use `{{ post.class }}` instead of `{{post.css_class}}` or `{{post.post_class}}`
         * @internal
         * @param string $class additional classes you want to add
         * @see Timber\Post::$_css_class
         * @example
         * ```twig
         * <article class="{{ post.class }}">
         *    {# Some stuff here #}
         * </article>
         * ```
         *
         * @return string a space-seperated list of classes
         */
        public function css_class($class = '')
        {
        }
        // Docs
        /**
         * @return array
         * @codeCoverageIgnore
         */
        public function get_method_values()
        {
        }
        /**
         * Return the author of a post
         * @api
         * @example
         * ```twig
         * <h1>{{post.title}}</h1>
         * <p class="byline">
         *     <a href="{{post.author.link}}">{{post.author.name}}</a>
         * </p>
         * ```
         * @return User|null A User object if found, false if not
         */
        public function author()
        {
        }
        public function authors()
        {
        }
        /**
         * Get the author (WordPress user) who last modified the post
         * @example
         * ```twig
         * Last updated by {{ post.modified_author.name }}
         * ```
         * ```html
         * Last updated by Harper Lee
         * ```
         * @return User|null A User object if found, false if not
         */
        public function modified_author()
        {
        }
        /**
         * Get the categoires on a particular post
         * @api
         * @return array of Timber\Terms
         */
        public function categories()
        {
        }
        /**
         * Returns a category attached to a post
         * @api
         * If mulitpuile categories are set, it will return just the first one
         * @return Timber\Term|null
         */
        public function category()
        {
        }
        /**
         * Returns an array of children on the post as Timber\Posts
         * (or other claass as you define).
         * @api
         * @example
         * ```twig
         * {% if post.children %}
         *     Here are the child pages:
         *     {% for child in post.children %}
         *         <a href="{{ child.link }}">{{ child.title }}</a>
         *     {% endfor %}
         * {% endif %}
         * ```
         * @param string|array $post_type _optional_ use to find children of a particular post type (attachment vs. page for example). You might want to restrict to certain types of children in case other stuff gets all mucked in there. You can use 'parent' to use the parent's post type or you can pass an array of post types.
         * @param string|bool $childPostClass _optional_ a custom post class (ex: 'MyTimber\Post') to return the objects as. By default (false) it will use Timber\Post::$post_class value.
         * @return array
         */
        public function children($post_type = 'any', $childPostClass = false)
        {
        }
        /**
         * Gets the comments on a `Timber\Post` and returns them as a `Timber\CommentThread`: a PHP
         * ArrayObject of [`Timber\Comment`](https://timber.github.io/docs/reference/timber-comment/)
         * (or whatever comment class you set).
         * @api
         *
         * @param int    $count        Set the number of comments you want to get. `0` is analogous to
         *                             "all".
         * @param string $order        Use ordering set in WordPress admin, or a different scheme.
         * @param string $type         For when other plugins use the comments table for their own
         *                             special purposes. Might be set to 'liveblog' or other, depending
         *                             on what’s stored in your comments table.
         * @param string $status       Could be 'pending', etc.
         * @param string $CommentClass What class to use when returning Comment objects. As you become a
         *                             Timber Pro, you might find yourself extending `Timber\Comment`
         *                             for your site or app (obviously, totally optional).
         * @see \Timber\CommentThread for an example with nested comments
         * @return bool|\Timber\CommentThread
         *
         * @example
         *
         * **single.twig**
         *
         * ```twig
         * <div id="post-comments">
         *   <h4>Comments on {{ post.title }}</h4>
         *   <ul>
         *     {% for comment in post.comments() %}
         *       {% include 'comment.twig' %}
         *     {% endfor %}
         *   </ul>
         *   <div class="comment-form">
         *     {{ function('comment_form') }}
         *   </div>
         * </div>
         * ```
         *
         * **comment.twig**
         *
         * ```twig
         * {# comment.twig #}
         * <li>
         *   <p class="comment-author">{{ comment.author.name }} says:</p>
         *   <div>{{ comment.content }}</div>
         * </li>
         * ```
         */
        public function comments($count = null, $order = 'wp', $type = 'comment', $status = 'approve', $CommentClass = 'Timber\\Comment')
        {
        }
        /**
         * If the Password form is to be shown, show it!
         * @return string|void
         */
        protected function maybe_show_password_form()
        {
        }
        /**
         *
         */
        protected function get_revised_data_from_method($method, $args = false)
        {
        }
        /**
         * Gets the actual content of a WP Post, as opposed to post_content this will run the hooks/filters attached to the_content. \This guy will return your posts content with WordPress filters run on it (like for shortcodes and wpautop).
         * @api
         * @example
         * ```twig
         * <div class="article">
         *     <h2>{{post.title}}</h2>
         *     <div class="content">{{ post.content }}</div>
         * </div>
         * ```
         * @param int $page
         * @return string
         */
        public function content($page = 0, $len = -1)
        {
        }
        /**
         * Handles for an circumstance with the Block editor where a "more" block has an option to
         * "Hide the excerpt on the full content page" which hides everything prior to the inserted
         * "more" block
         * @ticket #2218
         * @param string $content
         * @return string
         */
        protected function content_handle_no_teaser_block($content)
        {
        }
        /**
         * @return string
         */
        public function paged_content()
        {
        }
        /**
         * Get the date to use in your template!
         * @api
         * @example
         * ```twig
         * Published on {{ post.date }} // Uses WP's formatting set in Admin
         * OR
         * Published on {{ post.date('F jS') }} // Jan 12th
         * ```
         *
         * ```html
         * Published on January 12, 2015
         * OR
         * Published on Jan 12th
         * ```
         * @param string $date_format
         * @return string
         */
        public function date($date_format = '')
        {
        }
        /**
         * Get the time to use in your template
         * @api
         * @example
         * ```twig
         * Published at {{ post.time }} // Uses WP's formatting set in Admin
         * OR
         * Published at {{ post.time | time('G:i') }} // 13:25
         * ```
         *
         * ```html
         * Published at 1:25 pm
         * OR
         * Published at 13:25
         * ```
         * @param string $time_format
         * @return string
         */
        public function time($time_format = '')
        {
        }
        /**
         * Returns the post_type object with labels and other info
         *
         * @since 1.0.4
         * @example
         *
         * ```twig
         * This post is from <span>{{ post.type.labels.name }}</span>
         * ```
         *
         * ```html
         * This post is from <span>Recipes</span>
         * ```
         * @return PostType
         */
        public function type()
        {
        }
        /**
         * Returns the edit URL of a post if the user has access to it
         * @return bool|string the edit URL of a post in the WordPress admin
         */
        public function edit_link()
        {
        }
        /**
         * @api
         * @return mixed
         */
        public function format()
        {
        }
        /**
         * whether post requires password and correct password has been provided
         * @api
         * @return boolean
         */
        public function password_required()
        {
        }
        /**
         * get the permalink for a post object
         * @api
         * @example
         * ```twig
         * <a href="{{post.link}}">Read my post</a>
         * ```
         * @return string ex: http://example.org/2015/07/my-awesome-post
         */
        public function link()
        {
        }
        /**
         * @param string $field_name
         * @return mixed
         */
        public function meta($field_name = null)
        {
        }
        /**
         * @return string
         */
        public function name()
        {
        }
        /**
         * @param string $date_format
         * @return string
         */
        public function modified_date($date_format = '')
        {
        }
        /**
         * @param string $time_format
         * @return string
         */
        public function modified_time($time_format = '')
        {
        }
        /**
         * @api
         * @param bool $in_same_term
         * @return mixed
         */
        public function next($in_same_term = false)
        {
        }
        /**
         * Get a data array of pagination so you can navigate to the previous/next for a paginated post
         * @return array
         */
        public function pagination()
        {
        }
        /**
         * Finds any WP_Post objects and converts them to Timber\Posts
         * @param array|WP_Post $data
         * @param string $class
         */
        public function convert($data)
        {
        }
        /**
         * Gets the parent (if one exists) from a post as a Timber\Post object (or whatever is set in Timber\Post::$PostClass)
         * @api
         * @example
         * ```twig
         * Parent page: <a href="{{ post.parent.link }}">{{ post.parent.title }}</a>
         * ```
         * @return bool|Timber\Post
         */
        public function parent()
        {
        }
        /**
         * Gets the relative path of a WP Post, so while link() will return http://example.org/2015/07/my-cool-post
         * this will return just /2015/07/my-cool-post
         * @api
         * @example
         * ```twig
         * <a href="{{post.path}}">{{post.title}}</a>
         * ```
         * @return string
         */
        public function path()
        {
        }
        /**
         * Get the previous post in a set
         * @api
         * @example
         * ```twig
         * <h4>Prior Entry:</h4>
         * <h3>{{post.prev.title}}</h3>
         * <p>{{post.prev.get_preview(25)}}</p>
         * ```
         * @param bool $in_same_term
         * @return mixed
         */
        public function prev($in_same_term = false)
        {
        }
        /**
         * Gets the tags on a post, uses WP's post_tag taxonomy
         * @api
         * @return array
         */
        public function tags()
        {
        }
        /**
         * get the featured image as a Timber/Image
         * @api
         * @example
         * ```twig
         * <img src="{{ post.thumbnail.src }}" />
         * ```
         * @return Image|null of your thumbnail
         */
        public function thumbnail()
        {
        }
        /**
         * Returns the processed title to be used in templates. This returns the title of the post after WP's filters have run. This is analogous to `the_title()` in standard WP template tags.
         * @api
         * @example
         * ```twig
         * <h1>{{ post.title }}</h1>
         * ```
         * @return string
         */
        public function title()
        {
        }
        /**
         * Returns the gallery
         * @api
         * @example
         * ```twig
         * {{ post.gallery }}
         * ```
         * @return html
         */
        public function gallery($html = true)
        {
        }
        /**
         * Returns the audio
         * @api
         * @example
         * ```twig
         * {{ post.audio }}
         * ```
         * @return html
         */
        public function audio()
        {
        }
        /**
         * Returns the video
         * @api
         * @example
         * ```twig
         * {{ post.video }}
         * ```
         * @return html
         */
        public function video()
        {
        }
        /**
         *
         * ===================================
         * DEPRECATED FUNCTIONS LIVE DOWN HERE
         * ===================================
         *
         */
        /**
         * Get the categories for a post
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @see Timber\Post::categories
         * @return array of Timber\Terms
         */
        public function get_categories()
        {
        }
        /**
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @see Timber\Post::category
         * @return mixed
         */
        public function get_category()
        {
        }
        /**
         * @param string $field
         * @return Timber\Image
         */
        public function get_image($field)
        {
        }
        /**
         * Gets an array of tags for you to use
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @example
         * ```twig
         * <ul class="tags">
         *     {% for tag in post.tags %}
         *         <li>{{tag.name}}</li>
         *     {% endfor %}
         * </ul>
         * ```
         * @return array
         */
        public function get_tags()
        {
        }
        /**
         * Outputs the title with filters applied
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @example
         * ```twig
         * <h1>{{post.get_title}}</h1>
         * ```
         * ```html
         * <h1>Hello World!</h1>
         * ```
         * @return string
         */
        public function get_title()
        {
        }
        /**
         * Displays the content of the post with filters, shortcodes and wpautop applied
         * @example
         * ```twig
         * <div class="article-text">{{post.get_content}}</div>
         * ```
         * ```html
         * <div class="article-text"><p>Blah blah blah</p><p>More blah blah blah.</p></div>
         * ```
         * @param int $len
         * @param int $page
         * @return string
         */
        public function get_content($len = -1, $page = 0)
        {
        }
        /**
         * @internal
         * @deprecated since 1.0
         * @return mixed
         */
        public function get_format()
        {
        }
        /**
         * Get the terms associated with the post
         * This goes across all taxonomies by default
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @param string|array $tax What taxonom(y|ies) to pull from. Defaults to all registered taxonomies for the post type. You can use custom ones, or built-in WordPress taxonomies (category, tag). Timber plays nice and figures out that tag/tags/post_tag are all the same (and categories/category), for custom taxonomies you're on your own.
         * @param bool $merge Should the resulting array be one big one (true)? Or should it be an array of sub-arrays for each taxonomy (false)?
         * @return array
         */
        public function get_terms($tax = '', $merge = true, $TermClass = '')
        {
        }
        /**
         * @deprecated 0.20.0 use link() instead
         * @codeCoverageIgnore
         * @return string
         */
        public function permalink()
        {
        }
        /**
         * @internal
         * @see Timber\Post::date
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @param  string $date_format
         * @return string
         */
        public function get_date($date_format = '')
        {
        }
        /**
         * @internal
         * @see Timber\Post::modified_date
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @param  string $date_format
         * @return string
         */
        public function get_modified_date($date_format = '')
        {
        }
        /**
         * @internal
         * @param  string $time_format
         * @return string
         */
        public function get_modified_time($time_format = '')
        {
        }
        /**
         * @internal
         * @see Timber\Post::children
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @param string 		$post_type
         * @param bool|string 	$childPostClass
         * @return array
         */
        public function get_children($post_type = 'any', $childPostClass = false)
        {
        }
        /**
         * Get the permalink for a post, but as a relative path
         * For example, where {{post.link}} would return "http://example.org/2015/07/04/my-cool-post"
         * this will return the relative version: "/2015/07/04/my-cool-post"
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @return string
         */
        public function get_path()
        {
        }
        /**
         * Get the next post in WordPress's ordering
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @return TimberPost|boolean
         */
        public function get_prev($in_same_term = false)
        {
        }
        /**
         * Get the parent post of the post
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @return bool|TimberPost
         */
        public function get_parent()
        {
        }
        /**
         * Gets a User object from the author of the post
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @see Timber\Post::author
         * @return User|null
         */
        public function get_author()
        {
        }
        /**
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @return User|null
         */
        public function get_modified_author()
        {
        }
        /**
         * @internal
         * @see TimberPost::thumbnail
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @return Image|null
         */
        public function get_thumbnail()
        {
        }
        /**
         * @internal
         * @see TimberPost::link
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @return string
         */
        public function get_permalink()
        {
        }
        /**
         * get the permalink for a post object
         * In your templates you should use link:
         * <a href="{{post.link}}">Read my post</a>
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @return string
         */
        public function get_link()
        {
        }
        /**
         * Get the next post in WordPress's ordering
         * @internal
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @param bool $taxonomy
         * @return TimberPost|boolean
         */
        public function get_next($taxonomy = false)
        {
        }
        /**
         * Get a data array of pagination so you can navigate to the previous/next for a paginated post
         * @internal
         * @see Timber\Post::pagination();
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @return array
         */
        public function get_pagination()
        {
        }
        /**
         * Get the comments for a post
         * @internal
         * @see Timber\Post::comments
         * @param int $count
         * @param string $order
         * @param string $type
         * @param string $status
         * @param string $CommentClass
         * @return array|mixed
         */
        public function get_comments($count = null, $order = 'wp', $type = 'comment', $status = 'approve', $CommentClass = 'Timber\\Comment')
        {
        }
    }
    /**
     * If TimberPost is the class you're going to spend the most time, Timber\Image is the class you're going to have the most fun with.
     * @example
     * ```php
     * $context = Timber::context();
     * $post = new Timber\Post();
     * $context['post'] = $post;
     *
     * // lets say you have an alternate large 'cover image' for your post stored in a custom field which returns an image ID
     * $cover_image_id = $post->cover_image;
     * $context['cover_image'] = new Timber\Image($cover_image_id);
     * Timber::render('single.twig', $context);
     * ```
     *
     * ```twig
     * <article>
     * 	<img src="{{cover_image.src}}" class="cover-image" />
     * 	<h1 class="headline">{{post.title}}</h1>
     * 	<div class="body">
     * 		{{post.content}}
     * 	</div>
     *
     * 	<img src="{{ Image(post.custom_field_with_image_id).src }}" alt="Another way to initialize images as TimberImages, but within Twig" />
     * </article>
     * ```
     *
     * ```html
     * <article>
     * 	<img src="http://example.org/wp-content/uploads/2015/06/nevermind.jpg" class="cover-image" />
     * 	<h1 class="headline">Now you've done it!</h1>
     * 	<div class="body">
     * 		Whatever whatever
     * 	</div>
     * 	<img src="http://example.org/wp-content/uploads/2015/06/kurt.jpg" alt="Another way to initialize images as TimberImages, but within Twig" />
     * </article>
     * ```
     */
    class Image extends \Timber\Post implements \Timber\CoreInterface
    {
        protected $_can_edit;
        protected $_dimensions;
        public $abs_url;
        /**
         * @var string $object_type what does this class represent in WordPress terms?
         */
        public $object_type = 'image';
        /**
         * @var string $representation what does this class represent in WordPress terms?
         */
        public static $representation = 'image';
        /**
         * @api
         * @var string $file_loc the location of the image file in the filesystem (ex: `/var/www/htdocs/wp-content/uploads/2015/08/my-pic.jpg`)
         */
        public $file_loc;
        public $file;
        /**
         * @api
         * @var integer the ID of the image (which is a WP_Post)
         */
        public $id;
        public $sizes = array();
        /**
         * @api
         * @var string $caption the string stored in the WordPress database
         */
        public $caption;
        /**
         * @var $_wp_attached_file the file as stored in the WordPress database
         */
        protected $_wp_attached_file;
        /**
         * Creates a new Timber\Image object
         * @example
         * ```php
         * // You can pass it an ID number
         * $myImage = new Timber\Image(552);
         *
         * //Or send it a URL to an image
         * $myImage = new Timber\Image('http://google.com/logo.jpg');
         * ```
         * @param bool|int|string $iid
         */
        public function __construct($iid)
        {
        }
        /**
         * @return string the src of the file
         */
        public function __toString()
        {
        }
        /**
         * Get a PHP array with pathinfo() info from the file
         * @return array
         */
        public function get_pathinfo()
        {
        }
        /**
         * @internal
         * @param string $dim
         * @return array|int
         */
        protected function get_dimensions($dim)
        {
        }
        /**
         * Retrieve dimensions from SVG file
         *
         * @internal
         * @param string $svg SVG Path
         * @return array
         */
        protected function svgs_get_dimensions($svg)
        {
        }
        /**
         * @internal
         * @param string|null $dim
         * @return array|int
         */
        protected function get_dimensions_loaded($dim)
        {
        }
        /**
         * @return array
         */
        protected function get_post_custom($iid)
        {
        }
        /**
         * @internal
         * @param  int $iid the id number of the image in the WP database
         */
        protected function get_image_info($iid)
        {
        }
        /**
         * @internal
         * @param  string $url for evaluation
         * @return string with http/https corrected depending on what's appropriate for server
         */
        protected static function _maybe_secure_url($url)
        {
        }
        /**
         * Gets cached version of wp_upload_dir().
         *
         * Because wp_upload_dir() returns a different result for each site in a multisite, we shouldn’t
         * return the cached version when we switched to a different site in a multisite environment.
         *
         * @todo Deprecate this function in the future and use wp_upload_dir() directly.
         */
        public static function wp_upload_dir()
        {
        }
        /**
         * @internal
         * @param int|bool|string $iid
         */
        public function init($iid = false)
        {
        }
        /**
         * @internal
         * @param string $relative_path
         */
        protected function init_with_relative_path($relative_path)
        {
        }
        /**
         * @internal
         * @param string $file_path
         */
        protected function init_with_file_path($file_path)
        {
        }
        /**
         * @internal
         * @param string $url
         */
        protected function init_with_url($url)
        {
        }
        /**
         * @api
         * @example
         * ```twig
         * <img src="{{ image.src }}" alt="{{ image.alt }}" />
         * ```
         * ```html
         * <img src="http://example.org/wp-content/uploads/2015/08/pic.jpg" alt="W3 Checker told me to add alt text, so I am" />
         * ```
         * @return string alt text stored in WordPress
         */
        public function alt()
        {
        }
        /**
         * @api
         * @example
         * ```twig
         * {% if post.thumbnail.aspect < 1 %}
         *     {# handle vertical image #}
         *     <img src="{{ post.thumbnail.src|resize(300, 500) }}" alt="A basketball player" />
         * {% else %}
         * 	   <img src="{{ post.thumbnail.src|resize(500) }}" alt="A sumo wrestler" />
         * {% endif %}
         * ```
         * @return float
         */
        public function aspect()
        {
        }
        /**
         * @api
         * @example
         * ```twig
         * <img src="{{ image.src }}" height="{{ image.height }}" />
         * ```
         * ```html
         * <img src="http://example.org/wp-content/uploads/2015/08/pic.jpg" height="900" />
         * ```
         * @return int
         */
        public function height()
        {
        }
        /**
         * Returns the link to an image attachment's Permalink page (NOT the link for the image itself!!)
         * @api
         * @example
         * ```twig
         * <a href="{{ image.link }}"><img src="{{ image.src }} "/></a>
         * ```
         * ```html
         * <a href="http://example.org/my-cool-picture"><img src="http://example.org/wp-content/uploads/2015/whatever.jpg"/></a>
         * ```
         */
        public function link()
        {
        }
        /**
         * @api
         * @return bool|TimberPost
         */
        public function parent()
        {
        }
        /**
         * @api
         * @example
         * ```twig
         * <img src="{{ image.path }}" />
         * ```
         * ```html
         * <img src="/wp-content/uploads/2015/08/pic.jpg" />
         * ```
         * @return  string the /relative/path/to/the/file
         */
        public function path()
        {
        }
        /**
         * @param string $size a size known to WordPress (like "medium")
         * @api
         * @example
         * ```twig
         * <h1>{{ post.title }}</h1>
         * <img src="{{ post.thumbnail.src }}" />
         * ```
         * ```html
         * <img src="http://example.org/wp-content/uploads/2015/08/pic.jpg" />
         * ```
         * @return bool|string
         */
        public function src($size = 'full')
        {
        }
        /**
         * @param string $size a size known to WordPress (like "medium")
         * @api
         * @example
         * ```twig
         * <h1>{{ post.title }}</h1>
         * <img src="{{ post.thumbnail.src }}" srcset="{{ post.thumbnail.srcset }}" />
         * ```
         * ```html
         * <img src="http://example.org/wp-content/uploads/2018/10/pic.jpg" srcset="http://example.org/wp-content/uploads/2018/10/pic.jpg 1024w, http://example.org/wp-content/uploads/2018/10/pic-600x338.jpg 600w, http://example.org/wp-content/uploads/2018/10/pic-300x169.jpg 300w" />
         * ```
         *	@return bool|string
         */
        public function srcset($size = "full")
        {
        }
        /**
         * @param string $size a size known to WordPress (like "medium")
         * @api
         * @example
         * ```twig
         * <h1>{{ post.title }}</h1>
         * <img src="{{ post.thumbnail.src }}" srcset="{{ post.thumbnail.srcset }}" sizes="{{ post.thumbnail.img_sizes }}" />
         * ```
         * ```html
         * <img src="http://example.org/wp-content/uploads/2018/10/pic.jpg" srcset="http://example.org/wp-content/uploads/2018/10/pic.jpg 1024w, http://example.org/wp-content/uploads/2018/10/pic-600x338.jpg 600w, http://example.org/wp-content/uploads/2018/10/pic-300x169.jpg 300w sizes="(max-width: 1024px) 100vw, 102" />
         * ```
         *	@return bool|string
         */
        public function img_sizes($size = "full")
        {
        }
        /**
         * @internal
         * @return bool true if media is an image
         */
        protected function is_image()
        {
        }
        /**
         * @api
         * @example
         * ```twig
         * <img src="{{ image.src }}" width="{{ image.width }}" />
         * ```
         * ```html
         * <img src="http://example.org/wp-content/uploads/2015/08/pic.jpg" width="1600" />
         * ```
         * @return int
         */
        public function width()
        {
        }
        /**
         * @deprecated 0.21.9 use TimberImage::src
         * @codeCoverageIgnore
         * @internal
         * @param string $size
         * @return bool|string
         */
        public function get_src($size = '')
        {
        }
        /**
         * @deprecated since 0.21.9 use src() instead
         * @codeCoverageIgnore
         * @return string
         */
        public function url($size = '')
        {
        }
        /**
         * @deprecated since 0.21.9 use src() instead
         * @codeCoverageIgnore
         * @return string
         */
        public function get_url($size = '')
        {
        }
    }
}
namespace Timber\Image {
    /**
     * Each image filter is represented by a subclass of this class,m
     * and each filter call is a new instance, with call arguments as properties.
     * 
     * Only 3 methods need to be implemented:
     * - constructor, storing all filter arguments
     * - filename
     * - run
     */
    abstract class Operation
    {
        /**
         *
         * Builds the result filename, based on source filename and extension
         * 
         * @param  string $src_filename  source filename (excluding extension and path)
         * @param  string $src_extension source file extension
         * @return string                resulting filename (including extension but excluding path)
         *                               ex: my-awesome-file.jpg
         */
        public abstract function filename($src_filename, $src_extension);
        /**
         * Performs the actual image manipulation,
         * including saving the target file.
         * 
         * @param  string $load_filename filepath (not URL) to source file
         * @param  string $save_filename filepath (not URL) where result file should be saved
         * @return bool                  true if everything went fine, false otherwise
         */
        public abstract function run($load_filename, $save_filename);
        /**
         * Helper method to convert hex string to rgb array
         * 
         * @param  string $hexstr hex color string (like '#FF1455', 'FF1455', '#CCC', 'CCC')
         * @return array          array('red', 'green', 'blue') to int
         *                        ex: array('red' => 255, 'green' => 20, 'blue' => 85);
         */
        public static function hexrgb($hexstr)
        {
        }
        public static function rgbhex($r, $g, $b)
        {
        }
    }
}
namespace Timber\Image\Operation {
    /*
     * Changes image to new size, by shrinking/enlarging then padding with colored bands,
     * so that no part of the image is cropped or stretched.
     *
     * Arguments:
     * - width of new image
     * - height of new image
     * - color of padding
     */
    class Letterbox extends \Timber\Image\Operation
    {
        /**
         * @param int    $w     width of result image
         * @param int    $h     height
         * @param string $color hex string, for color of padding bands
         */
        public function __construct($w, $h, $color)
        {
        }
        /**
         * @param   string    $src_filename     the basename of the file (ex: my-awesome-pic)
         * @param   string    $src_extension    the extension (ex: .jpg)
         * @return  string    the final filename to be used
         *                    (ex: my-awesome-pic-lbox-300x200-FF3366.jpg)
         */
        public function filename($src_filename, $src_extension)
        {
        }
        /**
         * Performs the actual image manipulation,
         * including saving the target file.
         *
         * @param  string $load_filename filepath (not URL) to source file
         *                               (ex: /src/var/www/wp-content/uploads/my-pic.jpg)
         * @param  string $save_filename filepath (not URL) where result file should be saved
         *                               (ex: /src/var/www/wp-content/uploads/my-pic-lbox-300x200-FF3366.jpg)
         * @return bool                  true if everything went fine, false otherwise
         */
        public function run($load_filename, $save_filename)
        {
        }
    }
    /**
     * Changes image to new size, by shrinking/enlarging
     * then cropping to respect new ratio.
     *
     * Arguments:
     * - width of new image
     * - height of new image
     * - crop method
     */
    class Resize extends \Timber\Image\Operation
    {
        /**
         * @param int    $w    width of new image
         * @param int    $h    height of new image
         * @param string $crop cropping method, one of: 'default', 'center', 'top', 'bottom', 'left', 'right', 'top-center', 'bottom-center'.
         */
        public function __construct($w, $h, $crop)
        {
        }
        /**
         * @param   string    $src_filename     the basename of the file (ex: my-awesome-pic)
         * @param   string    $src_extension    the extension (ex: .jpg)
         * @return  string    the final filename to be used (ex: my-awesome-pic-300x200-c-default.jpg)
         */
        public function filename($src_filename, $src_extension)
        {
        }
        /**
         * Run a resize as animated GIF (if the server supports it)
         *
         * @param string           $load_filename the name of the file to resize.
         * @param string           $save_filename the desired name of the file to save.
         * @param \WP_Image_Editor $editor the image editor we're using.
         * @return bool
         */
        protected function run_animated_gif($load_filename, $save_filename, \WP_Image_Editor $editor)
        {
        }
        /**
         * @param \WP_Image_Editor $image
         */
        protected function get_target_sizes(\WP_Image_Editor $image)
        {
        }
        /**
         * Performs the actual image manipulation,
         * including saving the target file.
         *
         * @param  string $load_filename filepath (not URL) to source file
         *                               (ex: /src/var/www/wp-content/uploads/my-pic.jpg)
         * @param  string $save_filename filepath (not URL) where result file should be saved
         *                               (ex: /src/var/www/wp-content/uploads/my-pic-300x200-c-default.jpg)
         * @return boolean|null                  true if everything went fine, false otherwise
         */
        public function run($load_filename, $save_filename)
        {
        }
    }
    /**
     * Contains the class for running image retina-izing operations
     */
    /**
     * Increases image size by a given factor
     * Arguments:
     * - factor by which to multiply image dimensions
     * @property float $factor the factor (ex: 2, 1.5, 1.75) to multiply dimension by
     */
    class Retina extends \Timber\Image\Operation
    {
        /**
         * Construct our operation
         * @param float   $factor to multiply original dimensions by
         */
        public function __construct($factor)
        {
        }
        /**
         * Generates the final filename based on the source's name and extension
         *
         * @param   string    $src_filename     the basename of the file (ex: my-awesome-pic)
         * @param   string    $src_extension    the extension (ex: .jpg)
         * @return  string    the final filename to be used (ex: my-awesome-pic@2x.jpg)
         */
        public function filename($src_filename, $src_extension)
        {
        }
        /**
         * Performs the actual image manipulation,
         * including saving the target file.
         *
         * @param  string $load_filename filepath (not URL) to source file
         *                               (ex: /src/var/www/wp-content/uploads/my-pic.jpg)
         * @param  string $save_filename filepath (not URL) where result file should be saved
         *                               (ex: /src/var/www/wp-content/uploads/my-pic@2x.jpg)
         * @return bool                  true if everything went fine, false otherwise
         */
        public function run($load_filename, $save_filename)
        {
        }
    }
    /**
     * Implements converting a PNG file to JPG.
     * Argument:
     * - color to fill transparent zones
     */
    class ToJpg extends \Timber\Image\Operation
    {
        /**
         * @param string $color hex string of color to use for transparent zones
         */
        public function __construct($color)
        {
        }
        /**
         * @param   string    $src_filename     the basename of the file (ex: my-awesome-pic)
         * @param   string    $src_extension    ignored
         * @return  string    the final filename to be used (ex: my-awesome-pic.jpg)
         */
        public function filename($src_filename, $src_extension = 'jpg')
        {
        }
        /**
         * Performs the actual image manipulation,
         * including saving the target file.
         *
         * @param  string $load_filename filepath (not URL) to source file (ex: /src/var/www/wp-content/uploads/my-pic.jpg)
         * @param  string $save_filename filepath (not URL) where result file should be saved
         *                               (ex: /src/var/www/wp-content/uploads/my-pic.png)
         * @return bool                  true if everything went fine, false otherwise
         */
        public function run($load_filename, $save_filename)
        {
        }
    }
    /**
     * This class is used to process webp images. Not all server configurations support webp. 
     * If webp is not enabled, Timber will generate webp images instead
     * @codeCoverageIgnore
     */
    class ToWebp extends \Timber\Image\Operation
    {
        /**
         * @param string $quality  ranges from 0 (worst quality, smaller file) to 100 (best quality, biggest file)
         */
        public function __construct($quality)
        {
        }
        /**
         * @param   string    $src_filename     the basename of the file (ex: my-awesome-pic)
         * @param   string    $src_extension    ignored
         * @return  string    the final filename to be used (ex: my-awesome-pic.webp)
         */
        public function filename($src_filename, $src_extension = 'webp')
        {
        }
        /**
         * Performs the actual image manipulation,
         * including saving the target file.
         *
         * @param  string $load_filename filepath (not URL) to source file (ex: /src/var/www/wp-content/uploads/my-pic.webp)
         * @param  string $save_filename filepath (not URL) where result file should be saved
         *                               (ex: /src/var/www/wp-content/uploads/my-pic.webp)
         * @return bool                  true if everything went fine, false otherwise
         */
        public function run($load_filename, $save_filename)
        {
        }
    }
}
namespace Timber {
    /**
     * Implements the Twig image filters:
     * https://timber.github.io/docs/guides/cookbook-images/#arbitrary-resizing-of-images
     * - resize
     * - retina
     * - letterbox
     * - tojpg
     *
     * Implementation:
     * - public static functions provide the methods that are called by the filter
     * - most of the work is common to all filters (URL analysis, directory gymnastics, file caching, error management) and done by private static functions
     * - the specific part (actual image processing) is delegated to dedicated subclasses of TimberImageOperation
     */
    class ImageHelper
    {
        const BASE_UPLOADS = 1;
        const BASE_CONTENT = 2;
        static $home_url;
        public static function init()
        {
        }
        /**
         * Generates a new image with the specified dimensions.
         * New dimensions are achieved by cropping to maintain ratio.
         *
         * @api
         * @param string  		$src an URL (absolute or relative) to the original image
         * @param int|string	$w target width(int) or WordPress image size (WP-set or user-defined).
         * @param int     		$h target height (ignored if $w is WP image size). If not set, will ignore and resize based on $w only.
         * @param string  		$crop your choices are 'default', 'center', 'top', 'bottom', 'left', 'right'
         * @param bool    		$force
         * @example
         * ```twig
         * <img src="{{ image.src | resize(300, 200, 'top') }}" />
         * ```
         * ```html
         * <img src="http://example.org/wp-content/uploads/pic-300x200-c-top.jpg" />
         * ```
         * @return string (ex: )
         */
        public static function resize($src, $w, $h = 0, $crop = 'default', $force = false)
        {
        }
        /**
         * Generates a new image with increased size, for display on Retina screens.
         *
         * @param string  $src
         * @param float   $multiplier
         * @param boolean $force
         *
         * @return string url to the new image
         */
        public static function retina_resize($src, $multiplier = 2, $force = false)
        {
        }
        /**
         * checks to see if the given file is an aimated gif
         * @param  string  $file local filepath to a file, not a URL
         * @return boolean true if it's an animated gif, false if not
         */
        public static function is_animated_gif($file)
        {
        }
        /**
         * Checks if file is an SVG.
         *
         * @param string $file_path File path to check.
         * @return bool True if SVG, false if not SVG or file doesn't exist.
         */
        public static function is_svg($file_path)
        {
        }
        /**
         * Generate a new image with the specified dimensions.
         * New dimensions are achieved by adding colored bands to maintain ratio.
         *
         * @param string  $src
         * @param int     $w
         * @param int     $h
         * @param string  $color
         * @param bool    $force
         * @return string
         */
        public static function letterbox($src, $w, $h, $color = false, $force = false)
        {
        }
        /**
         * Generates a new image by converting the source GIF or PNG into JPG
         *
         * @param string  $src   a url or path to the image (http://example.org/wp-content/uploads/2014/image.jpg) or (/wp-content/uploads/2014/image.jpg)
         * @param string  $bghex
         * @return string
         */
        public static function img_to_jpg($src, $bghex = '#FFFFFF', $force = false)
        {
        }
        /**
         * Generates a new image by converting the source into WEBP if supported by the server
         *
         * @param string  $src      a url or path to the image (http://example.org/wp-content/uploads/2014/image.webp)
         *							or (/wp-content/uploads/2014/image.jpg)
         *							If webp is not supported, a jpeg image will be generated
         * @param int     $quality  ranges from 0 (worst quality, smaller file) to 100 (best quality, biggest file)
         * @param bool    $force
         */
        public static function img_to_webp($src, $quality = 80, $force = false)
        {
        }
        //-- end of public methods --//
        /**
         * Deletes all resized versions of an image when the source is deleted.
         *
         * @since 1.5.0
         * @param int   $post_id an attachment post id
         */
        public static function delete_attachment($post_id)
        {
        }
        /**
         * Delete all resized version of an image when its meta data is regenerated.
         *
         * @since 1.5.0
         * @param array $metadata
         * @param int   $post_id an attachment post id
         * @return array
         */
        public static function generate_attachment_metadata($metadata, $post_id)
        {
        }
        /**
         * Adds a 'relative' key to wp_upload_dir() result.
         * It will contain the relative url to upload dir.
         *
         * @since 1.5.0
         * @param array $arr
         * @return array
         */
        public static function add_relative_upload_dir_key($arr)
        {
        }
        /**
         * Checks if attachment is an image before deleting generated files
         *
         * @param  int  $post_id   an attachment post id
         *
         */
        public static function _delete_generated_if_image($post_id)
        {
        }
        /**
         * Deletes the auto-generated files for resize and letterboxing created by Timber
         * @param string  $local_file   ex: /var/www/wp-content/uploads/2015/my-pic.jpg
         *	                            or: http://example.org/wp-content/uploads/2015/my-pic.jpg
         */
        static function delete_generated_files($local_file)
        {
        }
        /**
         * Deletes resized versions of the supplied file name.
         * So if passed a value like my-pic.jpg, this function will delete my-pic-500x200-c-left.jpg, my-pic-400x400-c-default.jpg, etc.
         *
         * keeping these here so I know what the hell we're matching
         * $match = preg_match("/\/srv\/www\/wordpress-develop\/src\/wp-content\/uploads\/2014\/05\/$filename-[0-9]*x[0-9]*-c-[a-z]*.jpg/", $found_file);
         * $match = preg_match("/\/srv\/www\/wordpress-develop\/src\/wp-content\/uploads\/2014\/05\/arch-[0-9]*x[0-9]*-c-[a-z]*.jpg/", $filename);
         *
         * @param string 	$filename   ex: my-pic
         * @param string 	$ext ex: jpg
         * @param string 	$dir var/www/wp-content/uploads/2015/
         * @param string 	$search_pattern pattern of files to pluck from
         * @param string 	$match_pattern pattern of files to go forth and delete
         */
        protected static function process_delete_generated_files($filename, $ext, $dir, $search_pattern, $match_pattern = null)
        {
        }
        /**
         * Determines the filepath corresponding to a given URL
         *
         * @param string  $url
         * @return string
         */
        public static function get_server_location($url)
        {
        }
        /**
         * Determines the filepath where a given external file will be stored.
         *
         * @param string  $file
         * @return string
         */
        public static function get_sideloaded_file_loc($file)
        {
        }
        /**
         * downloads an external image to the server and stores it on the server
         *
         * @param string  $file the URL to the original file
         * @return string the URL to the downloaded file
         */
        public static function sideload_image($file)
        {
        }
        /**
         * Takes in an URL and breaks it into components,
         * that will then be used in the different steps of image processing.
         * The image is expected to be either part of a theme, plugin, or an upload.
         *
         * @param  string $url an URL (absolute or relative) pointing to an image
         * @return array       an array (see keys in code below)
         */
        public static function analyze_url($url)
        {
        }
        /**
         * Converts a URL located in a theme directory into the raw file path
         * @param string 	$src a URL (http://example.org/wp-content/themes/twentysixteen/images/home.jpg)
         * @return string full path to the file in question
         */
        static function theme_url_to_dir($src)
        {
        }
        /**
         * Checks if uploaded image is located in theme.
         *
         * @param string $path image path.
         * @return bool     If the image is located in the theme directory it returns true.
         *                  If not or $path doesn't exits it returns false.
         */
        protected static function is_in_theme_dir($path)
        {
        }
        /**
         * Runs realpath to resolve symbolic links (../, etc). But only if it's a path and not a URL
         * @param  string $path
         * @return string 			the resolved path
         */
        protected static function maybe_realpath($path)
        {
        }
        // -- the below methods are just used for unit testing the URL generation code
        //
        public static function get_letterbox_file_url($url, $w, $h, $color)
        {
        }
        public static function get_letterbox_file_path($url, $w, $h, $color)
        {
        }
        public static function get_resize_file_url($url, $w, $h, $crop)
        {
        }
        public static function get_resize_file_path($url, $w, $h, $crop)
        {
        }
    }
    /**
     * This is for integrating external plugins into timber
     * @package  timber
     */
    class Integrations
    {
        var $acf;
        var $coauthors_plus;
        public function __construct()
        {
        }
        public function init()
        {
        }
        public function maybe_init_integrations()
        {
        }
    }
}
namespace Timber\Integrations {
    /**
     * Class used to handle integration with Advanced Custom Fields
     */
    class ACF
    {
        public function __construct()
        {
        }
        public function post_get_meta($customs, $post_id)
        {
        }
        public function post_get_meta_field($value, $post_id, $field_name)
        {
        }
        public function post_meta_object($value, $post_id, $field_name)
        {
        }
        public function term_get_meta_field($value, $term_id, $field_name, $term)
        {
        }
        public function term_set_meta($value, $field, $term_id, $term)
        {
        }
        public function term_get_meta($fields, $term_id, $term)
        {
        }
        public function user_get_meta($fields, $user_id)
        {
        }
        public function user_get_meta_field($value, $uid, $field)
        {
        }
    }
    class CoAuthorsPlus
    {
        public static $prefer_gravatar = false;
        /**
         * @codeCoverageIgnore
         */
        public function __construct()
        {
        }
        /**
         * Filters {{ post.authors }} to return authors stored from Co-Authors Plus
         * @since 1.1.4
         * @param array $author
         * @param Post $post
         * @return array of User objects
         */
        public function authors($author, $post)
        {
        }
        /**
         * return the user id for normal authors
         * the user login for guest authors if it exists and self::prefer_users == true
         * or null
         * @internal
         * @param object $cauthor
         * @return int|string|null
         */
        protected function get_user_uid($cauthor)
        {
        }
    }
}
namespace Timber {
    /**
     * This is used in Timber to represent users retrived from WordPress. You can call `$my_user = new Timber\User(123);` directly, or access it through the `{{ post.author }}` method.
     * @example
     * ```php
     * $context['current_user'] = new Timber\User();
     * $context['post'] = new Timber\Post();
     * Timber::render('single.twig', $context);
     * ```
     * ```twig
     * <p class="current-user-info">Your name is {{ current_user.name }}</p>
     * <p class="article-info">This article is called "{{ post.title }}" and it's by {{ post.author.name }}
     * ```
     * ```html
     * <p class="current-user-info">Your name is Jesse Eisenberg</p>
     * <p class="article-info">This article is called "Consider the Lobster" and it's by David Foster Wallace
     * ```
     */
    class User extends \Timber\Core implements \Timber\CoreInterface
    {
        public $object_type = 'user';
        public static $representation = 'user';
        public $_link;
        /**
         * @api
         * @var string A URL to an avatar that overrides anything from Gravatar, etc.
         */
        public $avatar_override;
        /**
         * @api
         * @var string The description from WordPress
         */
        public $description;
        public $display_name = '';
        /**
         * @api
         * @var  string The first name of the user
         */
        public $first_name;
        /**
         * @api
         * @var  string The last name of the user
         */
        public $last_name;
        /**
         * @api
         * @var int The ID from WordPress
         */
        public $id;
        public $user_nicename;
        /**
         * The roles the user is part of.
         *
         * @api
         * @since 1.8.5
         *
         * @var array
         */
        protected $roles;
        /**
         * @param object|int|bool $uid
         */
        public function __construct($uid = false)
        {
        }
        /**
         * @example
         * ```twig
         * This post is by {{ post.author }}
         * ```
         * ```html
         * This post is by Jared Novack
         * ```
         *
         * @return string a fallback for TimberUser::name()
         */
        public function __toString()
        {
        }
        /**
         * @internal
         * @param string $field_name
         * @return null
         */
        public function get_meta($field_name)
        {
        }
        /**
         * @internal
         * @param string 	$field
         * @param mixed 	$value
         */
        public function __set($field, $value)
        {
        }
        /**
         * @internal
         * @param object|int|bool $uid The user ID to use
         */
        protected function init($uid = false)
        {
        }
        /**
         * @param string $field_name
         * @return mixed
         */
        public function get_meta_field($field_name)
        {
        }
        /**
         * @return array|null
         */
        public function get_custom()
        {
        }
        /**
         * @api
         * @return string http://example.org/author/lincoln
         */
        public function link()
        {
        }
        /**
         * @api
         * @return string the human-friendly name of the user (ex: "Buster Bluth")
         */
        public function name()
        {
        }
        /**
         * @param string $field_name
         * @return mixed
         */
        public function meta($field_name)
        {
        }
        /**
         * @api
         * @return string ex: /author/lincoln
         */
        public function path()
        {
        }
        /**
         * @api
         * @return string ex baberaham-lincoln
         */
        public function slug()
        {
        }
        /**
         * Creates an associative array with user role slugs and their translated names.
         *
         * @internal
         * @since 1.8.5
         * @param array $roles user roles.
         * @return array|null
         */
        protected function get_roles($roles)
        {
        }
        /**
         * Gets the user roles.
         * Roles shouldn’t be used to check whether a user has a capability. Use roles only for
         * displaying purposes. For example, if you want to display the name of the subscription a user
         * has on the site behind a paywall.
         *
         * If you want to check for capabilities, use `{{ user.can('capability') }}`. If you only want
         * to check whether a user is logged in, you can use `{% if user %}`.
         *
         * @api
         * @since 1.8.5
         * @example
         * ```twig
         * <h2>Role name</h2>
         * {% for role in post.author.roles %}
         *     {{ role }}
         * {% endfor %}
         * ```
         * ```twig
         * <h2>Role name</h2>
         * {{ post.author.roles|join(', ') }}
         * ```
         * ```twig
         * {% for slug, name in post.author.roles %}
         *     {{ slug }}
         * {% endfor %}
         * ```
         *
         * @return array|null
         */
        public function roles()
        {
        }
        /**
         * Checks whether a user has a capability.
         *
         * Don’t use role slugs for capability checks. While checking against a role in place of a
         * capability is supported in part, this practice is discouraged as it may produce unreliable
         * results. This includes cases where you want to check whether a user is registered. If you
         * want to check whether a user is a Subscriber, use `{{ user.can('read') }}`. If you only want
         * to check whether a user is logged in, you can use `{% if user %}`.
         *
         * @api
         * @since 1.8.5
         *
         * @param string $capability The capability to check.
         *
         * @example
         * Give moderation users another CSS class to style them differently.
         *
         * ```twig
         * <span class="comment-author {{ comment.author.can('moderate_comments') ? 'comment-author--is-moderator }}">
         *     {{ comment.author.name }}
         * </span>
         * ```
         *
         * @return bool Whether the user has the capability.
         */
        public function can($capability)
        {
        }
        /**
         * Gets a user’s avatar URL.
         *
         * @api
         * @since 1.9.1
         * @example
         * Get a user avatar with a width and height of 150px:
         *
         * ```twig
         * <img src="{{ post.author.avatar({ size: 150 }) }}">
         * ```
         *
         * @param null|array $args Parameters for
         *                         [`get_avatar_url()`](https://developer.wordpress.org/reference/functions/get_avatar_url/).
         * @return string|\Timber\Image The avatar URL.
         */
        public function avatar($args = null)
        {
        }
    }
}
namespace Timber\Integrations {
    class CoAuthorsPlusUser extends \Timber\User
    {
        /**
         * @param object $author co-author object
         */
        public function __construct($author)
        {
        }
        /**
         * @internal
         * @param false|object $coauthor co-author object
         */
        protected function init($coauthor = false)
        {
        }
    }
    /**
     * These are methods that can be executed by WPCLI, other CLI mechanism or other external controllers
     * @package  timber
     */
    class Command
    {
        public static function clear_cache($mode = 'all')
        {
        }
        public static function clear_cache_timber()
        {
        }
        public static function clear_cache_twig()
        {
        }
    }
    class Timber_WP_CLI_Command extends \WP_CLI_Command
    {
        /**
         * Clears Timber and Twig's Cache
         *
         * ## EXAMPLES
         *
         *    wp timber clear_cache
         *
         */
        public function clear_cache($mode = 'all')
        {
        }
        /**
         * Clears Twig's Cache
         *
         * ## EXAMPLES
         *
         *    wp timber clear_cache_twig
         *
         */
        public function clear_cache_twig()
        {
        }
        /**
         * Clears Timber's Cache
         *
         * ## EXAMPLES
         *
         *    wp timber clear_cache_timber
         *
         */
        public function clear_cache_timber()
        {
        }
    }
    class WPML
    {
        public function __construct()
        {
        }
        public function file_system_to_url($url)
        {
        }
    }
}
namespace Timber {
    class Loader
    {
        const CACHEGROUP = 'timberloader';
        const TRANS_KEY_LEN = 50;
        const CACHE_NONE = 'none';
        const CACHE_OBJECT = 'cache';
        const CACHE_TRANSIENT = 'transient';
        const CACHE_SITE_TRANSIENT = 'site-transient';
        const CACHE_USE_DEFAULT = 'default';
        public static $cache_modes = array(self::CACHE_NONE, self::CACHE_OBJECT, self::CACHE_TRANSIENT, self::CACHE_SITE_TRANSIENT);
        protected $cache_mode = self::CACHE_TRANSIENT;
        protected $locations;
        /**
         * @param bool|string   $caller the calling directory or false
         */
        public function __construct($caller = false)
        {
        }
        /**
         * @param string        	$file
         * @param array         	$data
         * @param array|boolean    	$expires (array for options, false for none, integer for # of seconds)
         * @param string        	$cache_mode
         * @return bool|string
         */
        public function render($file, $data = null, $expires = false, $cache_mode = self::CACHE_USE_DEFAULT)
        {
        }
        protected function delete_cache()
        {
        }
        /**
         * Get first existing template.
         *
         * @param array|string $templates  Name(s) of the Twig template(s) to choose from.
         * @return string|bool             Name of chosen template, otherwise false.
         */
        public function choose_template($templates)
        {
        }
        /**
         * @param string $name
         * @return bool
         * @deprecated 1.3.5 No longer used internally
         * @todo remove in 2.x
         * @codeCoverageIgnore
         */
        protected function template_exists($name)
        {
        }
        /**
         * @return \Twig\Loader\FilesystemLoader
         */
        public function get_loader()
        {
        }
        /**
         * @return \Twig\Environment
         */
        public function get_twig()
        {
        }
        public function clear_cache_timber($cache_mode = self::CACHE_USE_DEFAULT)
        {
        }
        protected static function clear_cache_timber_database()
        {
        }
        protected static function clear_cache_timber_object()
        {
        }
        public function clear_cache_twig()
        {
        }
        /**
         * Remove a directory and everything inside
         *
         * @param string|false $dirPath
         */
        public static function rrmdir($dirPath)
        {
        }
        /**
         * @param string $key
         * @param string $group
         * @param string $cache_mode
         * @return bool
         */
        public function get_cache($key, $group = self::CACHEGROUP, $cache_mode = self::CACHE_USE_DEFAULT)
        {
        }
        /**
         * @param string $key
         * @param string|boolean $value
         * @param string $group
         * @param integer $expires
         * @param string $cache_mode
         * @return string|boolean
         */
        public function set_cache($key, $value, $group = self::CACHEGROUP, $expires = 0, $cache_mode = self::CACHE_USE_DEFAULT)
        {
        }
    }
    class LocationManager
    {
        /**
         * @param bool|string   $caller the calling directory (or false)
         * @return array
         */
        public static function get_locations($caller = false)
        {
        }
        /**
         * @return array
         */
        protected static function get_locations_theme()
        {
        }
        /**
         * Get calling script file.
         * @api
         * @param int     $offset
         * @return string|null
         */
        public static function get_calling_script_file($offset = 0)
        {
        }
        /**
         * Get calling script dir.
         * @api
         * @return string
         */
        public static function get_calling_script_dir($offset = 0)
        {
        }
        /**
         * returns an array of the directory inside themes that holds twig files
         * @return string[] the names of directores, ie: array('templats', 'views');
         */
        public static function get_locations_theme_dir()
        {
        }
        /**
         *
         * @return array
         */
        protected static function get_locations_user()
        {
        }
        /**
         * @param bool|string   $caller the calling directory
         * @return array
         */
        protected static function get_locations_caller($caller = false)
        {
        }
    }
    /**
     * Terms: WordPress has got 'em, you want 'em. Categories. Tags. Custom
     * Taxonomies. You don't care, you're a fiend. Well let's get this under control:
     * @example
     * ```php
     * //Get a term by its ID
     * $context['term'] = new Timber\Term(6);
     * //Get a term when on a term archive page
     * $context['term_page'] = new Timber\Term();
     * //Get a term with a slug
     * $context['team'] = new Timber\Term('patriots');
     * //Get a team with a slug from a specific taxonomy
     * $context['st_louis'] = new Timber\Term('cardinals', 'baseball');
     * Timber::render('index.twig', $context);
     * ```
     * ```twig
     * <h2>{{term_page.name}} Archives</h2>
     * <h3>Teams</h3>
     * <ul>
     *     <li>{{st_louis.name}} - {{st_louis.description}}</li>
     *     <li>{{team.name}} - {{team.description}}</li>
     * </ul>
     * ```
     * ```html
     * <h2>Team Archives</h2>
     * <h3>Teams</h3>
     * <ul>
     *     <li>St. Louis Cardinals - Winner of 11 World Series</li>
     *     <li>New England Patriots - Winner of 6 Super Bowls</li>
     * </ul>
     * ```
     */
    class Term extends \Timber\Core implements \Timber\CoreInterface
    {
        public $PostClass = 'Timber\\Post';
        public $TermClass = 'Term';
        public $object_type = 'term';
        public static $representation = 'term';
        public $_children;
        /**
         * @api
         * @var string the human-friendly name of the term (ex: French Cuisine)
         */
        public $name;
        /**
         * @api
         * @var string the WordPress taxonomy slug (ex: `post_tag` or `actors`)
         */
        public $taxonomy;
        /**
         * @param int $tid
         * @param string $tax
         */
        public function __construct($tid = null, $tax = '')
        {
        }
        /**
         * @return string
         */
        public function __toString()
        {
        }
        /**
         * @param $tid
         * @param $taxonomy
         *
         * @return static
         */
        public static function from($tid, $taxonomy)
        {
        }
        /* Setup
        	===================== */
        /**
         * @internal
         * @return integer
         */
        protected function get_term_from_query()
        {
        }
        /**
         * @internal
         * @param int $tid
         */
        protected function init($tid)
        {
        }
        /**
         * @internal
         * @param int $tid
         * @return array
         */
        protected function get_term_meta($tid)
        {
        }
        /**
         * @internal
         * @param int|object|array $tid
         * @return mixed
         */
        protected function get_term($tid)
        {
        }
        /**
         * @internal
         * @param int $tid
         * @return int|array
         */
        protected static function get_tid($tid)
        {
        }
        /* Public methods
        	===================== */
        /**
         * @internal
         * @return string
         */
        public function get_edit_url()
        {
        }
        /**
         * @internal
         * @param string $field_name
         * @return string
         */
        public function get_meta_field($field_name)
        {
        }
        /**
         * @internal
         * @deprecated since 1.0
         * @return string
         */
        public function get_path()
        {
        }
        /**
         * @internal
         * @deprecated since 1.0
         * @return string
         */
        public function get_link()
        {
        }
        /**
         * Get posts that have the current term assigned.
         *
         * @internal
         * @param int|array $numberposts_or_args Optional. Either the number of posts or an array of
         *                                       arguments for the post query that this method is going.
         *                                       to perform. Default `10`.
         * @param string    $post_type_or_class  Optional. Either the post type to get or the name of
         *                                       post class to use for the returned posts. Default
         *                                       `any`.
         * @param string    $post_class          Optional. The name of the post class to use for the
         *                                       returned posts. Default `Timber\Post`.
         * @return array|bool|null
         */
        public function get_posts($numberposts_or_args = 10, $post_type_or_class = 'any', $post_class = '')
        {
        }
        /**
         * @internal
         * @return array
         */
        public function get_children()
        {
        }
        /**
         *
         *
         * @param string  $key
         * @param mixed   $value
         */
        public function update($key, $value)
        {
        }
        /* Alias
        	====================== */
        /**
         * @api
         * @return array
         */
        public function children()
        {
        }
        /**
         * @api
         * @return string
         */
        public function description()
        {
        }
        /**
         * @api
         * @return string
         */
        public function edit_link()
        {
        }
        /**
         * Returns a full link to the term archive page like
         * `http://example.com/category/news`
         * @api
         * @example
         * ```twig
         * See all posts in: <a href="{{ term.link }}">{{ term.name }}</a>
         * ```
         * @return string
         */
        public function link()
        {
        }
        /**
         * Retrieves and outputs meta information stored with a term. This will use
         * both data stored under (old) ACF hacks and new (WP 4.6+) where term meta
         * has its own table. If retrieving a special ACF field (repeater, etc.) you
         * can use the output immediately in Twig — no further processing is
         * required.
         * @api
         * @param string $field_name
         * @example
         * ```twig
         * <div class="location-info">
         *   <h2>{{ term.name }}</h2>
         *   <p>{{ term.meta('address') }}</p>
         * </div>
         * ```
         * @return string
         */
        public function meta($field_name)
        {
        }
        /**
         * Returns a relative link (path) to the term archive page like
         * `/category/news`
         * @api
         * @example
         * ```twig
         * See all posts in: <a href="{{ term.path }}">{{ term.name }}</a>
         * ```
         * @return string
         */
        public function path()
        {
        }
        /**
         * Gets posts that have the current term assigned.
         *
         * @api
         * @example
         * ```twig
         * <h4>Recent posts in {{ term.name }}</h4>
         *
         * <ul>
         * {% for post in term.posts(3, 'post') %}
         *     <li>
         *         <a href="{{ post.link }}">{{ post.title }}</a>
         *     </li>
         * {% endfor %}
         * </ul>
         * ```
         *
         * If you need more control over the query that is going to be performed, you can pass your
         * custom query arguments in the first parameter.
         *
         * ```twig
         * <h4>Our branches in {{ region.name }}</h4>
         *
         * <ul>
         * {% for branch in region.posts({
         *     posts_per_page: -1,
         *     orderby: 'menu_order'
         * }, 'branch', 'Branch') %}
         *     <li>
         *         <a href="{{ branch.link }}">{{ branch.title }}</a>
         *     </li>
         * {% endfor %}
         * </ul>
         * ```
         *
         * @param int|array $numberposts_or_args Optional. Either the number of posts or an array of
         *                                       arguments for the post query that this method is going.
         *                                       to perform. Default `10`.
         * @param string $post_type_or_class     Optional. Either the post type to get or the name of
         *                                       post class to use for the returned posts. Default
         *                                       `any`.
         * @param string $post_class             Optional. The name of the post class to use for the
         *                                       returned posts. Default `Timber\Post`.
         * @return array|bool|null
         */
        public function posts($numberposts_or_args = 10, $post_type_or_class = 'any', $post_class = '')
        {
        }
        /**
         * @api
         * @return string
         */
        public function title()
        {
        }
    }
    class Menu extends \Timber\Term
    {
        public $MenuItemClass = 'Timber\\MenuItem';
        public $PostClass = 'Timber\\Post';
        /**
         * @api
         * @var integer The depth of the menu we are rendering
         */
        public $depth;
        /**
         * @api
         * @var array|null Array of `Timber\Menu` objects you can to iterate through.
         */
        public $items = null;
        /**
         * @api
         * @var integer The ID of the menu, corresponding to the wp_terms table.
         */
        public $id;
        /**
         * @api
         * @var integer The ID of the menu, corresponding to the wp_terms table.
         */
        public $ID;
        /**
         * @api
         * @var integer The ID of the menu, corresponding to the wp_terms table.
         */
        public $term_id;
        /**
         * @api
         * @var string The name of the menu (ex: `Main Navigation`).
         */
        public $name;
        /**
         * @api
         * @var string The name of the menu (ex: `Main Navigation`).
         */
        public $title;
        /**
         * Menu options.
         *
         * @api
         * @since 1.9.6
         * @var array An array of menu options.
         */
        public $options;
        /**
         * @api
         * @var array The unfiltered options sent forward via the user in the __construct
         */
        public $raw_options;
        /**
         * Theme Location.
         *
         * @api
         * @since 1.9.6
         * @var string The theme location of the menu, if available.
         */
        public $theme_location = null;
        /**
         * Initialize a menu.
         *
         * @param int|string $slug    A menu slug, the term ID of the menu, the full name from the admin
         *                            menu, the slug of the registered location or nothing. Passing
         *                            nothing is good if you only have one menu. Timber will grab what
         *                            it finds.
         * @param array      $options Optional. An array of options. Right now, only the `depth` is
         *                            supported which says how many levels of hierarchy should be
         *                            included in the menu. Default `0`, which is all levels.
         */
        public function __construct($slug = 0, $options = array())
        {
        }
        /**
         * @internal
         * @param int $menu_id
         */
        protected function init($menu_id)
        {
        }
        /**
         * @internal
         */
        protected function init_as_page_menu()
        {
        }
        /**
         * @internal
         * @param string $slug
         * @param array $locations
         * @return integer
         */
        protected function get_menu_id_from_locations($slug, $locations)
        {
        }
        /**
         * @internal
         * @param int $slug
         * @return int
         */
        protected function get_menu_id_from_terms($slug = 0)
        {
        }
        /**
         * Find a parent menu item in a set of menu items.
         *
         * @api
         * @param array $menu_items An array of menu items.
         * @param int   $parent_id  The parent ID to look for.
         * @return \Timber\MenuItem|bool A menu item. False if no parent was found.
         */
        public function find_parent_item_in_menu($menu_items, $parent_id)
        {
        }
        /**
         * @internal
         * @param array $items
         * @return array
         */
        protected function order_children($items)
        {
        }
        /**
         * @internal
         * @param object $item the WP menu item object to wrap
         * @return mixed an instance of the user-configured $MenuItemClass
         */
        protected function create_menu_item($item)
        {
        }
        /**
         * @internal
         * @param array $menu
         */
        protected function strip_to_depth_limit($menu, $current = 1)
        {
        }
        /**
         * Get menu items.
         *
         * Instead of using this function, you can use the `$items` property directly to get the items
         * for a menu.
         *
         * @api
         * @example
         * ```twig
         * {% for item in menu.get_items %}
         *     <a href="{{ item.link }}">{{ item.title }}</a>
         * {% endfor %}
         * ```
         * @return array Array of `Timber\MenuItem` objects. Empty array if no items could be found.
         */
        public function get_items()
        {
        }
    }
    class MenuItem extends \Timber\Core implements \Timber\CoreInterface
    {
        /**
         * @api
         * @var array Array of children of a menu item. Empty if there are no child menu items.
         */
        public $children = array();
        /**
         * @api
         * @var bool Whether the menu item has a `menu-item-has-children` CSS class.
         */
        public $has_child_class = false;
        /**
         * @api
         * @var array Array of class names.
         */
        public $classes = array();
        public $class = '';
        public $level = 0;
        public $post_name;
        public $url;
        public $PostClass = 'Timber\\Post';
        /**
         * Inherited property. Listed here to make it available in the documentation.
         *
         * @api
         * @see _wp_menu_item_classes_by_context()
         * @var bool Whether the menu item links to the currently displayed page.
         */
        public $current;
        /**
         * Inherited property. Listed here to make it available in the documentation.
         *
         * @api
         * @see _wp_menu_item_classes_by_context()
         * @var bool Whether the menu item refers to the parent item of the currently displayed page.
         */
        public $current_item_parent;
        /**
         * Inherited property. Listed here to make it available in the documentation.
         *
         * @api
         * @see _wp_menu_item_classes_by_context()
         * @var bool Whether the menu item refers to an ancestor (including direct parent) of the
         *      currently displayed page.
         */
        public $current_item_ancestor;
        /**
         * Timber Menu. Previously this was a public property, but converted to a method to avoid
         * recursion (see #2071).
         *
         * @since 1.12.0
         * @see \Timber\Menu::menu();
         * @var \Timber\Menu The `Timber\Menu` object the menu item is associated with.
         */
        protected $menu;
        protected $_name;
        protected $_menu_item_object_id;
        protected $_menu_item_url;
        protected $menu_object;
        /**
         * @internal
         * @param array|object $data
         * @param \Timber\Menu $menu The `Timber\Menu` object the menu item is associated with.
         */
        public function __construct($data, $menu = null)
        {
        }
        /**
         * Add a CSS class the menu item should have.
         *
         * @param string $class_name CSS class name to be added.
         */
        public function add_class($class_name)
        {
        }
        /**
         * Get the label for the menu item.
         *
         * @api
         * @return string The label for the menu item.
         */
        public function name()
        {
        }
        /**
         * Magic method to get the label for the menu item.
         *
         * @api
         * @example
         * ```twig
         * <a href="{{ item.link }}">{{ item }}</a>
         * ```
         * @see \Timber\MenuItem::name()
         * @return string The label for the menu item.
         */
        public function __toString()
        {
        }
        /**
         * Get the slug for the menu item.
         *
         * @api
         * @example
         * ```twig
         * <ul>
         *     {% for item in menu.items %}
         *         <li class="{{ item.slug }}">
         *             <a href="{{ item.link }}">{{ item.name }}</a>
         *          </li>
         *     {% endfor %}
         * </ul>
         * ```
         * @return string The URL-safe slug of the menu item.
         */
        public function slug()
        {
        }
        /**
         * Allows dev to access the "master object" (ex: post or page) the menu item represents
         * @api
         * @example
         * ```twig
         * <div>
         *     {% for item in menu.items %}
         *         <a href="{{ item.link }}"><img src="{{ item.master_object.thumbnail }}" /></a>
         *     {% endfor %}
         * </div>
         * ```
         * @return mixed Whatever object (Timber\Post, Timber\Term, etc.) the menu item represents.
         */
        public function master_object()
        {
        }
        /**
         * Get link.
         *
         * @internal
         * @see \Timber\MenuItem::link()
         * @deprecated since 1.0
         * @codeCoverageIgnore
         * @return string An absolute URL, e.g. `http://example.org/my-page`.
         */
        public function get_link()
        {
        }
        /**
         * Get path.
         *
         * @internal
         * @codeCoverageIgnore
         * @see        \Timber\MenuItem::path()
         * @deprecated since 1.0
         * @return string A relative URL, e.g. `/my-page`.
         */
        public function get_path()
        {
        }
        /**
         * Add a new `Timber\MenuItem` object as a child of this menu item.
         *
         * @param \Timber\MenuItem $item The menu item to add.
         */
        public function add_child($item)
        {
        }
        /**
         * @internal
         * @return bool|null
         */
        public function update_child_levels()
        {
        }
        /**
         * Imports the classes to be used in CSS.
         *
         * @internal
         *
         * @param array|object $data
         */
        public function import_classes($data)
        {
        }
        /**
         * Get children of a menu item.
         *
         * You can also directly access the children through the `$children` property (`item.children`
         * in Twig).
         *
         * @internal
         * @example
         * ```twig
         * {% for child in item.get_children %}
         *     <li class="nav-drop-item">
         *         <a href="{{ child.link }}">{{ child.title }}</a>
         *     </li>
         * {% endfor %}
         * ```
         * @return array|bool Array of children of a menu item. Empty if there are no child menu items.
         */
        public function get_children()
        {
        }
        /**
         * Checks to see if the menu item is an external link.
         *
         * If your site is `example.org`, then `google.com/whatever` is an external link. This is
         * helpful when you want to create rules for the target of a link.
         *
         * @api
         * @example
         * ```twig
         * <a href="{{ item.link }}" target="{{ item.is_external ? '_blank' : '_self' }}">
         * ```
         * @return bool Whether the link is external or not.
         */
        public function is_external()
        {
        }
        /**
         * Get the type of the menu item.
         *
         * Depending on what is the menu item links to. Can be `post_type` for posts, pages and custom
         * posts, `post_type_archive` for post type archive pages, `taxonomy` for terms or `custom` for
         * custom links.
         *
         * @api
         * @since 1.0.4
         * @return string The type of the menu item.
         */
        public function type()
        {
        }
        /**
         * Timber Menu.
         *
         * @api
         * @since 1.12.0
         * @return \Timber\Menu The `Timber\Menu` object the menu item is associated with.
         */
        public function menu()
        {
        }
        /**
         * Get a meta value of the menu item.
         *
         * Plugins like Advanced Custom Fields allow you to set custom fields for menu items. With this
         * method you can retrieve the value of these.
         *
         * @example
         * ```twig
         * <a class="icon-{{ item.meta('icon') }}" href="{{ item.link }}">{{ item.title }}</a>
         * ```
         * @api
         * @param string $key The meta key to get the value for.
         * @return mixed Whatever value is stored in the database.
         */
        public function meta($key)
        {
        }
        /* Aliases */
        /**
         * Get the child menu items of a `Timber\MenuItem`.
         *
         * @api
         * @example
         * ```twig
         * {% for child in item.children %}
         *     <li class="nav-drop-item">
         *         <a href="{{ child.link }}">{{ child.title }}</a>
         *     </li>
         * {% endfor %}
         * ```
         * @return array|bool Array of children of a menu item. Empty if there are no child menu items.
         */
        public function children()
        {
        }
        /**
         * Check if a link is external.
         *
         * This is helpful when creating rules for the target of a link.
         *
         * @internal
         * @see \Timber\MenuItem::is_external()
         * @return bool Whether the link is external or not.
         */
        public function external()
        {
        }
        /**
         * Get the full link to a menu item.
         *
         * @api
         * @example
         * ```twig
         * {% for item in menu.items %}
         *     <li><a href="{{ item.link }}">{{ item.title }}</a></li>
         * {% endfor %}
         * ```
         * @return string A full URL, like `http://mysite.com/thing/`.
         */
        public function link()
        {
        }
        /**
         * Get the link the menu item points at.
         *
         * @internal
         * @deprecated since 0.21.7 Use link method instead.
         * @see \Timber\MenuItem::link()
         * @codeCoverageIgnore
         * @return string A full URL, like `http://mysite.com/thing/`.
         */
        public function permalink()
        {
        }
        /**
         * Get the relative path of the menu item’s link.
         *
         * @api
         * @example
         * ```twig
         * {% for item in menu.items %}
         *     <li><a href="{{ item.path }}">{{ item.title }}</a></li>
         * {% endfor %}
         * ```
         * @return string The path of a URL, like `/foo`.
         */
        public function path()
        {
        }
        /**
         * Get the public label for the menu item.
         *
         * @api
         * @example
         * ```twig
         * {% for item in menu.items %}
         *     <li><a href="{{ item.link }}">{{ item.title }}</a></li>
         * {% endfor %}
         * ```
         * @return string The public label, like "Foo".
         */
        public function title()
        {
        }
        /**
         * Get the featured image of the post associated with the menu item.
         *
         * @api
         * @deprecated since 1.5.2 to be removed in v2.0
         * @example
         * ```twig
         * {% for item in menu.items %}
         *     <li><a href="{{ item.link }}"><img src="{{ item.thumbnail }}"/></a></li>
         * {% endfor %}
         * ```
         * @return \Timber\Image|null The featured image object.
         */
        public function thumbnail()
        {
        }
    }
    class Pagination
    {
        public $current;
        public $total;
        public $pages;
        public $next;
        public $prev;
        public function __construct($prefs = array(), $wp_query = null)
        {
        }
        /**
         * Get pagination.
         * @api
         * @param array   $prefs
         * @return array mixed
         */
        public static function get_pagination($prefs = array())
        {
        }
        protected function init($prefs = array(), $wp_query = null)
        {
        }
        /**
         *
         *
         * @param array  $args
         * @return array
         */
        public static function paginate_links($args = array())
        {
        }
        protected static function sanitize_url_params($add_args)
        {
        }
        protected static function sanitize_args($args)
        {
        }
    }
    /**
     * Class PathHelper
     *
     * Useful methods for working with file paths.
     *
     * @api
     * @since 1.11.1
     */
    class PathHelper
    {
        /**
         * Returns information about a file path.
         *
         * Unicode-friendly version of PHP’s pathinfo() function.
         *
         * @link  https://www.php.net/manual/en/function.pathinfo.php
         *
         * @api
         * @since 1.11.1
         *
         * @param string $path    The path to be parsed.
         * @param int    $options The path part to extract. One of `PATHINFO_DIRNAME`,
         *                        `PATHINFO_BASENAME`, `PATHINFO_EXTENSION` or `PATHINFO_FILENAME`. If
         *                        not specified, returns all available elements.
         *
         * @return mixed
         */
        public static function pathinfo($path, $options = PATHINFO_DIRNAME | PATHINFO_BASENAME | PATHINFO_EXTENSION | PATHINFO_FILENAME)
        {
        }
        /**
         * Returns trailing name component of path.
         *
         * Unicode-friendly version of the PHP basename() function.
         *
         * @link  https://www.php.net/manual/en/function.basename.php
         *
         * @api
         * @since 1.11.1
         *
         * @param string $path   The path.
         * @param string $suffix Optional. If the name component ends in suffix, this part will also be
         *                       cut off.
         *
         * @return string
         */
        public static function basename($path, $suffix = '')
        {
        }
    }
    /**
     * Class PostCollection
     *
     * PostCollections are internal objects used to hold a collection of posts.
     *
     * @package Timber
     */
    class PostCollection extends \ArrayObject
    {
        public function __construct($posts = array(), $post_class = '\\Timber\\Post')
        {
        }
        protected static function init($posts, $post_class)
        {
        }
        public function get_posts()
        {
        }
        /**
         * @param array $posts
         * @return array
         */
        public static function maybe_set_preview($posts)
        {
        }
    }
    class PostGetter
    {
        /**
         * @param mixed        $query
         * @param string|array $PostClass
         * @return \Timber\Post|bool
         */
        public static function get_post($query = false, $PostClass = '\\Timber\\Post')
        {
        }
        /**
         * get_post_id_by_name($post_name)
         * @internal
         * @since 1.5.0
         * @param string $post_name
         * @return int
         */
        public static function get_post_id_by_name($post_name)
        {
        }
        public static function get_posts($query = false, $PostClass = '\\Timber\\Post', $return_collection = false)
        {
        }
        public static function query_post($query = false, $PostClass = '\\Timber\\Post')
        {
        }
        /**
         * Sets some default values for those parameters for the query when not set. WordPress's get_posts sets a few of
         * these parameters true by default (compared to WP_Query), we should do the same.
         * @internal
         * @param \WP_Query $query
         * @return \WP_Query
         */
        public static function set_query_defaults($query)
        {
        }
        /**
         * @param mixed $query
         * @param string|array $PostClass
         * @return PostCollection | QueryIterator
         */
        public static function query_posts($query = false, $PostClass = '\\Timber\\Post')
        {
        }
        /**
         * @return integer the ID of the post in the loop
         */
        public static function loop_to_id()
        {
        }
        /**
         * @return bool
         */
        public static function wp_query_has_posts()
        {
        }
        /**
         * @param string $post_type
         * @param string|array $post_class
         *
         * @return string
         */
        public static function get_post_class($post_type, $post_class = '\\Timber\\Post')
        {
        }
        /**
         * @param string|array $arg
         * @return boolean
         */
        public static function is_post_class_or_class_map($arg)
        {
        }
        /**
         * @param string|array $arg
         * @return string|bool if a $type is found; false if not
         */
        public static function get_class_for_use_as_timber_post($arg)
        {
        }
    }
    /**
     * The PostPreview class lets a user modify a post preview/excerpt to their liking.
     *
     * It’s designed to be used through the `Timber\Post::preview()` method. The public methods of this
     * class all return the object itself, which means that this is a **chainable object**. You can
     * change the output of the preview by **adding more methods**.
     *
     * By default, the preview will
     *
     * - have a length of 50 words, which will be forced, even if a longer excerpt is set on the post.
     * - be stripped of all HTML tags.
     * - have an ellipsis (…) as the end of the text.
     * - have a "Read More" link appended.
     *
     * @example
     * ```twig
     * {# Use default preview #}
     * <p>{{ post.preview }}</p>
     *
     * {# Change the post preview text #}
     * <p>{{ post.preview.read_more('Continue Reading') }}</p>
     *
     * {# Additionally restrict the length to 50 words #}
     * <p>{{ post.preview.length(50).read_more('Continue Reading') }}</p>
     * ```
     * @since 1.0.4
     * @see \Timber\Post::preview()
     */
    class PostPreview
    {
        /**
         * Post.
         *
         * @var \Timber\Post
         */
        protected $post;
        /**
         * Preview end.
         *
         * @var string
         */
        protected $end = '&hellip;';
        /**
         * Force length.
         *
         * @var bool
         */
        protected $force = false;
        /**
         * Length in words.
         *
         * @var int
         */
        protected $length = 50;
        /**
         * Length in characters.
         *
         * @var bool
         */
        protected $char_length = false;
        /**
         * Read more text.
         *
         * @var string
         */
        protected $readmore = 'Read More';
        /**
         * HTML tag stripping behavior.
         *
         * @var bool
         */
        protected $strip = true;
        /**
         * Destroy tags.
         *
         * @var array List of tags that should always be destroyed.
         */
        protected $destroy_tags = array('script', 'style');
        /**
         * PostPreview constructor.
         *
         * @api
         * @param \Timber\Post $post The post to pull the preview from.
         */
        public function __construct($post)
        {
        }
        /**
         * Returns the resulting preview.
         *
         * @api
         * @return string
         */
        public function __toString()
        {
        }
        /**
         * Restricts the length of the preview to a certain amount of words.
         *
         * @api
         * @example
         * ```twig
         * <p>{{ post.preview.length(50) }}</p>
         * ```
         * @param int $length The maximum amount of words (not letters) for the preview. Default `50`.
         * @return \Timber\PostPreview
         */
        public function length($length = 50)
        {
        }
        /**
         * Restricts the length of the preview to a certain amount of characters.
         *
         * @api
         * @example
         * ```twig
         * <p>{{ post.preview.chars(180) }}</p>
         * ```
         * @param int|bool $char_length The maximum amount of characters for the preview. Default
         *                              `false`.
         * @return \Timber\PostPreview
         */
        public function chars($char_length = false)
        {
        }
        /**
         * Defines the text to end the preview with.
         *
         * @api
         * @example
         * ```twig
         * <p>{{ post.preview.end('… and much more!') }}</p>
         * ```
         * @param string $end The text for the end of the preview. Default `…`.
         * @return \Timber\PostPreview
         */
        public function end($end = '&hellip;')
        {
        }
        /**
         * Forces preview lengths.
         *
         * What happens if your custom post excerpt is longer than the length requested? By default, it
         * will use the full `post_excerpt`. However, you can set this to `true` to *force* your excerpt
         * to be of the desired length.
         *
         * @api
         * @example
         * ```twig
         * <p>{{ post.preview.length(20).force }}</p>
         * ```
         * @param bool $force Whether the length of the preview should be forced to the requested
         *                    length, even if an editor wrote a manual excerpt that is longer than the
         *                    set length. Default `true`.
         * @return \Timber\PostPreview
         */
        public function force($force = true)
        {
        }
        /**
         * Defines the text to be used for the "Read More" link.
         *
         * Set this to `false` to not add a "Read More" link.
         *
         * @api
         * ```twig
         * <p>{{ post.preview.read_more('Learn more') }}</p>
         * ```
         * @param string $readmore Text for the link. Default 'Read More'.
         * @return \Timber\PostPreview
         */
        public function read_more($readmore = 'Read More')
        {
        }
        /**
         * Defines how HTML tags should be stripped from the preview.
         *
         * @api
         * ```twig
         * {# Strips all HTML tags, except for bold or emphasized text #}
         * <p>{{ post.preview.length('50').strip('<strong><em>') }}</p>
         * ```
         * @param bool|string $strip Whether or how HTML tags in the preview should be stripped. Use
         *                           `true` to strip all tags, `false` for no stripping, or a string for
         *                           a list of allowed tags (e.g. '<p><a>'). Default `true`.
         * @return \Timber\PostPreview
         */
        public function strip($strip = true)
        {
        }
        /**
         * @param string $text
         * @param array|bool $readmore_matches
         * @param boolean $trimmed was the text trimmed?
         */
        protected function assemble($text, $readmore_matches, $trimmed)
        {
        }
        protected function run()
        {
        }
    }
    /**
     * A PostQuery allows a user to query for a Collection of WordPress Posts.
     * PostCollections are used directly in Twig templates to iterate through and retrieve
     * meta information about the collection of posts
     * @api
     * @package Timber
     */
    class PostQuery extends \Timber\PostCollection
    {
        /**
         * Found posts.
         *
         * The total amount of posts found for this query. Will be `0` if you used `no_found_rows` as a
         * query parameter. Will be `null` if you passed in an existing collection of posts.
         *
         * @api
         * @since 1.11.1
         * @var int The amount of posts found in the query.
         */
        public $found_posts = null;
        protected $userQuery;
        protected $queryIterator;
        protected $pagination = null;
        /**
         * @param mixed   	$query
         * @param string 	$post_class
         */
        public function __construct($query = false, $post_class = '\\Timber\\Post')
        {
        }
        /**
         * @return mixed the query the user orignally passed
         * to the pagination object
         */
        protected function get_query()
        {
        }
        /**
         * Set pagination for the collection. Optionally could be used to get pagination with custom preferences.
         *
         * @param 	array $prefs
         * @return 	Timber\Pagination object
         */
        public function pagination($prefs = array())
        {
        }
    }
    /**
     * Wrapper for the post_type object provided by WordPress
     * @since 1.0.4
    */
    class PostType
    {
        /**
         * @param string $post_type
         */
        public function __construct($post_type)
        {
        }
        public function __toString()
        {
        }
        protected function init($post_type)
        {
        }
    }
    /**
     * Class PostsIterator
     *
     * @package Timber
     */
    class PostsIterator extends \ArrayIterator
    {
        #[\ReturnTypeWillChange]
        public function current()
        {
        }
    }
    class QueryIterator implements \Iterator, \Countable
    {
        public function __construct($query = false, $posts_class = 'Timber\\Post')
        {
        }
        public function post_count()
        {
        }
        /**
         * Gets the amount of found posts in the query.
         *
         * @api
         * @since 1.11.1
         *
         * @return int
         */
        public function found_posts()
        {
        }
        public function get_pagination($prefs)
        {
        }
        public function get_posts($return_collection = false)
        {
        }
        //
        // GET POSTS
        //
        public static function get_query_from_array_of_ids($query = array())
        {
        }
        public static function get_query_from_string($string = '')
        {
        }
        //
        // Iterator Interface
        //
        public function valid() : bool
        {
        }
        #[\ReturnTypeWillChange]
        public function current()
        {
        }
        /**
         * Don't implement next, because current already advances the loop
         */
        #[\ReturnTypeWillChange]
        public final function next()
        {
        }
        #[\ReturnTypeWillChange]
        public function rewind()
        {
        }
        #[\ReturnTypeWillChange]
        public function key()
        {
        }
        //get_posts users numberposts
        public static function fix_number_posts_wp_quirk($query)
        {
        }
        //get_posts uses category, WP_Query uses cat. Why? who knows...
        public static function fix_cat_wp_quirk($query)
        {
        }
        /**
         * this will test for whether a custom page to display posts is active, and if so, set the query to the default
         * @param  WP_Query $query the original query recived from WordPress
         * @return WP_Query
         */
        public static function handle_maybe_custom_posts_page($query)
        {
        }
        /**
         * Count elements of an object.
         *
         * Necessary for some Twig `loop` variable properties.
         * @see http://twig.sensiolabs.org/doc/tags/for.html#the-loop-variable
         * @link  http://php.net/manual/en/countable.count.php
         * @return int The custom count as an integer.
         * The return value is cast to an integer.
         */
        public function count() : int
        {
        }
    }
    /**
     * TimberRequest exposes $_GET and $_POST to the context
     */
    class Request extends \Timber\Core implements \Timber\CoreInterface
    {
        public $post = array();
        public $get = array();
        /**
         * Constructs a TimberRequest object
         * @example
         */
        public function __construct()
        {
        }
        /**
         * @internal
         */
        protected function init()
        {
        }
        public function __call($field, $args)
        {
        }
        public function __get($field)
        {
        }
        /**
         * @return boolean|null
         */
        public function __isset($field)
        {
        }
        public function meta($key)
        {
        }
    }
    /**
     * Timber\Site gives you access to information you need about your site. In Multisite setups, you can get info on other sites in your network.
     * @example
     * ```php
     * $context = Timber::context();
     * $other_site_id = 2;
     * $context['other_site'] = new Timber\Site($other_site_id);
     * Timber::render('index.twig', $context);
     * ```
     * ```twig
     * My site is called {{site.name}}, another site on my network is {{other_site.name}}
     * ```
     * ```html
     * My site is called Jared's blog, another site on my network is Upstatement.com
     * ```
     */
    class Site extends \Timber\Core implements \Timber\CoreInterface
    {
        /**
         * @api
         * @var string the admin email address set in the WP admin panel
         */
        public $admin_email;
        public $blogname;
        /**
         * @api
         * @var string
         */
        public $charset;
        /**
         * @api
         * @var string
         */
        public $description;
        /**
         * @api
         * @var int the ID of a site in multisite
         */
        public $id;
        /**
         * @api
         * @var string the language setting ex: en-US
         */
        public $language;
        /**
         * @api
         * @var bool true if multisite, false if plain ole' WordPress
         */
        public $multisite;
        /**
         * @api
         * @var string
         */
        public $name;
        /** @api
         * @var string for people who like trackback spam
         */
        public $pingback_url;
        public $siteurl;
        /**
         * @api
         * @var [TimberTheme](#TimberTheme)
         */
        public $theme;
        /**
         * @api
         * @var string
         */
        public $title;
        public $url;
        public $home_url;
        public $site_url;
        /**
         * @api
         * @var string
         */
        public $rdf;
        public $rss;
        public $rss2;
        public $atom;
        /**
         * Constructs a Timber\Site object
         * @example
         * ```php
         * //multisite setup
         * $site = new Timber\Site(1);
         * $site_two = new Timber\Site("My Cool Site");
         * //non-multisite
         * $site = new Timber\Site();
         * ```
         * @param string|int $site_name_or_id
         */
        public function __construct($site_name_or_id = null)
        {
        }
        /**
         * Switches to the blog requested in the request
         * @param string|integer|null $site_name_or_id
         * @return integer with the ID of the new blog
         */
        protected static function switch_to_blog($site_name_or_id)
        {
        }
        /**
         * @internal
         * @param integer $site_id
         */
        protected function init_as_multisite($site_id)
        {
        }
        /**
         * Executed for single-blog sites
         * @internal
         */
        protected function init_as_singlesite()
        {
        }
        /**
         * Executed for all types of sites: both multisite and "regular"
         * @internal
         */
        protected function init()
        {
        }
        /**
         * Returns the language attributes that you're looking for
         * @return string
         */
        public function language_attributes()
        {
        }
        /**
         *
         *
         * @param string  $field
         * @return mixed
         */
        public function __get($field)
        {
        }
        public function icon()
        {
        }
        protected function icon_multisite($site_id)
        {
        }
        /**
         * Returns the link to the site's home.
         * @example
         * ```twig
         * <a href="{{ site.link }}" title="Home">
         * 	  <img src="/wp-content/uploads/logo.png" alt="Logo for some stupid thing" />
         * </a>
         * ```
         * ```html
         * <a href="http://example.org" title="Home">
         * 	  <img src="/wp-content/uploads/logo.png" alt="Logo for some stupid thing" />
         * </a>
         * ```
         * @api
         * @return string
         */
        public function link()
        {
        }
        /**
         * @deprecated 0.21.9
         * @internal
         * @return string
         */
        public function get_link()
        {
        }
        /**
         * @ignore
         */
        public function meta($field)
        {
        }
        /**
         *
         * @ignore
         * @param string  $key
         * @param mixed   $value
         */
        public function update($key, $value)
        {
        }
        /**
         * @deprecated 1.0.4
         * @see Timber\Site::link
         * @return string
         */
        public function url()
        {
        }
        /**
         * @deprecated 0.21.9
         * @internal
         * @return string
         */
        public function get_url()
        {
        }
    }
    class TermGetter
    {
        /**
         * @param int|WP_Term|object $term
         * @param string $taxonomy
         * @return Timber\Term|WP_Error|null
         */
        public static function get_term($term, $taxonomy, $TermClass = '\\Timber\\Term')
        {
        }
        /**
         * @param string|array $args
         * @param array $maybe_args
         * @param string $TermClass
         * @return mixed
         */
        public static function get_terms($args = null, $maybe_args = array(), $TermClass = '\\Timber\\Term')
        {
        }
        /**
         * @param string|array $taxonomies
         * @param string|array $args
         * @param string $TermClass
         * @return mixed
         */
        public static function handle_term_query($taxonomies, $args, $TermClass)
        {
        }
        /**
         * @param string $query_string
         * @return \stdClass
         */
        protected static function get_term_query_from_query_string($query_string)
        {
        }
        /**
         * @param string $taxs
         * @return \stdClass
         */
        protected static function get_term_query_from_string($taxs)
        {
        }
        /**
         * @param array $args
         * @return \stdClass
         */
        public static function get_term_query_from_assoc_array($args)
        {
        }
        /**
         * @param array $args
         * @return \stdClass|null
         */
        public static function get_term_query_from_array($args)
        {
        }
        /**
         * @param integer[] $args
         * @return \stdClass
         */
        public static function get_term_query_from_array_of_ids($args)
        {
        }
        /**
         * @param string[] $args
         * @return \stdClass
         */
        public static function get_term_query_from_array_of_strings($args)
        {
        }
    }
    /**
     * Class provides different text-related functions 
     * commonly used in WordPress development
     */
    class TextHelper
    {
        /**
         * Trims text to a certain number of characters.
         * This function can be useful for excerpt of the post
         * As opposed to wp_trim_words trims characters that makes text to
         * take the same amount of space in each post for example
         *
         * @since   1.2.0
         * @author  @CROSP
         * @param   string $text      Text to trim.
         * @param   int    $num_chars Number of characters. Default is 60.
         * @param   string $more      What to append if $text needs to be trimmed. Defaults to '&hellip;'.
         * @return  string trimmed text.
         */
        public static function trim_characters($text, $num_chars = 60, $more = '&hellip;')
        {
        }
        /**
         *
         *
         * @param string  $text
         * @param int     $num_words
         * @param string|null|false  $more text to appear in "Read more...". Null to use default, false to hide
         * @param string  $allowed_tags
         * @return string
         */
        public static function trim_words($text, $num_words = 55, $more = null, $allowed_tags = 'p a span b i br blockquote')
        {
        }
        public static function remove_tags($string, $tags = array())
        {
        }
        /**
         * @param string $haystack
         * @param string $needle
         * @return boolean
         */
        public static function starts_with($haystack, $needle)
        {
        }
        /**
         * Does the string in question (haystack) 
         * end with the substring in question (needle)?
         * @param string $haystack
         * @param string $needle
         * @return boolean
         */
        public static function ends_with($haystack, $needle)
        {
        }
        /**
         *
         *
         * @param string  $html
         * @return string
         */
        public static function close_tags($html)
        {
        }
    }
    /**
     * Need to display info about your theme? Well you've come to the right place. By default info on the current theme comes for free with what's fetched by `Timber::context()` in which case you can access it your theme like so:
     * @example
     * ```php
     * <?php
     * $context = Timber::context();
     * Timber::render('index.twig', $context);
     * ?>
     * ```
     * ```twig
     * <script src="{{theme.link}}/static/js/all.js"></script>
     * ```
     * ```html
     * <script src="http://example.org/wp-content/themes/my-theme/static/js/all.js"></script>
     * ```
     * @package Timber
     */
    class Theme extends \Timber\Core
    {
        /**
         * @api
         * @var string the human-friendly name of the theme (ex: `My Timber Starter Theme`)
         */
        public $name;
        /**
         * @api
         * @var string the version of the theme (ex: `1.2.3`)
         */
        public $version;
        /**
         * @api
         * @var TimberTheme|bool the TimberTheme object for the parent theme (if it exists), false otherwise
         */
        public $parent = false;
        /**
         * @api
         * @var string the slug of the parent theme (ex: `_s`)
         */
        public $parent_slug;
        /**
         * @api
         * @var string the slug of the theme (ex: `my-super-theme`)
         */
        public $slug;
        public $uri;
        /**
         * Constructs a new TimberTheme object. NOTE the TimberTheme object of the current theme comes in the default `Timber::context()` call. You can access this in your twig template via `{{site.theme}}.
         * @param string $slug
         * @example
         * ```php
         * <?php
         *     $theme = new TimberTheme("my-theme");
         *     $context['theme_stuff'] = $theme;
         *     Timber::render('single.twig', $context);
         * ?>
         * ```
         * ```twig
         * We are currently using the {{ theme_stuff.name }} theme.
         * ```
         * ```html
         * We are currently using the My Theme theme.
         * ```
         */
        public function __construct($slug = null)
        {
        }
        /**
         * @internal
         * @param string $slug
         */
        protected function init($slug = null)
        {
        }
        /**
         * @api
         * @return string the absolute path to the theme (ex: `http://example.org/wp-content/themes/my-timber-theme`)
         */
        public function link()
        {
        }
        /**
         * @api
         * @return  string the relative path to the theme (ex: `/wp-content/themes/my-timber-theme`)
         */
        public function path()
        {
        }
        /**
         * @param string $name
         * @param bool $default
         * @return string
         */
        public function theme_mod($name, $default = false)
        {
        }
        /**
         * @return array
         */
        public function theme_mods()
        {
        }
        /**
         * Gets a raw, unformatted theme header.
         * 
         * @api 
         * @see \WP_Theme::get()
         * @example
         * ```twig
         * {{ theme.get('Version') }}
         * ```
         *
         * @param string $header Name of the theme header. Name, Description, Author, Version,
         *                       ThemeURI, AuthorURI, Status, Tags.
         *
         * @return false|string String on success, false on failure.
         */
        public function get($header)
        {
        }
        /**
         * Gets a theme header, formatted and translated for display.
         *
         * @api
         * @see \WP_Theme::display()
         * @example
         * ```twig
         * {{ theme.display('Description') }}
         * ```
         *
         * @param string $header Name of the theme header. Name, Description, Author, Version,
         *                       ThemeURI, AuthorURI, Status, Tags.
         *
         * @return false|string
         */
        public function display($header)
        {
        }
    }
    /**
     * Timber Class.
     *
     * Main class called Timber for this plugin.
     *
     * @example
     * ```php
     *  $posts = Timber::get_posts();
     *  $posts = Timber::get_posts('post_type = article')
     *  $posts = Timber::get_posts(array('post_type' => 'article', 'category_name' => 'sports')); // uses wp_query format.
     *  $posts = Timber::get_posts(array(23,24,35,67), 'InkwellArticle');
     *
     *  $context = Timber::context(); // returns wp favorites!
     *  $context['posts'] = $posts;
     *  Timber::render('index.twig', $context);
     * ```
     */
    class Timber
    {
        public static $version = '1.22.1';
        public static $locations;
        public static $dirname = 'views';
        public static $twig_cache = false;
        public static $cache = false;
        public static $auto_meta = true;
        public static $autoescape = false;
        public static $context_cache = array();
        /**
         * @codeCoverageIgnore
         */
        public function __construct()
        {
        }
        /**
         * Tests whether we can use Timber
         * @codeCoverageIgnore
         * @return
         */
        protected function test_compatibility()
        {
        }
        public function init_constants()
        {
        }
        /**
         * @codeCoverageIgnore
         */
        protected static function init()
        {
        }
        /* Post Retrieval Routine
        	================================ */
        /**
         * Get a post by post ID or query (as a query string or an array of arguments).
         * But it's also cool
         *
         * @api
         * @param mixed        $query     Optional. Post ID or query (as query string or an array of arguments for
         *                                WP_Query). If a query is provided, only the first post of the result will be
         *                                returned. Default false.
         * @param string|array $PostClass Optional. Class to use to wrap the returned post object. Default 'Timber\Post'.
         * @return \Timber\Post|bool Timber\Post object if a post was found, false if no post was found.
         */
        public static function get_post($query = false, $PostClass = 'Timber\\Post')
        {
        }
        /**
         * Get posts.
         * @api
         * @example
         * ```php
         * $posts = Timber::get_posts();
         *  $posts = Timber::get_posts('post_type = article')
         *  $posts = Timber::get_posts(array('post_type' => 'article', 'category_name' => 'sports')); // uses wp_query format.
         *  $posts = Timber::get_posts('post_type=any', array('portfolio' => 'MyPortfolioClass', 'alert' => 'MyAlertClass')); //use a classmap for the $PostClass
         * ```
         * @param mixed   $query
         * @param string|array  $PostClass
         * @return array|bool|null
         */
        public static function get_posts($query = false, $PostClass = 'Timber\\Post', $return_collection = false)
        {
        }
        /**
         * Query post.
         * @api
         * @param mixed   $query
         * @param string  $PostClass
         * @return Post|array|bool|null
         */
        public static function query_post($query = false, $PostClass = 'Timber\\Post')
        {
        }
        /**
         * Query posts.
         * @api
         * @param mixed   $query
         * @param string  $PostClass
         * @return PostCollection
         */
        public static function query_posts($query = false, $PostClass = 'Timber\\Post')
        {
        }
        /* Term Retrieval
        	================================ */
        /**
         * Get terms.
         * @api
         * @param string|array $args
         * @param array   $maybe_args
         * @param string  $TermClass
         * @return mixed
         */
        public static function get_terms($args = null, $maybe_args = array(), $TermClass = 'Timber\\Term')
        {
        }
        /**
         * Get term.
         * @api
         * @param int|WP_Term|object $term
         * @param string     $taxonomy
         * @return Timber\Term|WP_Error|null
         */
        public static function get_term($term, $taxonomy = 'post_tag', $TermClass = 'Timber\\Term')
        {
        }
        /* Site Retrieval
        	================================ */
        /**
         * Get sites.
         * @api
         * @param array|bool $blog_ids
         * @return array
         */
        public static function get_sites($blog_ids = false)
        {
        }
        /*  Template Setup and Display
        	================================ */
        /**
         * Alias for Timber::get_context() which is deprecated in 2.0.
         *
         * This will allow us to update the starter theme to use the ::context() method and better
         * prepare users for the upgrade (even if the details of what the method returns differs
         * slightly).
         *
         * @see \Timber\Timber::get_context()
         * @api
         * @return array
         */
        public static function context()
        {
        }
        /**
         * Get context.
         * @api
         * @return array
         */
        public static function get_context()
        {
        }
        /**
         * Compile a Twig file.
         *
         * Passes data to a Twig file and returns the output.
         * If the template file doesn't exist it will throw a warning when WP_DEBUG is enabled.
         *
         * @api
         * @example
         * ```php
         * $data = array(
         *     'firstname' => 'Jane',
         *     'lastname' => 'Doe',
         *     'email' => 'jane.doe@example.org',
         * );
         *
         * $team_member = Timber::compile( 'team-member.twig', $data );
         * ```
         * @param array|string $filenames  Name of the Twig file to render. If this is an array of files, Timber will
         *                                 render the first file that exists.
         * @param array        $data       Optional. An array of data to use in Twig template.
         * @param bool|int     $expires    Optional. In seconds. Use false to disable cache altogether. When passed an
         *                                 array, the first value is used for non-logged in visitors, the second for users.
         *                                 Default false.
         * @param string       $cache_mode Optional. Any of the cache mode constants defined in TimberLoader.
         * @param bool         $via_render Optional. Whether to apply optional render or compile filters. Default false.
         * @return bool|string The returned output.
         */
        public static function compile($filenames, $data = array(), $expires = false, $cache_mode = \Timber\Loader::CACHE_USE_DEFAULT, $via_render = false)
        {
        }
        /**
         * Compile a string.
         *
         * @api
         * @example
         * ```php
         * $data = array(
         *     'username' => 'Jane Doe',
         * );
         *
         * $welcome = Timber::compile_string( 'Hi {{ username }}, I’m a string with a custom Twig variable', $data );
         * ```
         * @param string $string A string with Twig variables.
         * @param array  $data   Optional. An array of data to use in Twig template.
         * @return  bool|string
         */
        public static function compile_string($string, $data = array())
        {
        }
        /**
         * Fetch function.
         *
         * @api
         * @param array|string $filenames  Name of the Twig file to render. If this is an array of files, Timber will
         *                                 render the first file that exists.
         * @param array        $data       Optional. An array of data to use in Twig template.
         * @param bool|int     $expires    Optional. In seconds. Use false to disable cache altogether. When passed an
         *                                 array, the first value is used for non-logged in visitors, the second for users.
         *                                 Default false.
         * @param string       $cache_mode Optional. Any of the cache mode constants defined in TimberLoader.
         * @return bool|string The returned output.
         */
        public static function fetch($filenames, $data = array(), $expires = false, $cache_mode = \Timber\Loader::CACHE_USE_DEFAULT)
        {
        }
        /**
         * Render function.
         *
         * Passes data to a Twig file and echoes the output.
         *
         * @api
         * @example
         * ```php
         * $context = Timber::context();
         *
         * Timber::render( 'index.twig', $context );
         * ```
         * @param array|string $filenames  Name of the Twig file to render. If this is an array of files, Timber will
         *                                 render the first file that exists.
         * @param array        $data       Optional. An array of data to use in Twig template.
         * @param bool|int     $expires    Optional. In seconds. Use false to disable cache altogether. When passed an
         *                                 array, the first value is used for non-logged in visitors, the second for users.
         *                                 Default false.
         * @param string       $cache_mode Optional. Any of the cache mode constants defined in TimberLoader.
         * @return bool|string The echoed output.
         */
        public static function render($filenames, $data = array(), $expires = false, $cache_mode = \Timber\Loader::CACHE_USE_DEFAULT)
        {
        }
        /**
         * Render a string with Twig variables.
         *
         * @api
         * @example
         * ```php
         * $data = array(
         *     'username' => 'Jane Doe',
         * );
         *
         * Timber::render_string( 'Hi {{ username }}, I’m a string with a custom Twig variable', $data );
         * ```
         * @param string $string A string with Twig variables.
         * @param array  $data   An array of data to use in Twig template.
         * @return bool|string
         */
        public static function render_string($string, $data = array())
        {
        }
        /*  Sidebar
        	================================ */
        /**
         * Get sidebar.
         * @api
         * @param string  $sidebar
         * @param array   $data
         * @return bool|string
         */
        public static function get_sidebar($sidebar = 'sidebar.php', $data = array())
        {
        }
        /**
         * Get sidebar from PHP
         * @api
         * @param string  $sidebar
         * @param array   $data
         * @return string
         */
        public static function get_sidebar_from_php($sidebar = '', $data = array())
        {
        }
        /* Widgets
        	================================ */
        /**
         * Get widgets.
         *
         * @api
         * @param int|string $widget_id Optional. Index, name or ID of dynamic sidebar. Default 1.
         * @return string
         */
        public static function get_widgets($widget_id)
        {
        }
        /*  Pagination
        	================================ */
        /**
         * Get pagination.
         * @api
         * @param array   $prefs
         * @return array mixed
         */
        public static function get_pagination($prefs = array())
        {
        }
        /*  Utility
        	================================ */
        /**
         * Add route.
         *
         * @param string  $route
         * @param callable $callback
         * @param array   $args
         * @deprecated since 0.20.0 and will be removed in 1.1
         * @codeCoverageIgnore
         */
        public static function add_route($route, $callback, $args = array())
        {
        }
    }
    class Twig
    {
        public static $dir_name;
        /**
         * @codeCoverageIgnore
         */
        public static function init()
        {
        }
        /**
         * Adds functions to Twig.
         *
         * @param \Twig\Environment $twig The Twig Environment.
         * @return \Twig\Environment
         */
        public function add_timber_functions($twig)
        {
        }
        /**
         * Function for Term or TimberTerm() within Twig
         * @since 1.5.1
         * @author @jarednova
         * @param integer $tid the term ID to search for
         * @param string $taxonomy the taxonomy to search inside of. If sent a class name, it will use that class to support backwards compatibility
         * @param string $TermClass the class to use for processing the term
         * @return Term|array
         */
        function handle_term_object($tid, $taxonomy = '', $TermClass = 'Timber\\Term')
        {
        }
        /**
         * Process the arguments for handle_term_object to determine what arguments the user is sending
         * @since 1.5.1
         * @author @jarednova
         * @param string $maybe_taxonomy probably a taxonomy, but it could be a Timber\Term subclass
         * @param string $TermClass a string for the Timber\Term subclass
         * @return array of processed arguments
         */
        protected static function process_term_args($maybe_taxonomy, $TermClass)
        {
        }
        /**
         * Adds filters to Twig.
         *
         * @param \Twig\Environment $twig The Twig Environment.
         * @return \Twig\Environment
         */
        public function add_timber_filters($twig)
        {
        }
        /**
         * Adds escapers to Twig.
         *
         * @param \Twig\Environment $twig The Twig Environment.
         * @return \Twig\Environment
         */
        public function add_timber_escapers($twig)
        {
        }
        /**
         *
         *
         * @param mixed   $arr
         * @return array
         */
        public function to_array($arr)
        {
        }
        /**
         *
         *
         * @param string  $function_name
         * @return mixed
         */
        public function exec_function($function_name)
        {
        }
        /**
         *
         *
         * @param string  $content
         * @return string
         */
        public function twig_pretags($content)
        {
        }
        /**
         *
         *
         * @param array   $matches
         * @return string
         */
        public function convert_pre_entities($matches)
        {
        }
        /**
         *
         *
         * @param string  $date
         * @param string  $format (optional)
         * @return string
         */
        public function intl_date($date, $format = null)
        {
        }
        /**
         * Returns the difference between two times in a human readable format.
         *
         * Differentiates between past and future dates.
         *
         * @see \human_time_diff()
         *
         * @param int|string $from          Base date as a timestamp or a date string.
         * @param int|string $to            Optional. Date to calculate difference to as a timestamp or
         *                                  a date string. Default to current time.
         * @param string     $format_past   Optional. String to use for past dates. To be used with
         *                                  `sprintf()`. Default `%s ago`.
         * @param string     $format_future Optional. String to use for future dates. To be used with
         *                                  `sprintf()`. Default `%s from now`.
         *
         * @return string
         */
        public static function time_ago($from, $to = null, $format_past = null, $format_future = null)
        {
        }
        /**
         * @param array $arr
         * @param string $first_delimiter
         * @param string $second_delimiter
         * @return string
         */
        public function add_list_separators($arr, $first_delimiter = ',', $second_delimiter = ' and')
        {
        }
    }
    class URLHelper
    {
        /**
         *
         *
         * @return string
         */
        public static function get_current_url()
        {
        }
        /**
         *
         * Get url scheme
         * @return string
         */
        public static function get_scheme()
        {
        }
        /**
         *
         * Check to see if the URL begins with the string in question
         * Because it's a URL we don't care about protocol (HTTP vs HTTPS)
         * Or case (so it's cAsE iNsEnSeTiVe)
         * @return boolean
         */
        public static function starts_with($haystack, $starts_with)
        {
        }
        /**
         *
         *
         * @param string  $url
         * @return bool
         */
        public static function is_url($url)
        {
        }
        /**
         *
         *
         * @return string
         */
        public static function get_path_base()
        {
        }
        /**
         *
         *
         * @param string  $url
         * @param bool    $force
         * @return string
         */
        public static function get_rel_url($url, $force = false)
        {
        }
        /**
         * Some setups like HTTP_HOST, some like SERVER_NAME, it's complicated
         * @link http://stackoverflow.com/questions/2297403/http-host-vs-server-name
         * @return string the HTTP_HOST or SERVER_NAME
         */
        public static function get_host()
        {
        }
        /**
         *
         *
         * @param string  $url
         * @return bool
         */
        public static function is_local($url)
        {
        }
        /**
         *
         *
         * @param string  $src
         * @return string
         */
        public static function get_full_path($src)
        {
        }
        /**
         * Takes a url and figures out its place based in the file system based on path
         * NOTE: Not fool-proof, makes a lot of assumptions about the file path
         * matching the URL path
         *
         * @param string  $url
         * @return string
         */
        public static function url_to_file_system($url)
        {
        }
        /**
         * @param string $fs
         */
        public static function file_system_to_url($fs)
        {
        }
        /**
         * Get the path to the content directory relative to the site.
         * This replaces the WP_CONTENT_SUBDIR constant
         * @return string (ex: /wp-content or /content)
         */
        public static function get_content_subdir()
        {
        }
        /**
         *
         *
         * @param string  $src
         * @return string
         */
        public static function get_rel_path($src)
        {
        }
        /**
         *
         *
         * @param string  $url
         * @return string
         */
        public static function remove_double_slashes($url)
        {
        }
        /**
         *
         *
         * @param string  $url
         * @param string  $path
         * @return string
         */
        public static function prepend_to_url($url, $path)
        {
        }
        /**
         *
         *
         * @param string  $path
         * @return string
         */
        public static function preslashit($path)
        {
        }
        /**
         *
         *
         * @param string  $path
         * @return string
         */
        public static function unpreslashit($path)
        {
        }
        /**
         * This will evaluate wheter a URL is at an aboslute location (like http://example.org/whatever)
         *
         * @param string $path
         * @return boolean true if $path is an absolute url, false if relative.
         */
        public static function is_absolute($path)
        {
        }
        /**
         * This function is slightly different from the one below in the case of:
         * an image hosted on the same domain BUT on a different site than the
         * Wordpress install will be reported as external content.
         *
         * @param string  $url a URL to evaluate against
         * @return boolean if $url points to an external location returns true
         */
        public static function is_external_content($url)
        {
        }
        /**
         * Checks if URL is external or internal.
         * Works with domains, subdomains and protocol relative domains.
         *
         * @param string $url Url.
         * @return bool     true if $path is an external url, false if relative or local.
         *                  true if it's a subdomain (http://cdn.example.org = true)
         */
        public static function is_external($url)
        {
        }
        /**
         * Pass links through untrailingslashit unless they are a single /
         *
         * @param string  $link
         * @return string
         */
        public static function remove_trailing_slash($link)
        {
        }
        /**
         * Removes the subcomponent of a URL regardless of protocol
         * @since 1.3.3
         * @author jarednova
         * @param string $haystack ex: http://example.org/wp-content/uploads/dog.jpg
         * @param string $needle ex: http://example.org/wp-content
         * @return string
         */
        public static function remove_url_component($haystack, $needle)
        {
        }
        /**
         * Swaps whatever protocol of a URL is sent. http becomes https and vice versa
         * @since 1.3.3
         * @author jarednova
         * @param string $url ex: http://example.org/wp-content/uploads/dog.jpg
         * @return string ex: https://example.org/wp-content/uploads/dog.jpg
         */
        public static function swap_protocol($url)
        {
        }
        /**
         * Pass links through user_trailingslashit handling query strings properly
         *
         * @param string $link
         * @return string
         * */
        public static function user_trailingslashit($link)
        {
        }
        /**
         * Returns the url parameters, for example for url http://example.org/blog/post/news/2014/whatever
         * this will return array('blog', 'post', 'news', '2014', 'whatever');
         * OR if sent an integer like: TimberUrlHelper::get_params(2); this will return 'news';
         *
         * @param int $i the position of the parameter to grab.
         * @return array|string
         */
        public static function get_params($i = false)
        {
        }
    }
}
namespace Twig\Cache {
    /**
     * Interface implemented by cache classes.
     *
     * It is highly recommended to always store templates on the filesystem to
     * benefit from the PHP opcode cache. This interface is mostly useful if you
     * need to implement a custom strategy for storing templates on the filesystem.
     *
     * @author Andrew Tch <andrew@noop.lv>
     */
    interface CacheInterface
    {
        /**
         * Generates a cache key for the given template class name.
         *
         * @param string $name      The template name
         * @param string $className The template class name
         *
         * @return string
         */
        public function generateKey($name, $className);
        /**
         * Writes the compiled template to cache.
         *
         * @param string $key     The cache key
         * @param string $content The template representation as a PHP class
         */
        public function write($key, $content);
        /**
         * Loads a template from the cache.
         *
         * @param string $key The cache key
         */
        public function load($key);
        /**
         * Returns the modification timestamp of a key.
         *
         * @param string $key The cache key
         *
         * @return int
         */
        public function getTimestamp($key);
    }
    /**
     * Implements a cache on the filesystem.
     *
     * @author Andrew Tch <andrew@noop.lv>
     */
    class FilesystemCache implements \Twig\Cache\CacheInterface
    {
        public const FORCE_BYTECODE_INVALIDATION = 1;
        /**
         * @param string $directory The root cache directory
         * @param int    $options   A set of options
         */
        public function __construct($directory, $options = 0)
        {
        }
        public function generateKey($name, $className)
        {
        }
        public function load($key)
        {
        }
        public function write($key, $content)
        {
        }
        public function getTimestamp($key)
        {
        }
    }
    /**
     * Implements a no-cache strategy.
     *
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class NullCache implements \Twig\Cache\CacheInterface
    {
        public function generateKey($name, $className)
        {
        }
        public function write($key, $content)
        {
        }
        public function load($key)
        {
        }
        public function getTimestamp($key)
        {
        }
    }
}
namespace Twig {
    /**
     * Compiles a node to PHP code.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class Compiler implements \Twig_CompilerInterface
    {
        protected $lastLine;
        protected $source;
        protected $indentation;
        protected $env;
        protected $debugInfo = [];
        protected $sourceOffset;
        protected $sourceLine;
        protected $filename;
        public function __construct(\Twig\Environment $env)
        {
        }
        /**
         * @deprecated since 1.25 (to be removed in 2.0)
         */
        public function getFilename()
        {
        }
        /**
         * Returns the environment instance related to this compiler.
         *
         * @return Environment
         */
        public function getEnvironment()
        {
        }
        /**
         * Gets the current PHP code after compilation.
         *
         * @return string The PHP code
         */
        public function getSource()
        {
        }
        /**
         * Compiles a node.
         *
         * @param int $indentation The current indentation
         *
         * @return $this
         */
        public function compile(\Twig_NodeInterface $node, $indentation = 0)
        {
        }
        public function subcompile(\Twig_NodeInterface $node, $raw = true)
        {
        }
        /**
         * Adds a raw string to the compiled code.
         *
         * @param string $string The string
         *
         * @return $this
         */
        public function raw($string)
        {
        }
        /**
         * Writes a string to the compiled code by adding indentation.
         *
         * @return $this
         */
        public function write()
        {
        }
        /**
         * Appends an indentation to the current PHP code after compilation.
         *
         * @return $this
         *
         * @deprecated since 1.27 (to be removed in 2.0).
         */
        public function addIndentation()
        {
        }
        /**
         * Adds a quoted string to the compiled code.
         *
         * @param string $value The string
         *
         * @return $this
         */
        public function string($value)
        {
        }
        /**
         * Returns a PHP representation of a given value.
         *
         * @param mixed $value The value to convert
         *
         * @return $this
         */
        public function repr($value)
        {
        }
        /**
         * Adds debugging information.
         *
         * @return $this
         */
        public function addDebugInfo(\Twig_NodeInterface $node)
        {
        }
        public function getDebugInfo()
        {
        }
        /**
         * Indents the generated code.
         *
         * @param int $step The number of indentation to add
         *
         * @return $this
         */
        public function indent($step = 1)
        {
        }
        /**
         * Outdents the generated code.
         *
         * @param int $step The number of indentation to remove
         *
         * @return $this
         *
         * @throws \LogicException When trying to outdent too much so the indentation would become negative
         */
        public function outdent($step = 1)
        {
        }
        public function getVarName()
        {
        }
    }
    /**
     * Stores the Twig configuration and renders templates.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class Environment
    {
        public const VERSION = '1.44.7';
        public const VERSION_ID = 14407;
        public const MAJOR_VERSION = 1;
        public const MINOR_VERSION = 44;
        public const RELEASE_VERSION = 7;
        public const EXTRA_VERSION = '';
        protected $charset;
        protected $loader;
        protected $debug;
        protected $autoReload;
        protected $cache;
        protected $lexer;
        protected $parser;
        protected $compiler;
        protected $baseTemplateClass;
        protected $extensions;
        protected $parsers;
        protected $visitors;
        protected $filters;
        protected $tests;
        protected $functions;
        protected $globals;
        protected $runtimeInitialized = false;
        protected $extensionInitialized = false;
        protected $loadedTemplates;
        protected $strictVariables;
        protected $unaryOperators;
        protected $binaryOperators;
        protected $templateClassPrefix = '__TwigTemplate_';
        protected $functionCallbacks = [];
        protected $filterCallbacks = [];
        protected $staging;
        /**
         * Constructor.
         *
         * Available options:
         *
         *  * debug: When set to true, it automatically set "auto_reload" to true as
         *           well (default to false).
         *
         *  * charset: The charset used by the templates (default to UTF-8).
         *
         *  * base_template_class: The base template class to use for generated
         *                         templates (default to \Twig\Template).
         *
         *  * cache: An absolute path where to store the compiled templates,
         *           a \Twig\Cache\CacheInterface implementation,
         *           or false to disable compilation cache (default).
         *
         *  * auto_reload: Whether to reload the template if the original source changed.
         *                 If you don't provide the auto_reload option, it will be
         *                 determined automatically based on the debug value.
         *
         *  * strict_variables: Whether to ignore invalid variables in templates
         *                      (default to false).
         *
         *  * autoescape: Whether to enable auto-escaping (default to html):
         *                  * false: disable auto-escaping
         *                  * true: equivalent to html
         *                  * html, js: set the autoescaping to one of the supported strategies
         *                  * name: set the autoescaping strategy based on the template name extension
         *                  * PHP callback: a PHP callback that returns an escaping strategy based on the template "name"
         *
         *  * optimizations: A flag that indicates which optimizations to apply
         *                   (default to -1 which means that all optimizations are enabled;
         *                   set it to 0 to disable).
         */
        public function __construct(\Twig\Loader\LoaderInterface $loader = null, $options = [])
        {
        }
        /**
         * Gets the base template class for compiled templates.
         *
         * @return string The base template class name
         */
        public function getBaseTemplateClass()
        {
        }
        /**
         * Sets the base template class for compiled templates.
         *
         * @param string $class The base template class name
         */
        public function setBaseTemplateClass($class)
        {
        }
        /**
         * Enables debugging mode.
         */
        public function enableDebug()
        {
        }
        /**
         * Disables debugging mode.
         */
        public function disableDebug()
        {
        }
        /**
         * Checks if debug mode is enabled.
         *
         * @return bool true if debug mode is enabled, false otherwise
         */
        public function isDebug()
        {
        }
        /**
         * Enables the auto_reload option.
         */
        public function enableAutoReload()
        {
        }
        /**
         * Disables the auto_reload option.
         */
        public function disableAutoReload()
        {
        }
        /**
         * Checks if the auto_reload option is enabled.
         *
         * @return bool true if auto_reload is enabled, false otherwise
         */
        public function isAutoReload()
        {
        }
        /**
         * Enables the strict_variables option.
         */
        public function enableStrictVariables()
        {
        }
        /**
         * Disables the strict_variables option.
         */
        public function disableStrictVariables()
        {
        }
        /**
         * Checks if the strict_variables option is enabled.
         *
         * @return bool true if strict_variables is enabled, false otherwise
         */
        public function isStrictVariables()
        {
        }
        /**
         * Gets the current cache implementation.
         *
         * @param bool $original Whether to return the original cache option or the real cache instance
         *
         * @return CacheInterface|string|false A Twig\Cache\CacheInterface implementation,
         *                                     an absolute path to the compiled templates,
         *                                     or false to disable cache
         */
        public function getCache($original = true)
        {
        }
        /**
         * Sets the current cache implementation.
         *
         * @param CacheInterface|string|false $cache A Twig\Cache\CacheInterface implementation,
         *                                           an absolute path to the compiled templates,
         *                                           or false to disable cache
         */
        public function setCache($cache)
        {
        }
        /**
         * Gets the cache filename for a given template.
         *
         * @param string $name The template name
         *
         * @return string|false The cache file name or false when caching is disabled
         *
         * @deprecated since 1.22 (to be removed in 2.0)
         */
        public function getCacheFilename($name)
        {
        }
        /**
         * Gets the template class associated with the given string.
         *
         * The generated template class is based on the following parameters:
         *
         *  * The cache key for the given template;
         *  * The currently enabled extensions;
         *  * Whether the Twig C extension is available or not;
         *  * PHP version;
         *  * Twig version;
         *  * Options with what environment was created.
         *
         * @param string   $name  The name for which to calculate the template class name
         * @param int|null $index The index if it is an embedded template
         *
         * @return string The template class name
         */
        public function getTemplateClass($name, $index = null)
        {
        }
        /**
         * Gets the template class prefix.
         *
         * @return string The template class prefix
         *
         * @deprecated since 1.22 (to be removed in 2.0)
         */
        public function getTemplateClassPrefix()
        {
        }
        /**
         * Renders a template.
         *
         * @param string|TemplateWrapper $name    The template name
         * @param array                  $context An array of parameters to pass to the template
         *
         * @return string The rendered template
         *
         * @throws LoaderError  When the template cannot be found
         * @throws SyntaxError  When an error occurred during compilation
         * @throws RuntimeError When an error occurred during rendering
         */
        public function render($name, array $context = [])
        {
        }
        /**
         * Displays a template.
         *
         * @param string|TemplateWrapper $name    The template name
         * @param array                  $context An array of parameters to pass to the template
         *
         * @throws LoaderError  When the template cannot be found
         * @throws SyntaxError  When an error occurred during compilation
         * @throws RuntimeError When an error occurred during rendering
         */
        public function display($name, array $context = [])
        {
        }
        /**
         * Loads a template.
         *
         * @param string|TemplateWrapper|\Twig\Template $name The template name
         *
         * @throws LoaderError  When the template cannot be found
         * @throws RuntimeError When a previously generated cache is corrupted
         * @throws SyntaxError  When an error occurred during compilation
         *
         * @return TemplateWrapper
         */
        public function load($name)
        {
        }
        /**
         * Loads a template internal representation.
         *
         * This method is for internal use only and should never be called
         * directly.
         *
         * @param string $name  The template name
         * @param int    $index The index if it is an embedded template
         *
         * @return \Twig_TemplateInterface A template instance representing the given template name
         *
         * @throws LoaderError  When the template cannot be found
         * @throws RuntimeError When a previously generated cache is corrupted
         * @throws SyntaxError  When an error occurred during compilation
         *
         * @internal
         */
        public function loadTemplate($name, $index = null)
        {
        }
        /**
         * @internal
         */
        public function loadClass($cls, $name, $index = null)
        {
        }
        /**
         * Creates a template from source.
         *
         * This method should not be used as a generic way to load templates.
         *
         * @param string $template The template source
         * @param string $name     An optional name of the template to be used in error messages
         *
         * @return TemplateWrapper A template instance representing the given template name
         *
         * @throws LoaderError When the template cannot be found
         * @throws SyntaxError When an error occurred during compilation
         */
        public function createTemplate($template, $name = null)
        {
        }
        /**
         * Returns true if the template is still fresh.
         *
         * Besides checking the loader for freshness information,
         * this method also checks if the enabled extensions have
         * not changed.
         *
         * @param string $name The template name
         * @param int    $time The last modification time of the cached template
         *
         * @return bool true if the template is fresh, false otherwise
         */
        public function isTemplateFresh($name, $time)
        {
        }
        /**
         * Tries to load a template consecutively from an array.
         *
         * Similar to load() but it also accepts instances of \Twig\Template and
         * \Twig\TemplateWrapper, and an array of templates where each is tried to be loaded.
         *
         * @param string|Template|\Twig\TemplateWrapper|array $names A template or an array of templates to try consecutively
         *
         * @return TemplateWrapper|Template
         *
         * @throws LoaderError When none of the templates can be found
         * @throws SyntaxError When an error occurred during compilation
         */
        public function resolveTemplate($names)
        {
        }
        /**
         * Clears the internal template cache.
         *
         * @deprecated since 1.18.3 (to be removed in 2.0)
         */
        public function clearTemplateCache()
        {
        }
        /**
         * Clears the template cache files on the filesystem.
         *
         * @deprecated since 1.22 (to be removed in 2.0)
         */
        public function clearCacheFiles()
        {
        }
        /**
         * Gets the Lexer instance.
         *
         * @return \Twig_LexerInterface
         *
         * @deprecated since 1.25 (to be removed in 2.0)
         */
        public function getLexer()
        {
        }
        public function setLexer(\Twig_LexerInterface $lexer)
        {
        }
        /**
         * Tokenizes a source code.
         *
         * @param string|Source $source The template source code
         * @param string        $name   The template name (deprecated)
         *
         * @return TokenStream
         *
         * @throws SyntaxError When the code is syntactically wrong
         */
        public function tokenize($source, $name = null)
        {
        }
        /**
         * Gets the Parser instance.
         *
         * @return \Twig_ParserInterface
         *
         * @deprecated since 1.25 (to be removed in 2.0)
         */
        public function getParser()
        {
        }
        public function setParser(\Twig_ParserInterface $parser)
        {
        }
        /**
         * Converts a token stream to a node tree.
         *
         * @return ModuleNode
         *
         * @throws SyntaxError When the token stream is syntactically or semantically wrong
         */
        public function parse(\Twig\TokenStream $stream)
        {
        }
        /**
         * Gets the Compiler instance.
         *
         * @return \Twig_CompilerInterface
         *
         * @deprecated since 1.25 (to be removed in 2.0)
         */
        public function getCompiler()
        {
        }
        public function setCompiler(\Twig_CompilerInterface $compiler)
        {
        }
        /**
         * Compiles a node and returns the PHP code.
         *
         * @return string The compiled PHP source code
         */
        public function compile(\Twig_NodeInterface $node)
        {
        }
        /**
         * Compiles a template source code.
         *
         * @param string|Source $source The template source code
         * @param string        $name   The template name (deprecated)
         *
         * @return string The compiled PHP source code
         *
         * @throws SyntaxError When there was an error during tokenizing, parsing or compiling
         */
        public function compileSource($source, $name = null)
        {
        }
        public function setLoader(\Twig\Loader\LoaderInterface $loader)
        {
        }
        /**
         * Gets the Loader instance.
         *
         * @return LoaderInterface
         */
        public function getLoader()
        {
        }
        /**
         * Sets the default template charset.
         *
         * @param string $charset The default charset
         */
        public function setCharset($charset)
        {
        }
        /**
         * Gets the default template charset.
         *
         * @return string The default charset
         */
        public function getCharset()
        {
        }
        /**
         * Initializes the runtime environment.
         *
         * @deprecated since 1.23 (to be removed in 2.0)
         */
        public function initRuntime()
        {
        }
        /**
         * Returns true if the given extension is registered.
         *
         * @param string $class The extension class name
         *
         * @return bool Whether the extension is registered or not
         */
        public function hasExtension($class)
        {
        }
        /**
         * Adds a runtime loader.
         */
        public function addRuntimeLoader(\Twig\RuntimeLoader\RuntimeLoaderInterface $loader)
        {
        }
        /**
         * Gets an extension by class name.
         *
         * @param string $class The extension class name
         *
         * @return ExtensionInterface
         */
        public function getExtension($class)
        {
        }
        /**
         * Returns the runtime implementation of a Twig element (filter/function/test).
         *
         * @param string $class A runtime class name
         *
         * @return object The runtime implementation
         *
         * @throws RuntimeError When the template cannot be found
         */
        public function getRuntime($class)
        {
        }
        public function addExtension(\Twig\Extension\ExtensionInterface $extension)
        {
        }
        /**
         * Removes an extension by name.
         *
         * This method is deprecated and you should not use it.
         *
         * @param string $name The extension name
         *
         * @deprecated since 1.12 (to be removed in 2.0)
         */
        public function removeExtension($name)
        {
        }
        /**
         * Registers an array of extensions.
         *
         * @param array $extensions An array of extensions
         */
        public function setExtensions(array $extensions)
        {
        }
        /**
         * Returns all registered extensions.
         *
         * @return ExtensionInterface[] An array of extensions (keys are for internal usage only and should not be relied on)
         */
        public function getExtensions()
        {
        }
        public function addTokenParser(\Twig\TokenParser\TokenParserInterface $parser)
        {
        }
        /**
         * Gets the registered Token Parsers.
         *
         * @return \Twig_TokenParserBrokerInterface
         *
         * @internal
         */
        public function getTokenParsers()
        {
        }
        /**
         * Gets registered tags.
         *
         * Be warned that this method cannot return tags defined by \Twig_TokenParserBrokerInterface classes.
         *
         * @return TokenParserInterface[]
         *
         * @internal
         */
        public function getTags()
        {
        }
        public function addNodeVisitor(\Twig\NodeVisitor\NodeVisitorInterface $visitor)
        {
        }
        /**
         * Gets the registered Node Visitors.
         *
         * @return NodeVisitorInterface[]
         *
         * @internal
         */
        public function getNodeVisitors()
        {
        }
        /**
         * Registers a Filter.
         *
         * @param string|TwigFilter                $name   The filter name or a \Twig_SimpleFilter instance
         * @param \Twig_FilterInterface|TwigFilter $filter
         */
        public function addFilter($name, $filter = null)
        {
        }
        /**
         * Get a filter by name.
         *
         * Subclasses may override this method and load filters differently;
         * so no list of filters is available.
         *
         * @param string $name The filter name
         *
         * @return \Twig_Filter|false
         *
         * @internal
         */
        public function getFilter($name)
        {
        }
        public function registerUndefinedFilterCallback($callable)
        {
        }
        /**
         * Gets the registered Filters.
         *
         * Be warned that this method cannot return filters defined with registerUndefinedFilterCallback.
         *
         * @return \Twig_FilterInterface[]
         *
         * @see registerUndefinedFilterCallback
         *
         * @internal
         */
        public function getFilters()
        {
        }
        /**
         * Registers a Test.
         *
         * @param string|TwigTest              $name The test name or a \Twig_SimpleTest instance
         * @param \Twig_TestInterface|TwigTest $test A \Twig_TestInterface instance or a \Twig_SimpleTest instance
         */
        public function addTest($name, $test = null)
        {
        }
        /**
         * Gets the registered Tests.
         *
         * @return \Twig_TestInterface[]
         *
         * @internal
         */
        public function getTests()
        {
        }
        /**
         * Gets a test by name.
         *
         * @param string $name The test name
         *
         * @return \Twig_Test|false
         *
         * @internal
         */
        public function getTest($name)
        {
        }
        /**
         * Registers a Function.
         *
         * @param string|TwigFunction                  $name     The function name or a \Twig_SimpleFunction instance
         * @param \Twig_FunctionInterface|TwigFunction $function
         */
        public function addFunction($name, $function = null)
        {
        }
        /**
         * Get a function by name.
         *
         * Subclasses may override this method and load functions differently;
         * so no list of functions is available.
         *
         * @param string $name function name
         *
         * @return \Twig_Function|false
         *
         * @internal
         */
        public function getFunction($name)
        {
        }
        public function registerUndefinedFunctionCallback($callable)
        {
        }
        /**
         * Gets registered functions.
         *
         * Be warned that this method cannot return functions defined with registerUndefinedFunctionCallback.
         *
         * @return \Twig_FunctionInterface[]
         *
         * @see registerUndefinedFunctionCallback
         *
         * @internal
         */
        public function getFunctions()
        {
        }
        /**
         * Registers a Global.
         *
         * New globals can be added before compiling or rendering a template;
         * but after, you can only update existing globals.
         *
         * @param string $name  The global name
         * @param mixed  $value The global value
         */
        public function addGlobal($name, $value)
        {
        }
        /**
         * Gets the registered Globals.
         *
         * @return array An array of globals
         *
         * @internal
         */
        public function getGlobals()
        {
        }
        /**
         * Merges a context with the defined globals.
         *
         * @param array $context An array representing the context
         *
         * @return array The context merged with the globals
         */
        public function mergeGlobals(array $context)
        {
        }
        /**
         * Gets the registered unary Operators.
         *
         * @return array An array of unary operators
         *
         * @internal
         */
        public function getUnaryOperators()
        {
        }
        /**
         * Gets the registered binary Operators.
         *
         * @return array An array of binary operators
         *
         * @internal
         */
        public function getBinaryOperators()
        {
        }
        /**
         * @deprecated since 1.23 (to be removed in 2.0)
         */
        public function computeAlternatives($name, $items)
        {
        }
        /**
         * @internal
         */
        protected function initGlobals()
        {
        }
        /**
         * @internal
         */
        protected function initExtensions()
        {
        }
        /**
         * @internal
         */
        protected function initExtension(\Twig\Extension\ExtensionInterface $extension)
        {
        }
        /**
         * @deprecated since 1.22 (to be removed in 2.0)
         */
        protected function writeCacheFile($file, $content)
        {
        }
    }
}
namespace Twig\Error {
    /**
     * Twig base exception.
     *
     * This exception class and its children must only be used when
     * an error occurs during the loading of a template, when a syntax error
     * is detected in a template, or when rendering a template. Other
     * errors must use regular PHP exception classes (like when the template
     * cache directory is not writable for instance).
     *
     * To help debugging template issues, this class tracks the original template
     * name and line where the error occurred.
     *
     * Whenever possible, you must set these information (original template name
     * and line number) yourself by passing them to the constructor. If some or all
     * these information are not available from where you throw the exception, then
     * this class will guess them automatically (when the line number is set to -1
     * and/or the name is set to null). As this is a costly operation, this
     * can be disabled by passing false for both the name and the line number
     * when creating a new instance of this class.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class Error extends \Exception
    {
        protected $lineno;
        // to be renamed to name in 2.0
        protected $filename;
        protected $rawMessage;
        /**
         * Constructor.
         *
         * Set the line number to -1 to enable its automatic guessing.
         * Set the name to null to enable its automatic guessing.
         *
         * @param string             $message  The error message
         * @param int                $lineno   The template line where the error occurred
         * @param Source|string|null $source   The source context where the error occurred
         * @param \Exception         $previous The previous exception
         */
        public function __construct($message, $lineno = -1, $source = null, \Exception $previous = null)
        {
        }
        /**
         * Gets the raw message.
         *
         * @return string The raw message
         */
        public function getRawMessage()
        {
        }
        /**
         * Gets the logical name where the error occurred.
         *
         * @return string The name
         *
         * @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead.
         */
        public function getTemplateFile()
        {
        }
        /**
         * Sets the logical name where the error occurred.
         *
         * @param string $name The name
         *
         * @deprecated since 1.27 (to be removed in 2.0). Use setSourceContext() instead.
         */
        public function setTemplateFile($name)
        {
        }
        /**
         * Gets the logical name where the error occurred.
         *
         * @return string The name
         *
         * @deprecated since 1.29 (to be removed in 2.0). Use getSourceContext() instead.
         */
        public function getTemplateName()
        {
        }
        /**
         * Sets the logical name where the error occurred.
         *
         * @param string $name The name
         *
         * @deprecated since 1.29 (to be removed in 2.0). Use setSourceContext() instead.
         */
        public function setTemplateName($name)
        {
        }
        /**
         * Gets the template line where the error occurred.
         *
         * @return int The template line
         */
        public function getTemplateLine()
        {
        }
        /**
         * Sets the template line where the error occurred.
         *
         * @param int $lineno The template line
         */
        public function setTemplateLine($lineno)
        {
        }
        /**
         * Gets the source context of the Twig template where the error occurred.
         *
         * @return Source|null
         */
        public function getSourceContext()
        {
        }
        /**
         * Sets the source context of the Twig template where the error occurred.
         */
        public function setSourceContext(\Twig\Source $source = null)
        {
        }
        public function guess()
        {
        }
        public function appendMessage($rawMessage)
        {
        }
        /**
         * @internal
         */
        protected function updateRepr()
        {
        }
        /**
         * @internal
         */
        protected function guessTemplateInfo()
        {
        }
    }
    /**
     * Exception thrown when an error occurs during template loading.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class LoaderError extends \Twig\Error\Error
    {
    }
    /**
     * Exception thrown when an error occurs at runtime.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class RuntimeError extends \Twig\Error\Error
    {
    }
    /**
     * \Exception thrown when a syntax error occurs during lexing or parsing of a template.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class SyntaxError extends \Twig\Error\Error
    {
        /**
         * Tweaks the error message to include suggestions.
         *
         * @param string $name  The original name of the item that does not exist
         * @param array  $items An array of possible items
         */
        public function addSuggestions($name, array $items)
        {
        }
        /**
         * @internal
         *
         * To be merged with the addSuggestions() method in 2.0.
         */
        public static function computeAlternatives($name, $items)
        {
        }
    }
}
namespace Twig {
    /**
     * Parses expressions.
     *
     * This parser implements a "Precedence climbing" algorithm.
     *
     * @see https://www.engr.mun.ca/~theo/Misc/exp_parsing.htm
     * @see https://en.wikipedia.org/wiki/Operator-precedence_parser
     *
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @internal
     */
    class ExpressionParser
    {
        public const OPERATOR_LEFT = 1;
        public const OPERATOR_RIGHT = 2;
        protected $parser;
        protected $unaryOperators;
        protected $binaryOperators;
        public function __construct(\Twig\Parser $parser, $env = null)
        {
        }
        public function parseExpression($precedence = 0, $allowArrow = false)
        {
        }
        protected function getPrimary()
        {
        }
        protected function parseConditionalExpression($expr)
        {
        }
        protected function isUnary(\Twig\Token $token)
        {
        }
        protected function isBinary(\Twig\Token $token)
        {
        }
        public function parsePrimaryExpression()
        {
        }
        public function parseStringExpression()
        {
        }
        public function parseArrayExpression()
        {
        }
        public function parseHashExpression()
        {
        }
        public function parsePostfixExpression($node)
        {
        }
        public function getFunctionNode($name, $line)
        {
        }
        public function parseSubscriptExpression($node)
        {
        }
        public function parseFilterExpression($node)
        {
        }
        public function parseFilterExpressionRaw($node, $tag = null)
        {
        }
        /**
         * Parses arguments.
         *
         * @param bool $namedArguments Whether to allow named arguments or not
         * @param bool $definition     Whether we are parsing arguments for a function definition
         *
         * @return Node
         *
         * @throws SyntaxError
         */
        public function parseArguments($namedArguments = false, $definition = false, $allowArrow = false)
        {
        }
        public function parseAssignmentExpression()
        {
        }
        public function parseMultitargetExpression()
        {
        }
        protected function getFunctionNodeClass($name, $line)
        {
        }
        protected function getFilterNodeClass($name, $line)
        {
        }
        // checks that the node only contains "constant" elements
        protected function checkConstantExpression(\Twig_NodeInterface $node)
        {
        }
    }
}
namespace Twig\Extension {
    /**
     * Interface implemented by extension classes.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    interface ExtensionInterface
    {
        /**
         * Initializes the runtime environment.
         *
         * This is where you can load some file that contains filter functions for instance.
         *
         * @deprecated since 1.23 (to be removed in 2.0), implement \Twig_Extension_InitRuntimeInterface instead
         */
        public function initRuntime(\Twig\Environment $environment);
        /**
         * Returns the token parser instances to add to the existing list.
         *
         * @return TokenParserInterface[]
         */
        public function getTokenParsers();
        /**
         * Returns the node visitor instances to add to the existing list.
         *
         * @return NodeVisitorInterface[]
         */
        public function getNodeVisitors();
        /**
         * Returns a list of filters to add to the existing list.
         *
         * @return TwigFilter[]
         */
        public function getFilters();
        /**
         * Returns a list of tests to add to the existing list.
         *
         * @return TwigTest[]
         */
        public function getTests();
        /**
         * Returns a list of functions to add to the existing list.
         *
         * @return TwigFunction[]
         */
        public function getFunctions();
        /**
         * Returns a list of operators to add to the existing list.
         *
         * @return array<array> First array of unary operators, second array of binary operators
         */
        public function getOperators();
        /**
         * Returns a list of global variables to add to the existing list.
         *
         * @return array An array of global variables
         *
         * @deprecated since 1.23 (to be removed in 2.0), implement \Twig_Extension_GlobalsInterface instead
         */
        public function getGlobals();
        /**
         * Returns the name of the extension.
         *
         * @return string The extension name
         *
         * @deprecated since 1.26 (to be removed in 2.0), not used anymore internally
         */
        public function getName();
    }
    abstract class AbstractExtension implements \Twig\Extension\ExtensionInterface
    {
        /**
         * @deprecated since 1.23 (to be removed in 2.0), implement \Twig_Extension_InitRuntimeInterface instead
         */
        public function initRuntime(\Twig\Environment $environment)
        {
        }
        public function getTokenParsers()
        {
        }
        public function getNodeVisitors()
        {
        }
        public function getFilters()
        {
        }
        public function getTests()
        {
        }
        public function getFunctions()
        {
        }
        public function getOperators()
        {
        }
        /**
         * @deprecated since 1.23 (to be removed in 2.0), implement \Twig_Extension_GlobalsInterface instead
         */
        public function getGlobals()
        {
        }
        /**
         * @deprecated since 1.26 (to be removed in 2.0), not used anymore internally
         */
        public function getName()
        {
        }
    }
    /**
     * @final
     */
    class CoreExtension extends \Twig\Extension\AbstractExtension
    {
        protected $dateFormats = ['F j, Y H:i', '%d days'];
        protected $numberFormat = [0, '.', ','];
        protected $timezone = null;
        protected $escapers = [];
        /**
         * Defines a new escaper to be used via the escape filter.
         *
         * @param string   $strategy The strategy name that should be used as a strategy in the escape call
         * @param callable $callable A valid PHP callable
         */
        public function setEscaper($strategy, $callable)
        {
        }
        /**
         * Gets all defined escapers.
         *
         * @return array An array of escapers
         */
        public function getEscapers()
        {
        }
        /**
         * Sets the default format to be used by the date filter.
         *
         * @param string $format             The default date format string
         * @param string $dateIntervalFormat The default date interval format string
         */
        public function setDateFormat($format = null, $dateIntervalFormat = null)
        {
        }
        /**
         * Gets the default format to be used by the date filter.
         *
         * @return array The default date format string and the default date interval format string
         */
        public function getDateFormat()
        {
        }
        /**
         * Sets the default timezone to be used by the date filter.
         *
         * @param \DateTimeZone|string $timezone The default timezone string or a \DateTimeZone object
         */
        public function setTimezone($timezone)
        {
        }
        /**
         * Gets the default timezone to be used by the date filter.
         *
         * @return \DateTimeZone The default timezone currently in use
         */
        public function getTimezone()
        {
        }
        /**
         * Sets the default format to be used by the number_format filter.
         *
         * @param int    $decimal      the number of decimal places to use
         * @param string $decimalPoint the character(s) to use for the decimal point
         * @param string $thousandSep  the character(s) to use for the thousands separator
         */
        public function setNumberFormat($decimal, $decimalPoint, $thousandSep)
        {
        }
        /**
         * Get the default format used by the number_format filter.
         *
         * @return array The arguments for number_format()
         */
        public function getNumberFormat()
        {
        }
        public function getTokenParsers()
        {
        }
        public function getFilters()
        {
        }
        public function getFunctions()
        {
        }
        public function getTests()
        {
        }
        public function getOperators()
        {
        }
        public function getName()
        {
        }
    }
    /**
     * @final
     */
    class DebugExtension extends \Twig\Extension\AbstractExtension
    {
        public function getFunctions()
        {
        }
        public function getName()
        {
        }
    }
    /**
     * @final
     */
    class EscaperExtension extends \Twig\Extension\AbstractExtension
    {
        protected $defaultStrategy;
        /**
         * @param string|false|callable $defaultStrategy An escaping strategy
         *
         * @see setDefaultStrategy()
         */
        public function __construct($defaultStrategy = 'html')
        {
        }
        public function getTokenParsers()
        {
        }
        public function getNodeVisitors()
        {
        }
        public function getFilters()
        {
        }
        /**
         * Sets the default strategy to use when not defined by the user.
         *
         * The strategy can be a valid PHP callback that takes the template
         * name as an argument and returns the strategy to use.
         *
         * @param string|false|callable $defaultStrategy An escaping strategy
         */
        public function setDefaultStrategy($defaultStrategy)
        {
        }
        /**
         * Gets the default strategy to use when not defined by the user.
         *
         * @param string $name The template name
         *
         * @return string|false The default strategy to use for the template
         */
        public function getDefaultStrategy($name)
        {
        }
        public function getName()
        {
        }
    }
    /**
     * Enables usage of the deprecated Twig\Extension\AbstractExtension::getGlobals() method.
     *
     * Explicitly implement this interface if you really need to implement the
     * deprecated getGlobals() method in your extensions.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    interface GlobalsInterface
    {
    }
    /**
     * Enables usage of the deprecated Twig\Extension\AbstractExtension::initRuntime() method.
     *
     * Explicitly implement this interface if you really need to implement the
     * deprecated initRuntime() method in your extensions.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    interface InitRuntimeInterface
    {
    }
    /**
     * @final
     */
    class OptimizerExtension extends \Twig\Extension\AbstractExtension
    {
        protected $optimizers;
        public function __construct($optimizers = -1)
        {
        }
        public function getNodeVisitors()
        {
        }
        public function getName()
        {
        }
    }
    class ProfilerExtension extends \Twig\Extension\AbstractExtension
    {
        public function __construct(\Twig\Profiler\Profile $profile)
        {
        }
        public function enter(\Twig\Profiler\Profile $profile)
        {
        }
        public function leave(\Twig\Profiler\Profile $profile)
        {
        }
        public function getNodeVisitors()
        {
        }
        public function getName()
        {
        }
    }
    /**
     * @author Grégoire Pineau <lyrixx@lyrixx.info>
     */
    interface RuntimeExtensionInterface
    {
    }
    /**
     * @final
     */
    class SandboxExtension extends \Twig\Extension\AbstractExtension
    {
        protected $sandboxedGlobally;
        protected $sandboxed;
        protected $policy;
        public function __construct(\Twig\Sandbox\SecurityPolicyInterface $policy, $sandboxed = false)
        {
        }
        public function getTokenParsers()
        {
        }
        public function getNodeVisitors()
        {
        }
        public function enableSandbox()
        {
        }
        public function disableSandbox()
        {
        }
        public function isSandboxed()
        {
        }
        public function isSandboxedGlobally()
        {
        }
        public function setSecurityPolicy(\Twig\Sandbox\SecurityPolicyInterface $policy)
        {
        }
        public function getSecurityPolicy()
        {
        }
        public function checkSecurity($tags, $filters, $functions)
        {
        }
        public function checkMethodAllowed($obj, $method)
        {
        }
        public function checkPropertyAllowed($obj, $method)
        {
        }
        public function ensureToStringAllowed($obj)
        {
        }
        public function getName()
        {
        }
    }
    /**
     * Internal class.
     *
     * This class is used by \Twig\Environment as a staging area and must not be used directly.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @internal
     */
    class StagingExtension extends \Twig\Extension\AbstractExtension
    {
        protected $functions = [];
        protected $filters = [];
        protected $visitors = [];
        protected $tokenParsers = [];
        protected $globals = [];
        protected $tests = [];
        public function addFunction($name, $function)
        {
        }
        public function getFunctions()
        {
        }
        public function addFilter($name, $filter)
        {
        }
        public function getFilters()
        {
        }
        public function addNodeVisitor(\Twig\NodeVisitor\NodeVisitorInterface $visitor)
        {
        }
        public function getNodeVisitors()
        {
        }
        public function addTokenParser(\Twig\TokenParser\TokenParserInterface $parser)
        {
        }
        public function getTokenParsers()
        {
        }
        public function addGlobal($name, $value)
        {
        }
        public function getGlobals()
        {
        }
        public function addTest($name, $test)
        {
        }
        public function getTests()
        {
        }
        public function getName()
        {
        }
    }
    /**
     * @final
     */
    class StringLoaderExtension extends \Twig\Extension\AbstractExtension
    {
        public function getFunctions()
        {
        }
        public function getName()
        {
        }
    }
}
namespace Twig {
    /**
     * Default autoescaping strategy based on file names.
     *
     * This strategy sets the HTML as the default autoescaping strategy,
     * but changes it based on the template name.
     *
     * Note that there is no runtime performance impact as the
     * default autoescaping strategy is set at compilation time.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class FileExtensionEscapingStrategy
    {
        /**
         * Guesses the best autoescaping strategy based on the file name.
         *
         * @param string $name The template name
         *
         * @return string|false The escaping strategy name to use or false to disable
         */
        public static function guess($name)
        {
        }
    }
    /**
     * Lexes a template string.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class Lexer implements \Twig_LexerInterface
    {
        protected $tokens;
        protected $code;
        protected $cursor;
        protected $lineno;
        protected $end;
        protected $state;
        protected $states;
        protected $brackets;
        protected $env;
        // to be renamed to $name in 2.0 (where it is private)
        protected $filename;
        protected $options;
        protected $regexes;
        protected $position;
        protected $positions;
        protected $currentVarBlockLine;
        public const STATE_DATA = 0;
        public const STATE_BLOCK = 1;
        public const STATE_VAR = 2;
        public const STATE_STRING = 3;
        public const STATE_INTERPOLATION = 4;
        public const REGEX_NAME = '/[a-zA-Z_\\x7f-\\xff][a-zA-Z0-9_\\x7f-\\xff]*/A';
        public const REGEX_NUMBER = '/[0-9]+(?:\\.[0-9]+)?([Ee][\\+\\-][0-9]+)?/A';
        public const REGEX_STRING = '/"([^#"\\\\]*(?:\\\\.[^#"\\\\]*)*)"|\'([^\'\\\\]*(?:\\\\.[^\'\\\\]*)*)\'/As';
        public const REGEX_DQ_STRING_DELIM = '/"/A';
        public const REGEX_DQ_STRING_PART = '/[^#"\\\\]*(?:(?:\\\\.|#(?!\\{))[^#"\\\\]*)*/As';
        public const PUNCTUATION = '()[]{}?:.,|';
        public function __construct(\Twig\Environment $env, array $options = [])
        {
        }
        public function tokenize($code, $name = null)
        {
        }
        protected function lexData()
        {
        }
        protected function lexBlock()
        {
        }
        protected function lexVar()
        {
        }
        protected function lexExpression()
        {
        }
        protected function lexRawData($tag)
        {
        }
        protected function lexComment()
        {
        }
        protected function lexString()
        {
        }
        protected function lexInterpolation()
        {
        }
        protected function pushToken($type, $value = '')
        {
        }
        protected function moveCursor($text)
        {
        }
        protected function getOperatorRegex()
        {
        }
        protected function pushState($state)
        {
        }
        protected function popState()
        {
        }
    }
}
namespace Twig\Loader {
    /**
     * Interface all loaders must implement.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    interface LoaderInterface
    {
        /**
         * Gets the source code of a template, given its name.
         *
         * @param string $name The name of the template to load
         *
         * @return string The template source code
         *
         * @throws LoaderError When $name is not found
         *
         * @deprecated since 1.27 (to be removed in 2.0), implement Twig\Loader\SourceContextLoaderInterface
         */
        public function getSource($name);
        /**
         * Gets the cache key to use for the cache for a given template name.
         *
         * @param string $name The name of the template to load
         *
         * @return string The cache key
         *
         * @throws LoaderError When $name is not found
         */
        public function getCacheKey($name);
        /**
         * Returns true if the template is still fresh.
         *
         * @param string $name The template name
         * @param int    $time Timestamp of the last modification time of the
         *                     cached template
         *
         * @return bool true if the template is fresh, false otherwise
         *
         * @throws LoaderError When $name is not found
         */
        public function isFresh($name, $time);
    }
    /**
     * Adds an exists() method for loaders.
     *
     * @author Florin Patan <florinpatan@gmail.com>
     *
     * @deprecated since 1.12 (to be removed in 3.0)
     */
    interface ExistsLoaderInterface
    {
        /**
         * Check if we have the source code of a template, given its name.
         *
         * @param string $name The name of the template to check if we can load
         *
         * @return bool If the template source code is handled by this loader or not
         */
        public function exists($name);
    }
    /**
     * Adds a getSourceContext() method for loaders.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @deprecated since 1.27 (to be removed in 3.0)
     */
    interface SourceContextLoaderInterface
    {
        /**
         * Returns the source context for a given template logical name.
         *
         * @param string $name The template logical name
         *
         * @return Source
         *
         * @throws LoaderError When $name is not found
         */
        public function getSourceContext($name);
    }
    /**
     * Loads a template from an array.
     *
     * When using this loader with a cache mechanism, you should know that a new cache
     * key is generated each time a template content "changes" (the cache key being the
     * source code of the template). If you don't want to see your cache grows out of
     * control, you need to take care of clearing the old cache file by yourself.
     *
     * This loader should only be used for unit testing.
     *
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class ArrayLoader implements \Twig\Loader\LoaderInterface, \Twig\Loader\ExistsLoaderInterface, \Twig\Loader\SourceContextLoaderInterface
    {
        protected $templates = [];
        /**
         * @param array $templates An array of templates (keys are the names, and values are the source code)
         */
        public function __construct(array $templates = [])
        {
        }
        /**
         * Adds or overrides a template.
         *
         * @param string $name     The template name
         * @param string $template The template source
         */
        public function setTemplate($name, $template)
        {
        }
        public function getSource($name)
        {
        }
        public function getSourceContext($name)
        {
        }
        public function exists($name)
        {
        }
        public function getCacheKey($name)
        {
        }
        public function isFresh($name, $time)
        {
        }
    }
    /**
     * Loads templates from other loaders.
     *
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class ChainLoader implements \Twig\Loader\LoaderInterface, \Twig\Loader\ExistsLoaderInterface, \Twig\Loader\SourceContextLoaderInterface
    {
        protected $loaders = [];
        /**
         * @param LoaderInterface[] $loaders
         */
        public function __construct(array $loaders = [])
        {
        }
        public function addLoader(\Twig\Loader\LoaderInterface $loader)
        {
        }
        /**
         * @return LoaderInterface[]
         */
        public function getLoaders()
        {
        }
        public function getSource($name)
        {
        }
        public function getSourceContext($name)
        {
        }
        public function exists($name)
        {
        }
        public function getCacheKey($name)
        {
        }
        public function isFresh($name, $time)
        {
        }
    }
    /**
     * Loads template from the filesystem.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class FilesystemLoader implements \Twig\Loader\LoaderInterface, \Twig\Loader\ExistsLoaderInterface, \Twig\Loader\SourceContextLoaderInterface
    {
        /** Identifier of the main namespace. */
        public const MAIN_NAMESPACE = '__main__';
        protected $paths = [];
        protected $cache = [];
        protected $errorCache = [];
        /**
         * @param string|array $paths    A path or an array of paths where to look for templates
         * @param string|null  $rootPath The root path common to all relative paths (null for getcwd())
         */
        public function __construct($paths = [], $rootPath = null)
        {
        }
        /**
         * Returns the paths to the templates.
         *
         * @param string $namespace A path namespace
         *
         * @return array The array of paths where to look for templates
         */
        public function getPaths($namespace = self::MAIN_NAMESPACE)
        {
        }
        /**
         * Returns the path namespaces.
         *
         * The main namespace is always defined.
         *
         * @return array The array of defined namespaces
         */
        public function getNamespaces()
        {
        }
        /**
         * Sets the paths where templates are stored.
         *
         * @param string|array $paths     A path or an array of paths where to look for templates
         * @param string       $namespace A path namespace
         */
        public function setPaths($paths, $namespace = self::MAIN_NAMESPACE)
        {
        }
        /**
         * Adds a path where templates are stored.
         *
         * @param string $path      A path where to look for templates
         * @param string $namespace A path namespace
         *
         * @throws LoaderError
         */
        public function addPath($path, $namespace = self::MAIN_NAMESPACE)
        {
        }
        /**
         * Prepends a path where templates are stored.
         *
         * @param string $path      A path where to look for templates
         * @param string $namespace A path namespace
         *
         * @throws LoaderError
         */
        public function prependPath($path, $namespace = self::MAIN_NAMESPACE)
        {
        }
        public function getSource($name)
        {
        }
        public function getSourceContext($name)
        {
        }
        public function getCacheKey($name)
        {
        }
        public function exists($name)
        {
        }
        public function isFresh($name, $time)
        {
        }
        /**
         * Checks if the template can be found.
         *
         * @param string $name The template name
         *
         * @return string|false|null The template name or false/null
         */
        protected function findTemplate($name)
        {
        }
        protected function parseName($name, $default = self::MAIN_NAMESPACE)
        {
        }
        protected function normalizeName($name)
        {
        }
        protected function validateName($name)
        {
        }
    }
}
namespace Twig {
    /**
     * Marks a content as safe.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class Markup implements \Countable
    {
        protected $content;
        protected $charset;
        public function __construct($content, $charset)
        {
        }
        public function __toString()
        {
        }
        /**
         * @return int
         */
        public function count()
        {
        }
    }
}
namespace Twig\Node {
    /**
     * Represents a node in the AST.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class Node implements \Twig_NodeInterface
    {
        protected $nodes;
        protected $attributes;
        protected $lineno;
        protected $tag;
        /**
         * @param array  $nodes      An array of named nodes
         * @param array  $attributes An array of attributes (should not be nodes)
         * @param int    $lineno     The line number
         * @param string $tag        The tag name associated with the Node
         */
        public function __construct(array $nodes = [], array $attributes = [], $lineno = 0, $tag = null)
        {
        }
        public function __toString()
        {
        }
        /**
         * @deprecated since 1.16.1 (to be removed in 2.0)
         */
        public function toXml($asDom = false)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public function getTemplateLine()
        {
        }
        /**
         * @deprecated since 1.27 (to be removed in 2.0)
         */
        public function getLine()
        {
        }
        public function getNodeTag()
        {
        }
        /**
         * @return bool
         */
        public function hasAttribute($name)
        {
        }
        /**
         * @return mixed
         */
        public function getAttribute($name)
        {
        }
        /**
         * @param string $name
         * @param mixed  $value
         */
        public function setAttribute($name, $value)
        {
        }
        public function removeAttribute($name)
        {
        }
        /**
         * @return bool
         */
        public function hasNode($name)
        {
        }
        /**
         * @return Node
         */
        public function getNode($name)
        {
        }
        public function setNode($name, $node = null)
        {
        }
        public function removeNode($name)
        {
        }
        /**
         * @return int
         */
        public function count()
        {
        }
        /**
         * @return \Traversable
         */
        public function getIterator()
        {
        }
        public function setTemplateName($name)
        {
        }
        public function getTemplateName()
        {
        }
        public function setSourceContext(\Twig\Source $source)
        {
        }
        public function getSourceContext()
        {
        }
        /**
         * @deprecated since 1.27 (to be removed in 2.0)
         */
        public function setFilename($name)
        {
        }
        /**
         * @deprecated since 1.27 (to be removed in 2.0)
         */
        public function getFilename()
        {
        }
    }
    /**
     * Represents an autoescape node.
     *
     * The value is the escaping strategy (can be html, js, ...)
     *
     * The true value is equivalent to html.
     *
     * If autoescaping is disabled, then the value is false.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class AutoEscapeNode extends \Twig\Node\Node
    {
        public function __construct($value, \Twig_NodeInterface $body, $lineno, $tag = 'autoescape')
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a block node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class BlockNode extends \Twig\Node\Node
    {
        public function __construct($name, \Twig_NodeInterface $body, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a displayable node in the AST.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    interface NodeOutputInterface
    {
    }
    /**
     * Represents a block call node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class BlockReferenceNode extends \Twig\Node\Node implements \Twig\Node\NodeOutputInterface
    {
        public function __construct($name, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a body node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class BodyNode extends \Twig\Node\Node
    {
    }
    /**
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class CheckSecurityCallNode extends \Twig\Node\Node
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class CheckSecurityNode extends \Twig\Node\Node
    {
        protected $usedFilters;
        protected $usedTags;
        protected $usedFunctions;
        public function __construct(array $usedFilters, array $usedTags, array $usedFunctions)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
}
namespace Twig\Node\Expression {
    /**
     * Abstract class for all nodes that represents an expression.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    abstract class AbstractExpression extends \Twig\Node\Node
    {
    }
}
namespace Twig\Node {
    /**
     * Checks if casting an expression to __toString() is allowed by the sandbox.
     *
     * For instance, when there is a simple Print statement, like {{ article }},
     * and if the sandbox is enabled, we need to check that the __toString()
     * method is allowed if 'article' is an object. The same goes for {{ article|upper }}
     * or {{ random(article) }}
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class CheckToStringNode extends \Twig\Node\Expression\AbstractExpression
    {
        public function __construct(\Twig\Node\Expression\AbstractExpression $expr)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a deprecated node.
     *
     * @author Yonel Ceruto <yonelceruto@gmail.com>
     */
    class DeprecatedNode extends \Twig\Node\Node
    {
        public function __construct(\Twig\Node\Expression\AbstractExpression $expr, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a do node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class DoNode extends \Twig\Node\Node
    {
        public function __construct(\Twig\Node\Expression\AbstractExpression $expr, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents an include node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class IncludeNode extends \Twig\Node\Node implements \Twig\Node\NodeOutputInterface
    {
        public function __construct(\Twig\Node\Expression\AbstractExpression $expr, ?\Twig\Node\Expression\AbstractExpression $variables, $only, $ignoreMissing, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
        protected function addGetTemplate(\Twig\Compiler $compiler)
        {
        }
        protected function addTemplateArguments(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents an embed node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class EmbedNode extends \Twig\Node\IncludeNode
    {
        // we don't inject the module to avoid node visitors to traverse it twice (as it will be already visited in the main module)
        public function __construct($name, $index, ?\Twig\Node\Expression\AbstractExpression $variables, $only, $ignoreMissing, $lineno, $tag = null)
        {
        }
        protected function addGetTemplate(\Twig\Compiler $compiler)
        {
        }
    }
}
namespace Twig\Node\Expression {
    class ArrayExpression extends \Twig\Node\Expression\AbstractExpression
    {
        protected $index;
        public function __construct(array $elements, $lineno)
        {
        }
        public function getKeyValuePairs()
        {
        }
        public function hasElement(\Twig\Node\Expression\AbstractExpression $key)
        {
        }
        public function addElement(\Twig\Node\Expression\AbstractExpression $value, \Twig\Node\Expression\AbstractExpression $key = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents an arrow function.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class ArrowFunctionExpression extends \Twig\Node\Expression\AbstractExpression
    {
        public function __construct(\Twig\Node\Expression\AbstractExpression $expr, \Twig\Node\Node $names, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    class NameExpression extends \Twig\Node\Expression\AbstractExpression
    {
        protected $specialVars = ['_self' => '$this', '_context' => '$context', '_charset' => '$this->env->getCharset()'];
        public function __construct($name, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public function isSpecial()
        {
        }
        public function isSimple()
        {
        }
    }
    class AssignNameExpression extends \Twig\Node\Expression\NameExpression
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
}
namespace Twig\Node\Expression\Binary {
    abstract class AbstractBinary extends \Twig\Node\Expression\AbstractExpression
    {
        public function __construct(\Twig_NodeInterface $left, \Twig_NodeInterface $right, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public abstract function operator(\Twig\Compiler $compiler);
    }
    class AddBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class AndBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class BitwiseAndBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class BitwiseOrBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class BitwiseXorBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class ConcatBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class DivBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class EndsWithBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class EqualBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class FloorDivBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class GreaterBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class GreaterEqualBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class InBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class LessBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class LessEqualBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class MatchesBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class ModBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class MulBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class NotEqualBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class NotInBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class OrBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class PowerBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class RangeBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class StartsWithBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class SubBinary extends \Twig\Node\Expression\Binary\AbstractBinary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
}
namespace Twig\Node\Expression {
    /**
     * Represents a block call node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class BlockReferenceExpression extends \Twig\Node\Expression\AbstractExpression
    {
        /**
         * @param Node|null $template
         */
        public function __construct(\Twig_NodeInterface $name, $template, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    abstract class CallExpression extends \Twig\Node\Expression\AbstractExpression
    {
        protected function compileCallable(\Twig\Compiler $compiler)
        {
        }
        protected function compileArguments(\Twig\Compiler $compiler, $isArray = false)
        {
        }
        protected function getArguments($callable, $arguments)
        {
        }
        protected function normalizeName($name)
        {
        }
    }
    class ConditionalExpression extends \Twig\Node\Expression\AbstractExpression
    {
        public function __construct(\Twig\Node\Expression\AbstractExpression $expr1, \Twig\Node\Expression\AbstractExpression $expr2, \Twig\Node\Expression\AbstractExpression $expr3, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    class ConstantExpression extends \Twig\Node\Expression\AbstractExpression
    {
        public function __construct($value, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    class FilterExpression extends \Twig\Node\Expression\CallExpression
    {
        public function __construct(\Twig_NodeInterface $node, \Twig\Node\Expression\ConstantExpression $filterName, \Twig_NodeInterface $arguments, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
}
namespace Twig\Node\Expression\Filter {
    /**
     * Returns the value or the default value when it is undefined or empty.
     *
     *  {{ var.foo|default('foo item on var is not defined') }}
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class DefaultFilter extends \Twig\Node\Expression\FilterExpression
    {
        public function __construct(\Twig_NodeInterface $node, \Twig\Node\Expression\ConstantExpression $filterName, \Twig_NodeInterface $arguments, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
}
namespace Twig\Node\Expression {
    class FunctionExpression extends \Twig\Node\Expression\CallExpression
    {
        public function __construct($name, \Twig_NodeInterface $arguments, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    class GetAttrExpression extends \Twig\Node\Expression\AbstractExpression
    {
        public function __construct(\Twig\Node\Expression\AbstractExpression $node, \Twig\Node\Expression\AbstractExpression $attribute, ?\Twig\Node\Expression\AbstractExpression $arguments, string $type, int $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * @internal
     */
    final class InlinePrint extends \Twig\Node\Expression\AbstractExpression
    {
        public function __construct(\Twig\Node\Node $node, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    class MethodCallExpression extends \Twig\Node\Expression\AbstractExpression
    {
        public function __construct(\Twig\Node\Expression\AbstractExpression $node, $method, \Twig\Node\Expression\ArrayExpression $arguments, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    class NullCoalesceExpression extends \Twig\Node\Expression\ConditionalExpression
    {
        public function __construct(\Twig_NodeInterface $left, \Twig_NodeInterface $right, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a parent node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class ParentExpression extends \Twig\Node\Expression\AbstractExpression
    {
        public function __construct($name, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    class TempNameExpression extends \Twig\Node\Expression\AbstractExpression
    {
        public function __construct($name, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    class TestExpression extends \Twig\Node\Expression\CallExpression
    {
        public function __construct(\Twig_NodeInterface $node, $name, ?\Twig_NodeInterface $arguments, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
}
namespace Twig\Node\Expression\Test {
    /**
     * Checks if a variable is the exact same value as a constant.
     *
     *    {% if post.status is constant('Post::PUBLISHED') %}
     *      the status attribute is exactly the same as Post::PUBLISHED
     *    {% endif %}
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class ConstantTest extends \Twig\Node\Expression\TestExpression
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Checks if a variable is defined in the current context.
     *
     *    {# defined works with variable names and variable attributes #}
     *    {% if foo is defined %}
     *        {# ... #}
     *    {% endif %}
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class DefinedTest extends \Twig\Node\Expression\TestExpression
    {
        public function __construct(\Twig_NodeInterface $node, $name, ?\Twig_NodeInterface $arguments, $lineno)
        {
        }
        protected function changeIgnoreStrictCheck(\Twig\Node\Expression\GetAttrExpression $node)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Checks if a variable is divisible by a number.
     *
     *  {% if loop.index is divisible by(3) %}
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class DivisiblebyTest extends \Twig\Node\Expression\TestExpression
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Checks if a number is even.
     *
     *  {{ var is even }}
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class EvenTest extends \Twig\Node\Expression\TestExpression
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Checks that a variable is null.
     *
     *  {{ var is none }}
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class NullTest extends \Twig\Node\Expression\TestExpression
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Checks if a number is odd.
     *
     *  {{ var is odd }}
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class OddTest extends \Twig\Node\Expression\TestExpression
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Checks if a variable is the same as another one (=== in PHP).
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class SameasTest extends \Twig\Node\Expression\TestExpression
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
}
namespace Twig\Node\Expression\Unary {
    abstract class AbstractUnary extends \Twig\Node\Expression\AbstractExpression
    {
        public function __construct(\Twig_NodeInterface $node, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
        public abstract function operator(\Twig\Compiler $compiler);
    }
    class NegUnary extends \Twig\Node\Expression\Unary\AbstractUnary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class NotUnary extends \Twig\Node\Expression\Unary\AbstractUnary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
    class PosUnary extends \Twig\Node\Expression\Unary\AbstractUnary
    {
        public function operator(\Twig\Compiler $compiler)
        {
        }
    }
}
namespace Twig\Node {
    /**
     * Represents a flush node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class FlushNode extends \Twig\Node\Node
    {
        public function __construct($lineno, $tag)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Internal node used by the for node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class ForLoopNode extends \Twig\Node\Node
    {
        public function __construct($lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a for node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class ForNode extends \Twig\Node\Node
    {
        protected $loop;
        public function __construct(\Twig\Node\Expression\AssignNameExpression $keyTarget, \Twig\Node\Expression\AssignNameExpression $valueTarget, \Twig\Node\Expression\AbstractExpression $seq, ?\Twig\Node\Expression\AbstractExpression $ifexpr, \Twig_NodeInterface $body, ?\Twig_NodeInterface $else, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents an if node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class IfNode extends \Twig\Node\Node
    {
        public function __construct(\Twig_NodeInterface $tests, ?\Twig_NodeInterface $else, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents an import node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class ImportNode extends \Twig\Node\Node
    {
        public function __construct(\Twig\Node\Expression\AbstractExpression $expr, \Twig\Node\Expression\AbstractExpression $var, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a macro node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class MacroNode extends \Twig\Node\Node
    {
        public const VARARGS_NAME = 'varargs';
        public function __construct($name, \Twig_NodeInterface $body, \Twig_NodeInterface $arguments, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a module node.
     *
     * Consider this class as being final. If you need to customize the behavior of
     * the generated class, consider adding nodes to the following nodes: display_start,
     * display_end, constructor_start, constructor_end, and class_end.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class ModuleNode extends \Twig\Node\Node
    {
        public function __construct(\Twig_NodeInterface $body, ?\Twig\Node\Expression\AbstractExpression $parent, \Twig_NodeInterface $blocks, \Twig_NodeInterface $macros, \Twig_NodeInterface $traits, $embeddedTemplates, $name, $source = '')
        {
        }
        public function setIndex($index)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
        protected function compileTemplate(\Twig\Compiler $compiler)
        {
        }
        protected function compileGetParent(\Twig\Compiler $compiler)
        {
        }
        protected function compileClassHeader(\Twig\Compiler $compiler)
        {
        }
        protected function compileConstructor(\Twig\Compiler $compiler)
        {
        }
        protected function compileDisplay(\Twig\Compiler $compiler)
        {
        }
        protected function compileClassFooter(\Twig\Compiler $compiler)
        {
        }
        protected function compileMacros(\Twig\Compiler $compiler)
        {
        }
        protected function compileGetTemplateName(\Twig\Compiler $compiler)
        {
        }
        protected function compileIsTraitable(\Twig\Compiler $compiler)
        {
        }
        protected function compileDebugInfo(\Twig\Compiler $compiler)
        {
        }
        protected function compileGetSource(\Twig\Compiler $compiler)
        {
        }
        protected function compileGetSourceContext(\Twig\Compiler $compiler)
        {
        }
        protected function compileLoadTemplate(\Twig\Compiler $compiler, $node, $var)
        {
        }
    }
    /**
     * Represents a node that captures any nested displayable nodes.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    interface NodeCaptureInterface
    {
    }
    /**
     * Represents a node that outputs an expression.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class PrintNode extends \Twig\Node\Node implements \Twig\Node\NodeOutputInterface
    {
        public function __construct(\Twig\Node\Expression\AbstractExpression $expr, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a sandbox node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class SandboxNode extends \Twig\Node\Node
    {
        public function __construct(\Twig_NodeInterface $body, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Adds a check for the __toString() method when the variable is an object and the sandbox is activated.
     *
     * When there is a simple Print statement, like {{ article }},
     * and if the sandbox is enabled, we need to check that the __toString()
     * method is allowed if 'article' is an object.
     *
     * Not used anymore, to be deprecated in 2.x and removed in 3.0
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class SandboxedPrintNode extends \Twig\Node\PrintNode
    {
        public function compile(\Twig\Compiler $compiler)
        {
        }
        /**
         * Removes node filters.
         *
         * This is mostly needed when another visitor adds filters (like the escaper one).
         *
         * @return Node
         */
        protected function removeNodeFilter(\Twig\Node\Node $node)
        {
        }
    }
    /**
     * Represents a set node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class SetNode extends \Twig\Node\Node implements \Twig\Node\NodeCaptureInterface
    {
        public function __construct($capture, \Twig_NodeInterface $names, \Twig_NodeInterface $values, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * @internal
     */
    class SetTempNode extends \Twig\Node\Node
    {
        public function __construct($name, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a spaceless node.
     *
     * It removes spaces between HTML tags.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class SpacelessNode extends \Twig\Node\Node
    {
        public function __construct(\Twig_NodeInterface $body, $lineno, $tag = 'spaceless')
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a text node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class TextNode extends \Twig\Node\Node implements \Twig\Node\NodeOutputInterface
    {
        public function __construct($data, $lineno)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a nested "with" scope.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class WithNode extends \Twig\Node\Node
    {
        public function __construct(\Twig\Node\Node $body, ?\Twig\Node\Node $variables, $only, $lineno, $tag = null)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
}
namespace Twig {
    /**
     * A node traverser.
     *
     * It visits all nodes and their children and calls the given visitor for each.
     *
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class NodeTraverser
    {
        protected $env;
        protected $visitors = [];
        /**
         * @param NodeVisitorInterface[] $visitors
         */
        public function __construct(\Twig\Environment $env, array $visitors = [])
        {
        }
        public function addVisitor(\Twig\NodeVisitor\NodeVisitorInterface $visitor)
        {
        }
        /**
         * Traverses a node and calls the registered visitors.
         *
         * @return \Twig_NodeInterface
         */
        public function traverse(\Twig_NodeInterface $node)
        {
        }
        protected function traverseForVisitor(\Twig\NodeVisitor\NodeVisitorInterface $visitor, \Twig_NodeInterface $node = null)
        {
        }
    }
}
namespace Twig\NodeVisitor {
    /**
     * Interface for node visitor classes.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    interface NodeVisitorInterface
    {
        /**
         * Called before child nodes are visited.
         *
         * @return \Twig_NodeInterface The modified node
         */
        public function enterNode(\Twig_NodeInterface $node, \Twig\Environment $env);
        /**
         * Called after child nodes are visited.
         *
         * @return \Twig_NodeInterface|false|null The modified node or null if the node must be removed
         */
        public function leaveNode(\Twig_NodeInterface $node, \Twig\Environment $env);
        /**
         * Returns the priority for this visitor.
         *
         * Priority should be between -10 and 10 (0 is the default).
         *
         * @return int The priority level
         */
        public function getPriority();
    }
    /**
     * Used to make node visitors compatible with Twig 1.x and 2.x.
     *
     * To be removed in Twig 3.1.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    abstract class AbstractNodeVisitor implements \Twig\NodeVisitor\NodeVisitorInterface
    {
        public final function enterNode(\Twig_NodeInterface $node, \Twig\Environment $env)
        {
        }
        public final function leaveNode(\Twig_NodeInterface $node, \Twig\Environment $env)
        {
        }
        /**
         * Called before child nodes are visited.
         *
         * @return Node The modified node
         */
        protected abstract function doEnterNode(\Twig\Node\Node $node, \Twig\Environment $env);
        /**
         * Called after child nodes are visited.
         *
         * @return Node|false|null The modified node or null if the node must be removed
         */
        protected abstract function doLeaveNode(\Twig\Node\Node $node, \Twig\Environment $env);
    }
    /**
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class EscaperNodeVisitor extends \Twig\NodeVisitor\AbstractNodeVisitor
    {
        protected $statusStack = [];
        protected $blocks = [];
        protected $safeAnalysis;
        protected $traverser;
        protected $defaultStrategy = false;
        protected $safeVars = [];
        public function __construct()
        {
        }
        protected function doEnterNode(\Twig\Node\Node $node, \Twig\Environment $env)
        {
        }
        protected function doLeaveNode(\Twig\Node\Node $node, \Twig\Environment $env)
        {
        }
        protected function escapePrintNode(\Twig\Node\PrintNode $node, \Twig\Environment $env, $type)
        {
        }
        protected function preEscapeFilterNode(\Twig\Node\Expression\FilterExpression $filter, \Twig\Environment $env)
        {
        }
        protected function isSafeFor($type, \Twig_NodeInterface $expression, $env)
        {
        }
        protected function needEscaping(\Twig\Environment $env)
        {
        }
        protected function getEscaperFilter($type, \Twig_NodeInterface $node)
        {
        }
        public function getPriority()
        {
        }
    }
    /**
     * Tries to optimize the AST.
     *
     * This visitor is always the last registered one.
     *
     * You can configure which optimizations you want to activate via the
     * optimizer mode.
     *
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class OptimizerNodeVisitor extends \Twig\NodeVisitor\AbstractNodeVisitor
    {
        public const OPTIMIZE_ALL = -1;
        public const OPTIMIZE_NONE = 0;
        public const OPTIMIZE_FOR = 2;
        public const OPTIMIZE_RAW_FILTER = 4;
        public const OPTIMIZE_VAR_ACCESS = 8;
        protected $loops = [];
        protected $loopsTargets = [];
        protected $optimizers;
        protected $prependedNodes = [];
        protected $inABody = false;
        /**
         * @param int $optimizers The optimizer mode
         */
        public function __construct($optimizers = -1)
        {
        }
        protected function doEnterNode(\Twig\Node\Node $node, \Twig\Environment $env)
        {
        }
        protected function doLeaveNode(\Twig\Node\Node $node, \Twig\Environment $env)
        {
        }
        protected function optimizeVariables(\Twig_NodeInterface $node, \Twig\Environment $env)
        {
        }
        /**
         * Optimizes print nodes.
         *
         * It replaces:
         *
         *   * "echo $this->render(Parent)Block()" with "$this->display(Parent)Block()"
         *
         * @return \Twig_NodeInterface
         */
        protected function optimizePrintNode(\Twig_NodeInterface $node, \Twig\Environment $env)
        {
        }
        /**
         * Removes "raw" filters.
         *
         * @return \Twig_NodeInterface
         */
        protected function optimizeRawFilter(\Twig_NodeInterface $node, \Twig\Environment $env)
        {
        }
        /**
         * Optimizes "for" tag by removing the "loop" variable creation whenever possible.
         */
        protected function enterOptimizeFor(\Twig_NodeInterface $node, \Twig\Environment $env)
        {
        }
        /**
         * Optimizes "for" tag by removing the "loop" variable creation whenever possible.
         */
        protected function leaveOptimizeFor(\Twig_NodeInterface $node, \Twig\Environment $env)
        {
        }
        protected function addLoopToCurrent()
        {
        }
        protected function addLoopToAll()
        {
        }
        public function getPriority()
        {
        }
    }
    /**
     * @final
     */
    class SafeAnalysisNodeVisitor extends \Twig\NodeVisitor\AbstractNodeVisitor
    {
        protected $data = [];
        protected $safeVars = [];
        public function setSafeVars($safeVars)
        {
        }
        public function getSafe(\Twig_NodeInterface $node)
        {
        }
        protected function setSafe(\Twig_NodeInterface $node, array $safe)
        {
        }
        protected function doEnterNode(\Twig\Node\Node $node, \Twig\Environment $env)
        {
        }
        protected function doLeaveNode(\Twig\Node\Node $node, \Twig\Environment $env)
        {
        }
        protected function intersectSafe(array $a = null, array $b = null)
        {
        }
        public function getPriority()
        {
        }
    }
    /**
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class SandboxNodeVisitor extends \Twig\NodeVisitor\AbstractNodeVisitor
    {
        protected $inAModule = false;
        protected $tags;
        protected $filters;
        protected $functions;
        protected function doEnterNode(\Twig\Node\Node $node, \Twig\Environment $env)
        {
        }
        protected function doLeaveNode(\Twig\Node\Node $node, \Twig\Environment $env)
        {
        }
        public function getPriority()
        {
        }
    }
}
namespace Twig {
    /**
     * Default parser implementation.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class Parser implements \Twig_ParserInterface
    {
        protected $stack = [];
        protected $stream;
        protected $parent;
        protected $handlers;
        protected $visitors;
        protected $expressionParser;
        protected $blocks;
        protected $blockStack;
        protected $macros;
        protected $env;
        protected $reservedMacroNames;
        protected $importedSymbols;
        protected $traits;
        protected $embeddedTemplates = [];
        public function __construct(\Twig\Environment $env)
        {
        }
        /**
         * @deprecated since 1.27 (to be removed in 2.0)
         */
        public function getEnvironment()
        {
        }
        public function getVarName()
        {
        }
        /**
         * @deprecated since 1.27 (to be removed in 2.0). Use $parser->getStream()->getSourceContext()->getPath() instead.
         */
        public function getFilename()
        {
        }
        public function parse(\Twig\TokenStream $stream, $test = null, $dropNeedle = false)
        {
        }
        public function subparse($test, $dropNeedle = false)
        {
        }
        /**
         * @deprecated since 1.27 (to be removed in 2.0)
         */
        public function addHandler($name, $class)
        {
        }
        /**
         * @deprecated since 1.27 (to be removed in 2.0)
         */
        public function addNodeVisitor(\Twig\NodeVisitor\NodeVisitorInterface $visitor)
        {
        }
        public function getBlockStack()
        {
        }
        public function peekBlockStack()
        {
        }
        public function popBlockStack()
        {
        }
        public function pushBlockStack($name)
        {
        }
        public function hasBlock($name)
        {
        }
        public function getBlock($name)
        {
        }
        public function setBlock($name, \Twig\Node\BlockNode $value)
        {
        }
        public function hasMacro($name)
        {
        }
        public function setMacro($name, \Twig\Node\MacroNode $node)
        {
        }
        public function isReservedMacroName($name)
        {
        }
        public function addTrait($trait)
        {
        }
        public function hasTraits()
        {
        }
        public function embedTemplate(\Twig\Node\ModuleNode $template)
        {
        }
        public function addImportedSymbol($type, $alias, $name = null, \Twig\Node\Expression\AbstractExpression $node = null)
        {
        }
        public function getImportedSymbol($type, $alias)
        {
        }
        public function isMainScope()
        {
        }
        public function pushLocalScope()
        {
        }
        public function popLocalScope()
        {
        }
        /**
         * @return ExpressionParser
         */
        public function getExpressionParser()
        {
        }
        public function getParent()
        {
        }
        public function setParent($parent)
        {
        }
        /**
         * @return TokenStream
         */
        public function getStream()
        {
        }
        /**
         * @return Token
         */
        public function getCurrentToken()
        {
        }
        protected function filterBodyNodes(\Twig_NodeInterface $node)
        {
        }
    }
}
namespace Twig\Profiler\Dumper {
    /**
     * @author Fabien Potencier <fabien@symfony.com>
     */
    abstract class BaseDumper
    {
        public function dump(\Twig\Profiler\Profile $profile)
        {
        }
        protected abstract function formatTemplate(\Twig\Profiler\Profile $profile, $prefix);
        protected abstract function formatNonTemplate(\Twig\Profiler\Profile $profile, $prefix);
        protected abstract function formatTime(\Twig\Profiler\Profile $profile, $percent);
    }
    /**
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @final
     */
    class BlackfireDumper
    {
        public function dump(\Twig\Profiler\Profile $profile)
        {
        }
    }
    /**
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @final
     */
    class HtmlDumper extends \Twig\Profiler\Dumper\BaseDumper
    {
        public function dump(\Twig\Profiler\Profile $profile)
        {
        }
        protected function formatTemplate(\Twig\Profiler\Profile $profile, $prefix)
        {
        }
        protected function formatNonTemplate(\Twig\Profiler\Profile $profile, $prefix)
        {
        }
        protected function formatTime(\Twig\Profiler\Profile $profile, $percent)
        {
        }
    }
    /**
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @final
     */
    class TextDumper extends \Twig\Profiler\Dumper\BaseDumper
    {
        protected function formatTemplate(\Twig\Profiler\Profile $profile, $prefix)
        {
        }
        protected function formatNonTemplate(\Twig\Profiler\Profile $profile, $prefix)
        {
        }
        protected function formatTime(\Twig\Profiler\Profile $profile, $percent)
        {
        }
    }
}
namespace Twig\Profiler\Node {
    /**
     * Represents a profile enter node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class EnterProfileNode extends \Twig\Node\Node
    {
        public function __construct($extensionName, $type, $name, $varName)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
    /**
     * Represents a profile leave node.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class LeaveProfileNode extends \Twig\Node\Node
    {
        public function __construct($varName)
        {
        }
        public function compile(\Twig\Compiler $compiler)
        {
        }
    }
}
namespace Twig\Profiler\NodeVisitor {
    /**
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @final
     */
    class ProfilerNodeVisitor extends \Twig\NodeVisitor\AbstractNodeVisitor
    {
        public function __construct($extensionName)
        {
        }
        protected function doEnterNode(\Twig\Node\Node $node, \Twig\Environment $env)
        {
        }
        protected function doLeaveNode(\Twig\Node\Node $node, \Twig\Environment $env)
        {
        }
        public function getPriority()
        {
        }
    }
}
namespace Twig\Profiler {
    /**
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @final
     */
    class Profile implements \IteratorAggregate, \Serializable
    {
        public const ROOT = 'ROOT';
        public const BLOCK = 'block';
        public const TEMPLATE = 'template';
        public const MACRO = 'macro';
        public function __construct($template = 'main', $type = self::ROOT, $name = 'main')
        {
        }
        public function getTemplate()
        {
        }
        public function getType()
        {
        }
        public function getName()
        {
        }
        public function isRoot()
        {
        }
        public function isTemplate()
        {
        }
        public function isBlock()
        {
        }
        public function isMacro()
        {
        }
        public function getProfiles()
        {
        }
        public function addProfile(self $profile)
        {
        }
        /**
         * Returns the duration in microseconds.
         *
         * @return float
         */
        public function getDuration()
        {
        }
        /**
         * Returns the memory usage in bytes.
         *
         * @return int
         */
        public function getMemoryUsage()
        {
        }
        /**
         * Returns the peak memory usage in bytes.
         *
         * @return int
         */
        public function getPeakMemoryUsage()
        {
        }
        /**
         * Starts the profiling.
         */
        public function enter()
        {
        }
        /**
         * Stops the profiling.
         */
        public function leave()
        {
        }
        public function reset()
        {
        }
        public function getIterator() : \Traversable
        {
        }
        public function serialize() : string
        {
        }
        public function unserialize($data) : void
        {
        }
        /**
         * @internal
         */
        public function __serialize() : array
        {
        }
        /**
         * @internal
         */
        public function __unserialize(array $data) : void
        {
        }
    }
}
namespace Twig\RuntimeLoader {
    /**
     * Creates runtime implementations for Twig elements (filters/functions/tests).
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    interface RuntimeLoaderInterface
    {
        /**
         * Creates the runtime implementation of a Twig element (filter/function/test).
         *
         * @param string $class A runtime class
         *
         * @return object|null The runtime instance or null if the loader does not know how to create the runtime for this class
         */
        public function load($class);
    }
    /**
     * Lazily loads Twig runtime implementations from a PSR-11 container.
     *
     * Note that the runtime services MUST use their class names as identifiers.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     * @author Robin Chalas <robin.chalas@gmail.com>
     */
    class ContainerRuntimeLoader implements \Twig\RuntimeLoader\RuntimeLoaderInterface
    {
        public function __construct(\Psr\Container\ContainerInterface $container)
        {
        }
        public function load($class)
        {
        }
    }
    /**
     * Lazy loads the runtime implementations for a Twig element.
     *
     * @author Robin Chalas <robin.chalas@gmail.com>
     */
    class FactoryRuntimeLoader implements \Twig\RuntimeLoader\RuntimeLoaderInterface
    {
        /**
         * @param array $map An array where keys are class names and values factory callables
         */
        public function __construct($map = [])
        {
        }
        public function load($class)
        {
        }
    }
}
namespace Twig\Sandbox {
    /**
     * Exception thrown when a security error occurs at runtime.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class SecurityError extends \Twig\Error\Error
    {
    }
    /**
     * Exception thrown when a not allowed filter is used in a template.
     *
     * @author Martin Hasoň <martin.hason@gmail.com>
     */
    class SecurityNotAllowedFilterError extends \Twig\Sandbox\SecurityError
    {
        public function __construct($message, $functionName, $lineno = -1, $filename = null, \Exception $previous = null)
        {
        }
        public function getFilterName()
        {
        }
    }
    /**
     * Exception thrown when a not allowed function is used in a template.
     *
     * @author Martin Hasoň <martin.hason@gmail.com>
     */
    class SecurityNotAllowedFunctionError extends \Twig\Sandbox\SecurityError
    {
        public function __construct($message, $functionName, $lineno = -1, $filename = null, \Exception $previous = null)
        {
        }
        public function getFunctionName()
        {
        }
    }
    /**
     * Exception thrown when a not allowed class method is used in a template.
     *
     * @author Kit Burton-Senior <mail@kitbs.com>
     */
    class SecurityNotAllowedMethodError extends \Twig\Sandbox\SecurityError
    {
        public function __construct($message, $className, $methodName, $lineno = -1, $filename = null, \Exception $previous = null)
        {
        }
        public function getClassName()
        {
        }
        public function getMethodName()
        {
        }
    }
    /**
     * Exception thrown when a not allowed class property is used in a template.
     *
     * @author Kit Burton-Senior <mail@kitbs.com>
     */
    class SecurityNotAllowedPropertyError extends \Twig\Sandbox\SecurityError
    {
        public function __construct($message, $className, $propertyName, $lineno = -1, $filename = null, \Exception $previous = null)
        {
        }
        public function getClassName()
        {
        }
        public function getPropertyName()
        {
        }
    }
    /**
     * Exception thrown when a not allowed tag is used in a template.
     *
     * @author Martin Hasoň <martin.hason@gmail.com>
     */
    class SecurityNotAllowedTagError extends \Twig\Sandbox\SecurityError
    {
        public function __construct($message, $tagName, $lineno = -1, $filename = null, \Exception $previous = null)
        {
        }
        public function getTagName()
        {
        }
    }
    /**
     * Interface that all security policy classes must implements.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    interface SecurityPolicyInterface
    {
        public function checkSecurity($tags, $filters, $functions);
        public function checkMethodAllowed($obj, $method);
        public function checkPropertyAllowed($obj, $method);
    }
    /**
     * Represents a security policy which need to be enforced when sandbox mode is enabled.
     *
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class SecurityPolicy implements \Twig\Sandbox\SecurityPolicyInterface
    {
        protected $allowedTags;
        protected $allowedFilters;
        protected $allowedMethods;
        protected $allowedProperties;
        protected $allowedFunctions;
        public function __construct(array $allowedTags = [], array $allowedFilters = [], array $allowedMethods = [], array $allowedProperties = [], array $allowedFunctions = [])
        {
        }
        public function setAllowedTags(array $tags)
        {
        }
        public function setAllowedFilters(array $filters)
        {
        }
        public function setAllowedMethods(array $methods)
        {
        }
        public function setAllowedProperties(array $properties)
        {
        }
        public function setAllowedFunctions(array $functions)
        {
        }
        public function checkSecurity($tags, $filters, $functions)
        {
        }
        public function checkMethodAllowed($obj, $method)
        {
        }
        public function checkPropertyAllowed($obj, $property)
        {
        }
    }
}
namespace Twig {
    /**
     * Holds information about a non-compiled Twig template.
     *
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class Source
    {
        /**
         * @param string $code The template source code
         * @param string $name The template logical name
         * @param string $path The filesystem path of the template if any
         */
        public function __construct($code, $name, $path = '')
        {
        }
        public function getCode()
        {
        }
        public function getName()
        {
        }
        public function getPath()
        {
        }
    }
    /**
     * Default base class for compiled templates.
     *
     * This class is an implementation detail of how template compilation currently
     * works, which might change. It should never be used directly. Use $twig->load()
     * instead, which returns an instance of \Twig\TemplateWrapper.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @internal
     */
    abstract class Template implements \Twig_TemplateInterface
    {
        /**
         * @internal
         */
        protected static $cache = [];
        protected $parent;
        protected $parents = [];
        protected $env;
        protected $blocks = [];
        protected $traits = [];
        protected $sandbox;
        public function __construct(\Twig\Environment $env)
        {
        }
        /**
         * @internal this method will be removed in 2.0 and is only used internally to provide an upgrade path from 1.x to 2.0
         */
        public function __toString()
        {
        }
        /**
         * Returns the template name.
         *
         * @return string The template name
         */
        public abstract function getTemplateName();
        /**
         * Returns debug information about the template.
         *
         * @return array Debug information
         */
        public function getDebugInfo()
        {
        }
        /**
         * Returns the template source code.
         *
         * @return string The template source code
         *
         * @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead
         */
        public function getSource()
        {
        }
        /**
         * Returns information about the original template source code.
         *
         * @return Source
         */
        public function getSourceContext()
        {
        }
        /**
         * @deprecated since 1.20 (to be removed in 2.0)
         */
        public function getEnvironment()
        {
        }
        /**
         * Returns the parent template.
         *
         * This method is for internal use only and should never be called
         * directly.
         *
         * @return \Twig_TemplateInterface|TemplateWrapper|false The parent template or false if there is no parent
         *
         * @internal
         */
        public function getParent(array $context)
        {
        }
        protected function doGetParent(array $context)
        {
        }
        public function isTraitable()
        {
        }
        /**
         * Displays a parent block.
         *
         * This method is for internal use only and should never be called
         * directly.
         *
         * @param string $name    The block name to display from the parent
         * @param array  $context The context
         * @param array  $blocks  The current set of blocks
         */
        public function displayParentBlock($name, array $context, array $blocks = [])
        {
        }
        /**
         * Displays a block.
         *
         * This method is for internal use only and should never be called
         * directly.
         *
         * @param string $name      The block name to display
         * @param array  $context   The context
         * @param array  $blocks    The current set of blocks
         * @param bool   $useBlocks Whether to use the current set of blocks
         */
        public function displayBlock($name, array $context, array $blocks = [], $useBlocks = true)
        {
        }
        /**
         * Renders a parent block.
         *
         * This method is for internal use only and should never be called
         * directly.
         *
         * @param string $name    The block name to render from the parent
         * @param array  $context The context
         * @param array  $blocks  The current set of blocks
         *
         * @return string The rendered block
         */
        public function renderParentBlock($name, array $context, array $blocks = [])
        {
        }
        /**
         * Renders a block.
         *
         * This method is for internal use only and should never be called
         * directly.
         *
         * @param string $name      The block name to render
         * @param array  $context   The context
         * @param array  $blocks    The current set of blocks
         * @param bool   $useBlocks Whether to use the current set of blocks
         *
         * @return string The rendered block
         */
        public function renderBlock($name, array $context, array $blocks = [], $useBlocks = true)
        {
        }
        /**
         * Returns whether a block exists or not in the current context of the template.
         *
         * This method checks blocks defined in the current template
         * or defined in "used" traits or defined in parent templates.
         *
         * @param string $name    The block name
         * @param array  $context The context
         * @param array  $blocks  The current set of blocks
         *
         * @return bool true if the block exists, false otherwise
         */
        public function hasBlock($name, array $context = null, array $blocks = [])
        {
        }
        /**
         * Returns all block names in the current context of the template.
         *
         * This method checks blocks defined in the current template
         * or defined in "used" traits or defined in parent templates.
         *
         * @param array $context The context
         * @param array $blocks  The current set of blocks
         *
         * @return array An array of block names
         */
        public function getBlockNames(array $context = null, array $blocks = [])
        {
        }
        /**
         * @return Template|TemplateWrapper
         */
        protected function loadTemplate($template, $templateName = null, $line = null, $index = null)
        {
        }
        /**
         * @internal
         *
         * @return Template
         */
        public function unwrap()
        {
        }
        /**
         * Returns all blocks.
         *
         * This method is for internal use only and should never be called
         * directly.
         *
         * @return array An array of blocks
         */
        public function getBlocks()
        {
        }
        public function display(array $context, array $blocks = [])
        {
        }
        public function render(array $context)
        {
        }
        protected function displayWithErrorHandling(array $context, array $blocks = [])
        {
        }
        /**
         * Auto-generated method to display the template with the given context.
         *
         * @param array $context An array of parameters to pass to the template
         * @param array $blocks  An array of blocks to pass to the template
         */
        protected abstract function doDisplay(array $context, array $blocks = []);
        /**
         * Returns a variable from the context.
         *
         * This method is for internal use only and should never be called
         * directly.
         *
         * This method should not be overridden in a sub-class as this is an
         * implementation detail that has been introduced to optimize variable
         * access for versions of PHP before 5.4. This is not a way to override
         * the way to get a variable value.
         *
         * @param array  $context           The context
         * @param string $item              The variable to return from the context
         * @param bool   $ignoreStrictCheck Whether to ignore the strict variable check or not
         *
         * @return mixed The content of the context variable
         *
         * @throws RuntimeError if the variable does not exist and Twig is running in strict mode
         *
         * @internal
         */
        protected final function getContext($context, $item, $ignoreStrictCheck = false)
        {
        }
        /**
         * Returns the attribute value for a given array/object.
         *
         * @param mixed  $object            The object or array from where to get the item
         * @param mixed  $item              The item to get from the array or object
         * @param array  $arguments         An array of arguments to pass if the item is an object method
         * @param string $type              The type of attribute (@see \Twig\Template constants)
         * @param bool   $isDefinedTest     Whether this is only a defined check
         * @param bool   $ignoreStrictCheck Whether to ignore the strict attribute check or not
         *
         * @return mixed The attribute value, or a Boolean when $isDefinedTest is true, or null when the attribute is not set and $ignoreStrictCheck is true
         *
         * @throws RuntimeError if the attribute does not exist and Twig is running in strict mode and $isDefinedTest is false
         *
         * @internal
         */
        protected function getAttribute($object, $item, array $arguments = [], $type = self::ANY_CALL, $isDefinedTest = false, $ignoreStrictCheck = false)
        {
        }
    }
    /**
     * Exposes a template to userland.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    final class TemplateWrapper
    {
        /**
         * This method is for internal use only and should never be called
         * directly (use Twig\Environment::load() instead).
         *
         * @internal
         */
        public function __construct(\Twig\Environment $env, \Twig\Template $template)
        {
        }
        /**
         * Renders the template.
         *
         * @param array $context An array of parameters to pass to the template
         *
         * @return string The rendered template
         */
        public function render($context = [])
        {
        }
        /**
         * Displays the template.
         *
         * @param array $context An array of parameters to pass to the template
         */
        public function display($context = [])
        {
        }
        /**
         * Checks if a block is defined.
         *
         * @param string $name    The block name
         * @param array  $context An array of parameters to pass to the template
         *
         * @return bool
         */
        public function hasBlock($name, $context = [])
        {
        }
        /**
         * Returns defined block names in the template.
         *
         * @param array $context An array of parameters to pass to the template
         *
         * @return string[] An array of defined template block names
         */
        public function getBlockNames($context = [])
        {
        }
        /**
         * Renders a template block.
         *
         * @param string $name    The block name to render
         * @param array  $context An array of parameters to pass to the template
         *
         * @return string The rendered block
         */
        public function renderBlock($name, $context = [])
        {
        }
        /**
         * Displays a template block.
         *
         * @param string $name    The block name to render
         * @param array  $context An array of parameters to pass to the template
         */
        public function displayBlock($name, $context = [])
        {
        }
        /**
         * @return Source
         */
        public function getSourceContext()
        {
        }
        /**
         * @return string
         */
        public function getTemplateName()
        {
        }
        /**
         * @internal
         *
         * @return Template
         */
        public function unwrap()
        {
        }
    }
}
namespace Twig\Test {
    /**
     * Integration test helper.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     * @author Karma Dordrak <drak@zikula.org>
     */
    abstract class IntegrationTestCase extends \PHPUnit\Framework\TestCase
    {
        /**
         * @return string
         */
        protected abstract function getFixturesDir();
        /**
         * @return RuntimeLoaderInterface[]
         */
        protected function getRuntimeLoaders()
        {
        }
        /**
         * @return ExtensionInterface[]
         */
        protected function getExtensions()
        {
        }
        /**
         * @return TwigFilter[]
         */
        protected function getTwigFilters()
        {
        }
        /**
         * @return TwigFunction[]
         */
        protected function getTwigFunctions()
        {
        }
        /**
         * @return TwigTest[]
         */
        protected function getTwigTests()
        {
        }
        /**
         * @dataProvider getTests
         */
        public function testIntegration($file, $message, $condition, $templates, $exception, $outputs)
        {
        }
        /**
         * @dataProvider getLegacyTests
         * @group legacy
         */
        public function testLegacyIntegration($file, $message, $condition, $templates, $exception, $outputs)
        {
        }
        public function getTests($name, $legacyTests = false)
        {
        }
        public function getLegacyTests()
        {
        }
        protected function doIntegrationTest($file, $message, $condition, $templates, $exception, $outputs)
        {
        }
        protected static function parseTemplates($test)
        {
        }
    }
    abstract class NodeTestCase extends \PHPUnit\Framework\TestCase
    {
        public abstract function getTests();
        /**
         * @dataProvider getTests
         */
        public function testCompile($node, $source, $environment = null, $isPattern = false)
        {
        }
        public function assertNodeCompilation($source, \Twig\Node\Node $node, \Twig\Environment $environment = null, $isPattern = false)
        {
        }
        protected function getCompiler(\Twig\Environment $environment = null)
        {
        }
        protected function getEnvironment()
        {
        }
        protected function getVariableGetter($name, $line = false)
        {
        }
        protected function getAttributeGetter()
        {
        }
    }
}
namespace Twig {
    /**
     * Represents a Token.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @final
     */
    class Token
    {
        protected $value;
        protected $type;
        protected $lineno;
        public const EOF_TYPE = -1;
        public const TEXT_TYPE = 0;
        public const BLOCK_START_TYPE = 1;
        public const VAR_START_TYPE = 2;
        public const BLOCK_END_TYPE = 3;
        public const VAR_END_TYPE = 4;
        public const NAME_TYPE = 5;
        public const NUMBER_TYPE = 6;
        public const STRING_TYPE = 7;
        public const OPERATOR_TYPE = 8;
        public const PUNCTUATION_TYPE = 9;
        public const INTERPOLATION_START_TYPE = 10;
        public const INTERPOLATION_END_TYPE = 11;
        public const ARROW_TYPE = 12;
        /**
         * @param int    $type   The type of the token
         * @param string $value  The token value
         * @param int    $lineno The line position in the source
         */
        public function __construct($type, $value, $lineno)
        {
        }
        public function __toString()
        {
        }
        /**
         * Tests the current token for a type and/or a value.
         *
         * Parameters may be:
         *  * just type
         *  * type and value (or array of possible values)
         *  * just value (or array of possible values) (NAME_TYPE is used as type)
         *
         * @param array|string|int  $type   The type to test
         * @param array|string|null $values The token value
         *
         * @return bool
         */
        public function test($type, $values = null)
        {
        }
        /**
         * @return int
         */
        public function getLine()
        {
        }
        /**
         * @return int
         */
        public function getType()
        {
        }
        /**
         * @return string
         */
        public function getValue()
        {
        }
        /**
         * Returns the constant representation (internal) of a given type.
         *
         * @param int  $type  The type as an integer
         * @param bool $short Whether to return a short representation or not
         *
         * @return string The string representation
         */
        public static function typeToString($type, $short = false)
        {
        }
        /**
         * Returns the English representation of a given type.
         *
         * @param int $type The type as an integer
         *
         * @return string The string representation
         */
        public static function typeToEnglish($type)
        {
        }
    }
}
namespace Twig\TokenParser {
    /**
     * Interface implemented by token parsers.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    interface TokenParserInterface
    {
        /**
         * Sets the parser associated with this token parser.
         */
        public function setParser(\Twig\Parser $parser);
        /**
         * Parses a token and returns a node.
         *
         * @return \Twig_NodeInterface
         *
         * @throws SyntaxError
         */
        public function parse(\Twig\Token $token);
        /**
         * Gets the tag name associated with this token parser.
         *
         * @return string The tag name
         */
        public function getTag();
    }
    /**
     * Base class for all token parsers.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    abstract class AbstractTokenParser implements \Twig\TokenParser\TokenParserInterface
    {
        /**
         * @var Parser
         */
        protected $parser;
        public function setParser(\Twig\Parser $parser)
        {
        }
    }
    /**
     * Applies filters on a section of a template.
     *
     *   {% apply upper %}
     *      This text becomes uppercase
     *   {% endapply %}
     */
    final class ApplyTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideApplyEnd(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Marks a section of a template to be escaped or not.
     *
     *   {% autoescape true %}
     *     Everything will be automatically escaped in this block
     *   {% endautoescape %}
     *
     *   {% autoescape false %}
     *     Everything will be outputed as is in this block
     *   {% endautoescape %}
     *
     *   {% autoescape true js %}
     *     Everything will be automatically escaped in this block
     *     using the js escaping strategy
     *   {% endautoescape %}
     *
     * @final
     */
    class AutoEscapeTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideBlockEnd(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Marks a section of a template as being reusable.
     *
     *  {% block head %}
     *    <link rel="stylesheet" href="style.css" />
     *    <title>{% block title %}{% endblock %} - My Webpage</title>
     *  {% endblock %}
     *
     * @final
     */
    class BlockTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideBlockEnd(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Deprecates a section of a template.
     *
     *    {% deprecated 'The "base.twig" template is deprecated, use "layout.twig" instead.' %}
     *    {% extends 'layout.html.twig' %}
     *
     * @author Yonel Ceruto <yonelceruto@gmail.com>
     *
     * @final
     */
    class DeprecatedTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Evaluates an expression, discarding the returned value.
     *
     * @final
     */
    class DoTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Includes a template.
     *
     *   {% include 'header.html' %}
     *     Body
     *   {% include 'footer.html' %}
     */
    class IncludeTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        protected function parseArguments()
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Embeds a template.
     *
     * @final
     */
    class EmbedTokenParser extends \Twig\TokenParser\IncludeTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideBlockEnd(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Extends a template by another one.
     *
     *  {% extends "base.html" %}
     *
     * @final
     */
    class ExtendsTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Filters a section of a template by applying filters.
     *
     *   {% filter upper %}
     *      This text becomes uppercase
     *   {% endfilter %}
     *
     * @final
     */
    class FilterTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideBlockEnd(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Flushes the output to the client.
     *
     * @see flush()
     *
     * @final
     */
    class FlushTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Loops over each item of a sequence.
     *
     *   <ul>
     *    {% for user in users %}
     *      <li>{{ user.username|e }}</li>
     *    {% endfor %}
     *   </ul>
     *
     * @final
     */
    class ForTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideForFork(\Twig\Token $token)
        {
        }
        public function decideForEnd(\Twig\Token $token)
        {
        }
        // the loop variable cannot be used in the condition
        protected function checkLoopUsageCondition(\Twig\TokenStream $stream, \Twig_NodeInterface $node)
        {
        }
        // check usage of non-defined loop-items
        // it does not catch all problems (for instance when a for is included into another or when the variable is used in an include)
        protected function checkLoopUsageBody(\Twig\TokenStream $stream, \Twig_NodeInterface $node)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Imports macros.
     *
     *   {% from 'forms.html' import forms %}
     *
     * @final
     */
    class FromTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Tests a condition.
     *
     *   {% if users %}
     *    <ul>
     *      {% for user in users %}
     *        <li>{{ user.username|e }}</li>
     *      {% endfor %}
     *    </ul>
     *   {% endif %}
     *
     * @final
     */
    class IfTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideIfFork(\Twig\Token $token)
        {
        }
        public function decideIfEnd(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Imports macros.
     *
     *   {% import 'forms.html' as forms %}
     *
     * @final
     */
    class ImportTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Defines a macro.
     *
     *   {% macro input(name, value, type, size) %}
     *      <input type="{{ type|default('text') }}" name="{{ name }}" value="{{ value|e }}" size="{{ size|default(20) }}" />
     *   {% endmacro %}
     *
     * @final
     */
    class MacroTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideBlockEnd(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Marks a section of a template as untrusted code that must be evaluated in the sandbox mode.
     *
     *    {% sandbox %}
     *        {% include 'user.html' %}
     *    {% endsandbox %}
     *
     * @see https://twig.symfony.com/doc/api.html#sandbox-extension for details
     *
     * @final
     */
    class SandboxTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideBlockEnd(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Defines a variable.
     *
     *  {% set foo = 'foo' %}
     *  {% set foo = [1, 2] %}
     *  {% set foo = {'foo': 'bar'} %}
     *  {% set foo = 'foo' ~ 'bar' %}
     *  {% set foo, bar = 'foo', 'bar' %}
     *  {% set foo %}Some content{% endset %}
     *
     * @final
     */
    class SetTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideBlockEnd(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Remove whitespaces between HTML tags.
     *
     *   {% spaceless %}
     *      <div>
     *          <strong>foo</strong>
     *      </div>
     *   {% endspaceless %}
     *   {# output will be <div><strong>foo</strong></div> #}
     *
     * @final
     */
    class SpacelessTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideSpacelessEnd(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Imports blocks defined in another template into the current template.
     *
     *    {% extends "base.html" %}
     *
     *    {% use "blocks.html" %}
     *
     *    {% block title %}{% endblock %}
     *    {% block content %}{% endblock %}
     *
     * @see https://twig.symfony.com/doc/templates.html#horizontal-reuse for details.
     *
     * @final
     */
    class UseTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
    /**
     * Creates a nested scope.
     *
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @final
     */
    class WithTokenParser extends \Twig\TokenParser\AbstractTokenParser
    {
        public function parse(\Twig\Token $token)
        {
        }
        public function decideWithEnd(\Twig\Token $token)
        {
        }
        public function getTag()
        {
        }
    }
}
namespace Twig {
    /**
     * Represents a token stream.
     *
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class TokenStream
    {
        protected $tokens;
        protected $current = 0;
        protected $filename;
        /**
         * @param array       $tokens An array of tokens
         * @param string|null $name   The name of the template which tokens are associated with
         * @param string|null $source The source code associated with the tokens
         */
        public function __construct(array $tokens, $name = null, $source = null)
        {
        }
        public function __toString()
        {
        }
        public function injectTokens(array $tokens)
        {
        }
        /**
         * Sets the pointer to the next token and returns the old one.
         *
         * @return Token
         */
        public function next()
        {
        }
        /**
         * Tests a token, sets the pointer to the next one and returns it or throws a syntax error.
         *
         * @return Token|null The next token if the condition is true, null otherwise
         */
        public function nextIf($primary, $secondary = null)
        {
        }
        /**
         * Tests a token and returns it or throws a syntax error.
         *
         * @return Token
         */
        public function expect($type, $value = null, $message = null)
        {
        }
        /**
         * Looks at the next token.
         *
         * @param int $number
         *
         * @return Token
         */
        public function look($number = 1)
        {
        }
        /**
         * Tests the current token.
         *
         * @return bool
         */
        public function test($primary, $secondary = null)
        {
        }
        /**
         * Checks if end of stream was reached.
         *
         * @return bool
         */
        public function isEOF()
        {
        }
        /**
         * @return Token
         */
        public function getCurrent()
        {
        }
        /**
         * Gets the name associated with this stream (null if not defined).
         *
         * @return string|null
         *
         * @deprecated since 1.27 (to be removed in 2.0)
         */
        public function getFilename()
        {
        }
        /**
         * Gets the source code associated with this stream.
         *
         * @return string
         *
         * @internal Don't use this as it might be empty depending on the environment configuration
         *
         * @deprecated since 1.27 (to be removed in 2.0)
         */
        public function getSource()
        {
        }
        /**
         * Gets the source associated with this stream.
         *
         * @return Source
         *
         * @internal
         */
        public function getSourceContext()
        {
        }
    }
    /**
     * Represents a template filter.
     *
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class TwigFilter
    {
        protected $name;
        protected $callable;
        protected $options;
        protected $arguments = [];
        public function __construct($name, $callable, array $options = [])
        {
        }
        public function getName()
        {
        }
        public function getCallable()
        {
        }
        public function getNodeClass()
        {
        }
        public function setArguments($arguments)
        {
        }
        public function getArguments()
        {
        }
        public function needsEnvironment()
        {
        }
        public function needsContext()
        {
        }
        public function getSafe(\Twig\Node\Node $filterArgs)
        {
        }
        public function getPreservesSafety()
        {
        }
        public function getPreEscape()
        {
        }
        public function isVariadic()
        {
        }
        public function isDeprecated()
        {
        }
        public function getDeprecatedVersion()
        {
        }
        public function getAlternative()
        {
        }
    }
    /**
     * Represents a template function.
     *
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class TwigFunction
    {
        protected $name;
        protected $callable;
        protected $options;
        protected $arguments = [];
        public function __construct($name, $callable, array $options = [])
        {
        }
        public function getName()
        {
        }
        public function getCallable()
        {
        }
        public function getNodeClass()
        {
        }
        public function setArguments($arguments)
        {
        }
        public function getArguments()
        {
        }
        public function needsEnvironment()
        {
        }
        public function needsContext()
        {
        }
        public function getSafe(\Twig\Node\Node $functionArgs)
        {
        }
        public function isVariadic()
        {
        }
        public function isDeprecated()
        {
        }
        public function getDeprecatedVersion()
        {
        }
        public function getAlternative()
        {
        }
    }
    /**
     * Represents a template test.
     *
     * @final
     *
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class TwigTest
    {
        protected $name;
        protected $callable;
        protected $options;
        public function __construct($name, $callable, array $options = [])
        {
        }
        public function getName()
        {
        }
        public function getCallable()
        {
        }
        public function getNodeClass()
        {
        }
        public function isVariadic()
        {
        }
        public function isDeprecated()
        {
        }
        public function getDeprecatedVersion()
        {
        }
        public function getAlternative()
        {
        }
        public function setArguments($arguments)
        {
        }
        public function getArguments()
        {
        }
        public function hasOneMandatoryArgument() : bool
        {
        }
    }
}
namespace Twig\Util {
    /**
     * @author Fabien Potencier <fabien@symfony.com>
     *
     * @final
     */
    class DeprecationCollector
    {
        public function __construct(\Twig\Environment $twig)
        {
        }
        /**
         * Returns deprecations for templates contained in a directory.
         *
         * @param string $dir A directory where templates are stored
         * @param string $ext Limit the loaded templates by extension
         *
         * @return array An array of deprecations
         */
        public function collectDir($dir, $ext = '.twig')
        {
        }
        /**
         * Returns deprecations for passed templates.
         *
         * @param \Traversable $iterator An iterator of templates (where keys are template names and values the contents of the template)
         *
         * @return array An array of deprecations
         */
        public function collect(\Traversable $iterator)
        {
        }
        /**
         * @internal
         */
        public function errorHandler($type, $msg)
        {
        }
    }
    /**
     * @author Fabien Potencier <fabien@symfony.com>
     */
    class TemplateDirIterator extends \IteratorIterator
    {
        public function current()
        {
        }
        public function key()
        {
        }
    }
}
namespace {
    /**
     * Cycles over a value.
     *
     * @param \ArrayAccess|array $values
     * @param int                $position The cycle position
     *
     * @return string The next value in the cycle
     */
    function twig_cycle($values, $position)
    {
    }
    /**
     * Returns a random value depending on the supplied parameter type:
     * - a random item from a \Traversable or array
     * - a random character from a string
     * - a random integer between 0 and the integer parameter.
     *
     * @param \Traversable|array|int|float|string $values The values to pick a random item from
     * @param int|null                            $max    Maximum value used when $values is an int
     *
     * @throws RuntimeError when $values is an empty array (does not apply to an empty string which is returned as is)
     *
     * @return mixed A random value from the given sequence
     */
    function twig_random(\Twig\Environment $env, $values = \null, $max = \null)
    {
    }
    /**
     * Converts a date to the given format.
     *
     *   {{ post.published_at|date("m/d/Y") }}
     *
     * @param \DateTime|\DateTimeInterface|\DateInterval|string $date     A date
     * @param string|null                                       $format   The target format, null to use the default
     * @param \DateTimeZone|string|false|null                   $timezone The target timezone, null to use the default, false to leave unchanged
     *
     * @return string The formatted date
     */
    function twig_date_format_filter(\Twig\Environment $env, $date, $format = \null, $timezone = \null)
    {
    }
    /**
     * Returns a new date object modified.
     *
     *   {{ post.published_at|date_modify("-1day")|date("m/d/Y") }}
     *
     * @param \DateTime|string $date     A date
     * @param string           $modifier A modifier string
     *
     * @return \DateTime
     */
    function twig_date_modify_filter(\Twig\Environment $env, $date, $modifier)
    {
    }
    /**
     * Converts an input to a \DateTime instance.
     *
     *    {% if date(user.created_at) < date('+2days') %}
     *      {# do something #}
     *    {% endif %}
     *
     * @param \DateTime|\DateTimeInterface|string|null $date     A date
     * @param \DateTimeZone|string|false|null          $timezone The target timezone, null to use the default, false to leave unchanged
     *
     * @return \DateTimeInterface
     */
    function twig_date_converter(\Twig\Environment $env, $date = \null, $timezone = \null)
    {
    }
    /**
     * Replaces strings within a string.
     *
     * @param string             $str  String to replace in
     * @param array|\Traversable $from Replace values
     * @param string|null        $to   Replace to, deprecated (@see https://www.php.net/manual/en/function.strtr.php)
     *
     * @return string
     */
    function twig_replace_filter($str, $from, $to = \null)
    {
    }
    /**
     * Rounds a number.
     *
     * @param int|float $value     The value to round
     * @param int|float $precision The rounding precision
     * @param string    $method    The method to use for rounding
     *
     * @return int|float The rounded number
     */
    function twig_round($value, $precision = 0, $method = 'common')
    {
    }
    /**
     * Number format filter.
     *
     * All of the formatting options can be left null, in that case the defaults will
     * be used.  Supplying any of the parameters will override the defaults set in the
     * environment object.
     *
     * @param mixed  $number       A float/int/string of the number to format
     * @param int    $decimal      the number of decimal points to display
     * @param string $decimalPoint the character(s) to use for the decimal point
     * @param string $thousandSep  the character(s) to use for the thousands separator
     *
     * @return string The formatted number
     */
    function twig_number_format_filter(\Twig\Environment $env, $number, $decimal = \null, $decimalPoint = \null, $thousandSep = \null)
    {
    }
    /**
     * URL encodes (RFC 3986) a string as a path segment or an array as a query string.
     *
     * @param string|array $url A URL or an array of query parameters
     *
     * @return string The URL encoded value
     */
    function twig_urlencode_filter($url)
    {
    }
    /**
     * JSON encodes a variable.
     *
     * @param mixed $value   the value to encode
     * @param int   $options Bitmask consisting of JSON_HEX_QUOT, JSON_HEX_TAG, JSON_HEX_AMP, JSON_HEX_APOS, JSON_NUMERIC_CHECK, JSON_PRETTY_PRINT, JSON_UNESCAPED_SLASHES, JSON_FORCE_OBJECT
     *
     * @return mixed The JSON encoded value
     */
    function twig_jsonencode_filter($value, $options = 0)
    {
    }
    function _twig_markup2string(&$value)
    {
    }
    /**
     * Merges an array with another one.
     *
     *  {% set items = { 'apple': 'fruit', 'orange': 'fruit' } %}
     *
     *  {% set items = items|merge({ 'peugeot': 'car' }) %}
     *
     *  {# items now contains { 'apple': 'fruit', 'orange': 'fruit', 'peugeot': 'car' } #}
     *
     * @param array|\Traversable $arr1 An array
     * @param array|\Traversable $arr2 An array
     *
     * @return array The merged array
     */
    function twig_array_merge($arr1, $arr2)
    {
    }
    /**
     * Slices a variable.
     *
     * @param mixed $item         A variable
     * @param int   $start        Start of the slice
     * @param int   $length       Size of the slice
     * @param bool  $preserveKeys Whether to preserve key or not (when the input is an array)
     *
     * @return mixed The sliced variable
     */
    function twig_slice(\Twig\Environment $env, $item, $start, $length = \null, $preserveKeys = \false)
    {
    }
    /**
     * Returns the first element of the item.
     *
     * @param mixed $item A variable
     *
     * @return mixed The first element of the item
     */
    function twig_first(\Twig\Environment $env, $item)
    {
    }
    /**
     * Returns the last element of the item.
     *
     * @param mixed $item A variable
     *
     * @return mixed The last element of the item
     */
    function twig_last(\Twig\Environment $env, $item)
    {
    }
    /**
     * Joins the values to a string.
     *
     * The separators between elements are empty strings per default, you can define them with the optional parameters.
     *
     *  {{ [1, 2, 3]|join(', ', ' and ') }}
     *  {# returns 1, 2 and 3 #}
     *
     *  {{ [1, 2, 3]|join('|') }}
     *  {# returns 1|2|3 #}
     *
     *  {{ [1, 2, 3]|join }}
     *  {# returns 123 #}
     *
     * @param array       $value An array
     * @param string      $glue  The separator
     * @param string|null $and   The separator for the last pair
     *
     * @return string The concatenated string
     */
    function twig_join_filter($value, $glue = '', $and = \null)
    {
    }
    /**
     * Splits the string into an array.
     *
     *  {{ "one,two,three"|split(',') }}
     *  {# returns [one, two, three] #}
     *
     *  {{ "one,two,three,four,five"|split(',', 3) }}
     *  {# returns [one, two, "three,four,five"] #}
     *
     *  {{ "123"|split('') }}
     *  {# returns [1, 2, 3] #}
     *
     *  {{ "aabbcc"|split('', 2) }}
     *  {# returns [aa, bb, cc] #}
     *
     * @param string $value     A string
     * @param string $delimiter The delimiter
     * @param int    $limit     The limit
     *
     * @return array The split string as an array
     */
    function twig_split_filter(\Twig\Environment $env, $value, $delimiter, $limit = \null)
    {
    }
    // The '_default' filter is used internally to avoid using the ternary operator
    // which costs a lot for big contexts (before PHP 5.4). So, on average,
    // a function call is cheaper.
    /**
     * @internal
     */
    function _twig_default_filter($value, $default = '')
    {
    }
    /**
     * Returns the keys for the given array.
     *
     * It is useful when you want to iterate over the keys of an array:
     *
     *  {% for key in array|keys %}
     *      {# ... #}
     *  {% endfor %}
     *
     * @param array $array An array
     *
     * @return array The keys
     */
    function twig_get_array_keys_filter($array)
    {
    }
    /**
     * Reverses a variable.
     *
     * @param array|\Traversable|string $item         An array, a \Traversable instance, or a string
     * @param bool                      $preserveKeys Whether to preserve key or not
     *
     * @return mixed The reversed input
     */
    function twig_reverse_filter(\Twig\Environment $env, $item, $preserveKeys = \false)
    {
    }
    /**
     * Sorts an array.
     *
     * @param array|\Traversable $array
     *
     * @return array
     */
    function twig_sort_filter($array)
    {
    }
    /**
     * @internal
     */
    function twig_in_filter($value, $compare)
    {
    }
    /**
     * Returns a trimmed string.
     *
     * @return string
     *
     * @throws RuntimeError When an invalid trimming side is used (not a string or not 'left', 'right', or 'both')
     */
    function twig_trim_filter($string, $characterMask = \null, $side = 'both')
    {
    }
    /**
     * Removes whitespaces between HTML tags.
     *
     * @return string
     */
    function twig_spaceless($content)
    {
    }
    /**
     * Escapes a string.
     *
     * @param mixed  $string     The value to be escaped
     * @param string $strategy   The escaping strategy
     * @param string $charset    The charset
     * @param bool   $autoescape Whether the function is called by the auto-escaping feature (true) or by the developer (false)
     *
     * @return string
     */
    function twig_escape_filter(\Twig\Environment $env, $string, $strategy = 'html', $charset = \null, $autoescape = \false)
    {
    }
    /**
     * @internal
     */
    function twig_escape_filter_is_safe(\Twig\Node\Node $filterArgs)
    {
    }
    function _twig_escape_js_callback($matches)
    {
    }
    function _twig_escape_css_callback($matches)
    {
    }
    /**
     * This function is adapted from code coming from Zend Framework.
     *
     * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (https://www.zend.com)
     * @license   https://framework.zend.com/license/new-bsd New BSD License
     */
    function _twig_escape_html_attr_callback($matches)
    {
    }
    /**
     * @internal
     */
    function twig_ensure_traversable($seq)
    {
    }
    /**
     * @internal
     */
    function twig_to_array($seq, $preserveKeys = \true)
    {
    }
    /**
     * Checks if a variable is empty.
     *
     *    {# evaluates to true if the foo variable is null, false, or the empty string #}
     *    {% if foo is empty %}
     *        {# ... #}
     *    {% endif %}
     *
     * @param mixed $value A variable
     *
     * @return bool true if the value is empty, false otherwise
     */
    function twig_test_empty($value)
    {
    }
    /**
     * Checks if a variable is traversable.
     *
     *    {# evaluates to true if the foo variable is an array or a traversable object #}
     *    {% if foo is iterable %}
     *        {# ... #}
     *    {% endif %}
     *
     * @param mixed $value A variable
     *
     * @return bool true if the value is traversable
     */
    function twig_test_iterable($value)
    {
    }
    /**
     * Renders a template.
     *
     * @param array        $context
     * @param string|array $template      The template to render or an array of templates to try consecutively
     * @param array        $variables     The variables to pass to the template
     * @param bool         $withContext
     * @param bool         $ignoreMissing Whether to ignore missing templates or not
     * @param bool         $sandboxed     Whether to sandbox the template or not
     *
     * @return string The rendered template
     */
    function twig_include(\Twig\Environment $env, $context, $template, $variables = [], $withContext = \true, $ignoreMissing = \false, $sandboxed = \false)
    {
    }
    /**
     * Returns a template content without rendering it.
     *
     * @param string $name          The template name
     * @param bool   $ignoreMissing Whether to ignore missing templates or not
     *
     * @return string The template source
     */
    function twig_source(\Twig\Environment $env, $name, $ignoreMissing = \false)
    {
    }
    /**
     * Provides the ability to get constants from instances as well as class/global constants.
     *
     * @param string      $constant The name of the constant
     * @param object|null $object   The object to get the constant from
     *
     * @return string
     */
    function twig_constant($constant, $object = \null)
    {
    }
    /**
     * Checks if a constant exists.
     *
     * @param string      $constant The name of the constant
     * @param object|null $object   The object to get the constant from
     *
     * @return bool
     */
    function twig_constant_is_defined($constant, $object = \null)
    {
    }
    /**
     * Batches item.
     *
     * @param array $items An array of items
     * @param int   $size  The size of the batch
     * @param mixed $fill  A value used to fill missing items
     *
     * @return array
     */
    function twig_array_batch($items, $size, $fill = \null, $preserveKeys = \true)
    {
    }
    function twig_array_filter(\Twig\Environment $env, $array, $arrow)
    {
    }
    function twig_array_map(\Twig\Environment $env, $array, $arrow)
    {
    }
    function twig_array_reduce(\Twig\Environment $env, $array, $arrow, $initial = \null)
    {
    }
}
namespace {
    function twig_var_dump(\Twig\Environment $env, $context, array $vars = [])
    {
    }
}
namespace {
    /**
     * Marks a variable as being safe.
     *
     * @param string $string A PHP variable
     *
     * @return string
     */
    function twig_raw_filter($string)
    {
    }
}
namespace {
    /**
     * Loads a template from a string.
     *
     *     {{ include(template_from_string("Hello {{ name }}")) }}
     *
     * @param string $template A template as a string or object implementing __toString()
     * @param string $name     An optional name of the template to be used in error messages
     *
     * @return TemplateWrapper
     */
    function twig_template_from_string(\Twig\Environment $env, $template, $name = \null)
    {
    }
}